import os.path as osp
import os
import sys
sys.path.insert(0,'..')

import numpy as np
import pandas as pd
import json

from ad_methods import SVM_AD

# run training for given hyperparametrs, training data path, and GNN model id (for ensemble models)
def run_training_svm(hyperparameter_dict, train_data_path, data_GNN_model_id):

    ######################## DEFINE PARAMETERS ######################### 
    model_id = data_GNN_model_id
    # hyperaparemeters in SVM
    kernel, gamma, nu, tol = hyperparameter_dict['kernel'], hyperparameter_dict['gamma'], hyperparameter_dict['nu'], hyperparameter_dict['tol']
    model_name = "hp_test_{}_{}_{}_{}_GNN{}".format(kernel, gamma, nu, tol, model_id)

    ############################ INIT MODEL ############################ 
    svm = SVM_AD(model_name=model_name, kernel=kernel, gamma=gamma, nu=nu, tol=tol)

    ########################### TRAIN MODEL ############################ 
    train_latents = svm.read_csv_data(train_data_path, model_id)
    svm.train(train_latents)

    ############################ SAVE MODEL ############################ 
    svm.save_model("Trained_SVMs_GNN_FIQ", svm.model_name)


def run_evaluation_svm(hyperparameter_dict, infer_data_path, data_GNN_model_id):

    ######################## DEFINE PARAMETERS ######################### 
    model_id = data_GNN_model_id
    # hyperaparemeters in SVM
    kernel, gamma, nu, tol = hyperparameter_dict['kernel'], hyperparameter_dict['gamma'], hyperparameter_dict['nu'], hyperparameter_dict['tol']
    model_name = "hp_test_{}_{}_{}_{}_GNN{}".format(kernel, gamma, nu, tol, model_id)

    ############################ LOAD MODEL ############################ 
    svm = SVM_AD()
    svm.load_model("Trained_SVMs_GNN_FIQ", model_name)

    ########################## INFER W/ MODEL ########################## 
    test_latents = svm.read_csv_data(infer_data_path, model_id)
    gamma_scale = 1/test_latents.shape[1]/test_latents.var()
    #print(f'gamma scale: {1/test_latents.shape[1]/test_latents.var()}')
    test_infer_results = svm.predict(test_latents)
    n_support_vectors = svm._svm.n_support_

    return test_infer_results, n_support_vectors, gamma_scale


def run_diff_hyperp(variation_kernel, variation_gamma, variation_nu, variation_tol, num_models, data_path, save_name, train=False):
    svm_result_list = []
    for hp1 in variation_kernel:
        for hp2 in variation_gamma:
            for hp3 in variation_nu:
                for hp4 in variation_tol:
                    num_valid, num_invalid, num_support_vectors, n_gamma_scale = [], [], [], []
                    for i in range(1,num_models+1):
                        if train:
                            run_training_svm({'kernel': hp1, 'gamma': hp2, 'nu': hp3, 'tol': hp4}, data_path, data_GNN_model_id=i)
                        tmp_infer_results, n_support_vectors, gamma_scale = run_evaluation_svm({'kernel': hp1, 'gamma': hp2, 'nu': hp3, 'tol': hp4}, data_path, data_GNN_model_id=i)
                        num_valid.append(np.count_nonzero(tmp_infer_results == 1))
                        num_invalid.append(np.count_nonzero(tmp_infer_results == -1))
                        num_support_vectors.append(n_support_vectors)
                        n_gamma_scale.append(gamma_scale)
                        #print(num_valid)
                        #print(num_invalid)
                    print(num_valid)
                    svm_result_list.append([hp1,hp2,hp3,hp4, np.mean(num_valid), np.var(num_valid), np.mean(num_invalid), np.var(num_invalid), np.mean(num_support_vectors), np.mean(gamma_scale)])

    svm_result_list = np.array(svm_result_list)
    svm_results = {'kernel': svm_result_list[:,0], 'gamma':svm_result_list[:,1], 'nu': svm_result_list[:,2], 'tol': svm_result_list[:,3], 'valid_mean': svm_result_list[:,4], 'valid_var': svm_result_list[:,5], 'invalid_mean': svm_result_list[:,6], 'invalid_var': svm_result_list[:,7], 'n_support_vectors': svm_result_list[:,8], 'gamma_scale': svm_result_list[:,9]}
    svm_results_df = pd.DataFrame(data=svm_results)
    print(svm_results_df)

    with open(f'Results/svm_results_{save_name}_diff_nu.csv', 'a+') as fp:
        svm_results_df.to_csv(fp, encoding='utf-8', index=False)
    with open(f'Results/svm_results_{save_name}_diff_nu.json', 'a+') as fp:
        json.dump(svm_results_df.to_json(), fp, sort_keys=True, indent=4)


# run model on cross product of hyperparameter options
variation_kernel = ['rbf']
variation_gamma = ['scale'] #[0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001, 'scale']
variation_nu = [0.5, 0.3, 0.1, 0.08, 0.05, 0.03, 0.01, 0.008, 0.005, 0.003, 0.001]
variation_tol = [1e-3] #[1e-3, 1e-5, 1e-8] 

############################ LOAD GNN ENSEMBLE DATA ############################# 
dataset_name = "GNN_FIQ_Schweidtmann_2020"
data_path = os.path.join("../data", "latent_space", dataset_name)
training_data_path = os.path.join(data_path, "Input", "latent_train.csv")
test_data_path = os.path.join(data_path, "Input", "latent_test.csv")

# train models on GNN ensemble training data
run_diff_hyperp(variation_kernel, variation_gamma, variation_nu, variation_tol, num_models=40, data_path=training_data_path, save_name='train_data', train=True)
# evaluate models on GNN ensemble test data
run_diff_hyperp(variation_kernel, variation_gamma, variation_nu, variation_tol, num_models=40, data_path=test_data_path, save_name='test_data', train=False)


############################ LOAD QM9 DATA ############################# 

#dataset_name = "qm9"
#data_path = os.path.join("../data", "latent_space", dataset_name)
#qm9_data_path = os.path.join(data_path, "Input", "latents_qm9_all.csv")

# evaluate models on GNN ensemble test data
#run_diff_hyperp(variation_kernel, variation_gamma, variation_nu, variation_tol, num_models=40, data_path=qm9_data_path, save_name='qm9_data', train=False)


