import os.path as osp
import os
import sys
sys.path.insert(0,'../../..')

import pandas as pd

from graph_neural_networks.trained_GNNs.GNN_FIQ_Schweidtmann_2020 import get_lats, get_preds, get_single_preds


# Path for training and test dataset
dataset_folder_name = '/DCN_MON_RON_joined/Default/'
train_path = osp.join(osp.dirname(osp.realpath(__file__)), '../../../dep/graph_neural_network_for_fuel_ignition_quality/Data' + dataset_folder_name + 'Train/raw/raw.csv')
test_path = osp.join(osp.dirname(osp.realpath(__file__)), '../../../dep/graph_neural_network_for_fuel_ignition_quality/Data' + dataset_folder_name + 'Test/raw/raw.csv')

df_train = pd.read_csv(train_path, sep=';')
df_test = pd.read_csv(test_path, sep=';')

def evaluate_GNN(df=None, save_path=None):
    tmp_dict = {'Compounds': [], 'SMILES': [], 'measured DCNs': [], 'measured MONs': [], 'measured RONs': []}
    for index, row in df.iterrows():
        print(index)
        tmp_dict['Compounds'].append(row['Compounds'])
        tmp_dict['SMILES'].append(row['SMILES'])
        tmp_dict['measured DCNs'].append(row['measured DCNs'])
        tmp_dict['measured MONs'].append(row['measured MONs'])
        tmp_dict['measured RONs'].append(row['measured RONs'])
        tmp_pred_ensemble = get_preds(row['SMILES'])
        for key, value in tmp_pred_ensemble.items():
            if index == 0:
                tmp_dict['predicted_{}'.format(key)] = [value]
            else:
                tmp_dict['predicted_{}'.format(key)].append(value)
        tmp_lat = get_lats(row['SMILES'])
        for key, value in tmp_lat.items():
            if index == 0:
                tmp_dict['latent_vector_{}'.format(key)] = [value]
            else:
                tmp_dict['latent_vector_{}'.format(key)].append(value)
        tmp_pred_single_model = get_single_preds(row['SMILES'])
        for key, value in tmp_pred_single_model.items():
           if index == 0:
               tmp_dict['predicted_DCN_GNN_{}'.format(key)] = [value['DCN']]
               tmp_dict['predicted_MON_GNN_{}'.format(key)] = [value['MON']]
               tmp_dict['predicted_RON_GNN_{}'.format(key)] = [value['RON']]
           else:
               tmp_dict['predicted_DCN_GNN_{}'.format(key)].append(value['DCN'])
               tmp_dict['predicted_MON_GNN_{}'.format(key)].append(value['MON'])
               tmp_dict['predicted_RON_GNN_{}'.format(key)].append(value['RON'])
    df = pd.DataFrame.from_dict(tmp_dict)
    df.to_csv(path_or_buf=save_path, sep=',')

evaluate_GNN(df=df_test, save_path='latents_test_old.csv')
evaluate_GNN(df=df_train, save_path='latents_train_old.csv')




