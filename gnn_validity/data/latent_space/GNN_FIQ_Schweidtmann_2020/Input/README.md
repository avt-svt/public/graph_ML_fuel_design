Note that when using the latest rdkit version (rdkit==2020.09.2) for "3-Methyl-1,2-butadiene: C[C](C)=[C]=[CH2]" the predictions differs from the original model (seems like rdkit changed bond.getStereo()). Thus, for the prediction and latent space extraction the same rdkit version as for former model training is used (rdkit==2019.03.4).

