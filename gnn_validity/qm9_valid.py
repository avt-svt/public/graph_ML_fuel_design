import sys
sys.path.append('..')
from icml18_jtnn.utils.prediction_validity import PredictionValidity 

qm9_path =  '../data/qm9/raw/HCO_smiles.txt'
qm9_smiles = [x.strip("\r\n ") for x in open(qm9_path)]

valid_list = []
with open('validity_smiles_qm9.txt', 'a+') as val_f:
    for smi in qm9_smiles:
        #print(smi)
        prediction_validity_score = PredictionValidity.validity_score([smi], "svm")[0]
        val_f.write("\n{},{}".format(smi, prediction_validity_score))
