# Graph Machine Learning CAMD Framework

This is the source code for the paper **[Graph Machine Learning for Design of High-Octane Fuels](https://doi.org/10.1002/aic.17971)** (AIChE Journal). 

## Overall framework structure

![Framework_structure](doc/images/GraphML_CAMD_framework.png)

## Usage

This repository contains following folders:

* **data**: training data set for generator models
* **docs**: documentation
* **gnn_validity**: applicability domain (AD) via one-class SVMs [1] adapted for GNNs
* **graph_grammar**: Generator model with optimization loop: MHG-VAE by [2] adapted for fuel design
* **graph_neural_network_for_fuel_ignition_quality**: property prediction model: trained GNN for predicting fuel ignition quality [3]
* **icml18_jtnn**: Generator model with optimization loop: JT-VAE by [4] adapted for fuel design
* **molgan**: Generator model with optimization loop: MolGAN by [5] adapted for fuel design
* **utils**: util functions

For **running the optimization loop** with a generator model, please install the required environment and then execute:
`<generator_model>/tasks/run_main_<generator_model>.sh`

Please note that the **main code** for each model is stored in: `<generator_model>/tasks/main_<generator_model>.py`

Please also note that the implementation of the optimization loop is adapted from the MHG-VAE study [2]; the execution of the main code is structured by using Luigi [6].

References:
* [1]: Schweidtmann AM, Weber JM, Wende C, Netze L, Mitsos A. Obey validity limits of data-driven models through topological data analysis and one-class classification. Optimization and Engineering. 2021;pp. 1–22.
* [2]: Kajino H. Molecular Hypergraph Grammar with Its Application to Molecular Optimization. In: Proceedings of the 36th International Conference on Machine Learning, vol. 97 of Proceedings of Machine Learning Research. PMLR. 2019; pp.3183–3191.
* [3]: Schweidtmann AM, Rittig JG, K ̈onig A, Grohe M, Mitsos A, Dahmen M. Graph Neural Networks for Prediction of Fuel Ignition Quality. Energy & Fuels. 2020; 34(9):11395–11407.
* [4]: Jin W, Barzilay R, Jaakkola T. Junction tree variational autoencoder for molecular graph generation. 35th International Conference on Machine Learning, ICML 2018. 2018;5:3632–3648.
* [5]: De Cao N, Kipf T. MolGAN: An implicit generative model for small molecular graphs. arXiv preprint arXiv:1805.11973. 2018.
* [6]: The Luigi authors. Luigi (Python package). https://github.com/spotify/luigi (accessed on 08-09-2021).

## Required packages

The code is built upon: 

* **[PyTorch Geometric package](https://github.com/rusty1s/pytorch_geometric)**
* **[RDKit package](https://www.rdkit.org/)**
* **[icml18_jtnn_repository](https://github.com/wengong-jin/icml18-jtnn)**
* **[graph_grammar_repository](https://github.com/ibm-research-tokyo/graph_grammar)**
* **[MolGAN_repository](https://github.com/nicola-decao/MolGAN)**
* **[graph_neural_network_for_fuel_ignition_quality_repository](https://git.rwth-aachen.de/avt-svt/public/graph_neural_network_for_fuel_ignition_quality)**
* **[luigi_package](https://github.com/spotify/luigi)**

which need to be installed before using our code.

We recommend **setting up a conda environment for each generator model** individually by using the provided yaml-files: 
* MolGAN: `molgan_env.yml`
* MHG-VAE: `graph_grammar_env.yml`
* JT-VAE: `icml18_jtnn_env.yml`


## How to cite this work

Please cite [our paper](https://doi.org/10.1002/aic.17971) if you use this code:

This paper:

```
@misc{RittigRitzert2023,
 author = {Rittig, Jan G. and Ritzert, Martin and Schweidtmann, Artur M. and Winkler, Stefanie and Weber, Jana M. and Morsch, Philipp and Heufer, Karl Alexander and Grohe, Martin and Mitsos, Alexander and Dahmen, Manuel},
 year = {2023},
 title = {Graph machine learning for design of high--octane fuels},
 volume = {69},
 number = {4},
 issn = {00011541},
 journal = {AIChE Journal},
 doi = {10.1002/aic.17971}
}
```

Please also refer to the corresponding packages, that we use, if appropiate:

Pytorch Geomteric:

```
@inproceedings{Fey/Lenssen/2019,
  title={Fast Graph Representation Learning with {PyTorch Geometric}},
  author={Fey, Matthias and Lenssen, Jan E.},
  booktitle={ICLR Workshop on Representation Learning on Graphs and Manifolds},
  year={2019},
}
```

RDKit:

```
@misc{rdkit,
 author = {{Greg Landrum}},
 title = {RDKit: Open-Source Cheminformatics},
 url = {http://www.rdkit.org}
}
```

JT-VAE:
```
@article{Jin2018,
  author = {Jin, Wengong and Barzilay, Regina and Jaakkola, Tbmmi},
  title = {{Junction tree variational autoencoder for molecular graph generation}},
  archivePrefix = {arXiv},
  arxivId = {1802.04364},
  eprint = {1802.04364},
  isbn = {9781510867963},
  journal = {35th International Conference on Machine Learning, ICML 2018},
  pages = {3632--3648},
  volume = {5},
  year = {2018}
}
```

MHG-VAE:
```
@inproceedings{Kajino2019,
  title = {Molecular Hypergraph Grammar with Its Application to Molecular Optimization},
  author = {Kajino, Hiroshi},
  booktitle = {Proceedings of the 36th International Conference on Machine Learning},
  pages = {3183--3191},
  year = {2019},
  volume = {97},
  series = {Proceedings of Machine Learning Research},
  month = {09--15 Jun},
  publisher = {PMLR},
```

MolGAN:
```
@article{de2018molgan,
  title={{MolGAN: An implicit generative model for small
  molecular graphs}},
  author={De Cao, Nicola and Kipf, Thomas},
  journal={ICML 2018 workshop on Theoretical Foundations 
  and Applications of Deep Generative Models},
  year={2018}
}
```

GNN for predicting fuel ignition quality:
```
@article{Schweidtmann2020_GNN,
  author = {Schweidtmann, Artur M. and Rittig, Jan G. and K{\"{o}}nig, Andrea and Grohe, Martin and Mitsos, Alexander and Dahmen, Manuel},
  doi = {10.1021/acs.energyfuels.0c01533},
  issn = {0887-0624},
  journal = {Energy {\&} Fuels},
  number = {9},
  pages = {11395--11407},
  title = {{Graph Neural Networks for Prediction of Fuel Ignition Quality}},
  volume = {34},
  year = {2020}
}
```

Validity/Applicability domain for ANNs:
```
@article{Schweidtmann2021_AD,
 author = {Schweidtmann, Artur M. and Weber, Jana M. and Wende, Christian and Netze, Linus and Mitsos, Alexander},
 year = {2021},
 title = {Obey validity limits of data-driven models through topological data analysis and one-class classification},
 pages = {1--22},
 issn = {1389-4420},
 journal = {Optimization and Engineering},
 doi = {10.1007/s11081-021-09608-0},
}
```
