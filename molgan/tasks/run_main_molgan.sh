### begin of executable commands
export PATH=~/anaconda3/bin:$PATH
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/graph_neural_network_for_fuel_ignition_quality
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/gnn_validity
source activate molgan_py38

# choose one of the three pipelines: TrainingPipeline, MultipleBO, MultipleGA
python main_molgan.py MultipleGA --working-dir working_dir_molOpt_MolGAN --workers 1 --use-gpu --target RON+OS --max-opt-time 43200 --single-seed 123 --constrained 
# optional --constrained --molgan-target RON-MON --max-num-mols 1000 --single-seed 123
# note that molgan-target and can differ -> molgan model trained on <molgan-target> will be employed for generating molecules in the optimization of <target>
