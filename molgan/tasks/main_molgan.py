#!/usr/bin/env python
# -*- coding: utf-8 -*-

#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/graph_ML_fuel_design.git
#
#*********************************************************************************

# original author and copyright
__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "Jan 24 2018"
# adapted by Stefanie Winkler, Jan Rittig, and Martin Ritzert


# set luigi_config_path BEFORE importing luigi
import argparse
import os
import sys
sys.path.append('..')
sys.path.append('../..')

os.environ['CUDA_VISIBLE_DEVICES'] = '1'


# import before new working dir is set
import models # MolGAN
import models.gan # MolGAN
import optimizers # MolGAN
import optimizers.gan # MolGAN
# utils needed for predictions, AD, saving/loading
from utils.prediction_validity import PredictionValidity 
from utils.molecular_metrics import MolecularMetrics 
#from utils.utils_base import save_object, load_object
# molgan stuff (modified)
from utils_molgan.sparse_molecular_dataset import SparseMolecularDataset
from utils_molgan.trainer import Trainer
from utils_molgan.utils import *

# import before new working dir is set
try:
    working_dir = sys.argv[1:][sys.argv[1:].index("--working-dir") + 1]
    os.chdir(working_dir)
except ValueError:
    raise argparse.ArgumentError("--working-dir option must be specified.")
# add a path to luigi.cfg
os.environ["LUIGI_CONFIG_PATH"] = os.path.abspath(os.path.join("INPUT", "luigi.cfg"))
sys.path.append(os.path.abspath(os.path.join("INPUT")))
#sys.path.append(os.path.abspath(os.path.join("INPUT.param")))



# imports
import rdkit
from torch.utils.data import DataLoader
from torch.autograd import Variable
from copy import deepcopy
import glob
import gzip
import luigi
import logging
import numpy as np
import pickle
import torch
import traceback
import math
import random
import faulthandler; faulthandler.enable()
import csv
import json

# MolGAN specific
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 

# concurrency needed to abort long-running decoding processes
import time
import datetime
import concurrent.futures as futures

# TODO: check hyperparameters
from param import MolGAN_params, MolOpt_params, BayesianOptimization_params, GeneticAlgorithm_params, MultipleGA_params, MultipleBO_params

# Luigine is an extension of Luigi so simplify naming the files
from luigine.abc import MainTask, AutoNamingTask, main

logger = logging.getLogger('luigi-interface')
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))


# ensure consistent results
def seed_everything(seed=1234):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    os.environ['TF_CUDNN_DETERMINISTIC'] = '1'  # new flag present in tf 2.0+ # TODO: check tf seeds
    tf.set_random_seed(seed) # TODO: check tf seeds
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
seed_everything(seed=int(MolGAN_params['training']['seed']))


############### HELPER FUNCTIONS for OPTIMIZATION #################

# restores a model to the tensorflow session (no return value)
# restores a model to the tensorflow session (no return value)
def load_model_molgan(directory, session):
    saver = tf.train.Saver()
    saver.restore(session, f"{directory}/model.ckpt")

def timeout(timelimit, verbose=True):
    '''Timeout function with timelimit.
    
    This function stops another function call after a given time limit is exceeded.
    
    Parameters
    ----------
    timelimit: float
        Time limit in seconds after which function call is stopped.
    '''
    
    def decorator(func):
        def decorated(*args, **kwargs):
            with futures.ThreadPoolExecutor(max_workers=1) as executor:
                future = executor.submit(func, *args, **kwargs)
                tmp_t0 = time.time()
                try:
                    result = future.result(timelimit)
                except (futures.TimeoutError, RuntimeError) as error:
                    if verbose: print(f"Timeout! -> Error: {error}")
                    result =  None
                else:
                    if verbose: print(result)
                    pass
                tmp_t1 = time.time()
                if verbose: print(f"\nTotal time for decoding: {tmp_t1-tmp_t0:.3f} sec, (time limit: {timelimit} sec).")
                executor._threads.clear()
                futures.thread._threads_queues.clear()
                return result
        return decorated
    return decorator


# convert vector to smiles string
def get_decoded_smiles(latent, molgan_model, tfsession, data):
    '''Decoding a latent vector into a SMILES string with MolGAN-model
    '''
    reshaped_latent = np.expand_dims(latent,0)
    decoded_mols = samples(data, molgan_model, tfsession, reshaped_latent, sample=False)
    # note that not every latent vector can be converted to a valid molecule.
    smiles = [None for _ in range(len(decoded_mols))]
    for idx, mol in enumerate(decoded_mols):
        tmp_smiles = None
        if mol is not None:
            try:
                tmp_smiles = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(Chem.MolToSmiles(mol))) # make sure that canonical SMILES are stored
            except:
                tmp_smiles = None
                pass
        smiles[idx] = tmp_smiles
    return smiles


class TrainingMolGAN(AutoNamingTask):
                
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    target = luigi.Parameter(default=MolOpt_params['target'])
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="train_molgan")
    output_name = luigi.Parameter(default='finished_training')
    output_ext = luigi.Parameter(default='txt')
    rename = False
    
    def requires(self):
        return []
    
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        # make subdir for target
        if not self.rename:
            self.working_subdir = os.path.join(self.working_subdir, self.target)
            self.rename = True
        # repeat mkdir for subdir
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))
        
        
    def run(self):
        # reset tf graph
        tf.reset_default_graph()
        
        VERBOSE = bool(self.MolGAN_params['model']['verbose'])

        #from utils.sparse_molecular_dataset import SparseMolecularDataset
        #from utils.trainer import Trainer
        #from utils.utils import *

        #from models.gan import GraphGANModel
        #from models import encoder_rgcn, decoder_adj, #decoder_dot, decoder_rnn -> not used

        #from optimizers.gan import GraphGANOptimizer

        #from optparse import OptionParser

        # main entry file to optimize the MolGAN model

        #parser = OptionParser()
        #parser.add_option("-t", "--target", dest="target", default='ron-mon')   ### here user can define metric = <target_property> ###
        #opts, args = parser.parse_args()

        # global variables
        TARGET = self.target
        
        # training seed
        SEED = MolGAN_params['training']['seed']
        seed_everything(SEED)

        # general values
        batch_dim = MolGAN_params['training']['batch_dim'] # 128
        dropout = MolGAN_params['training']['dropout'] #0.1
        epochs = MolGAN_params['training']['epochs'] #60 # TODO: variieren! Bzw plotten wie sich das auswirkt
        learning_rate = MolGAN_params['training']['learning_rate'] #
        save_every = MolGAN_params['training']['save_every'] #1

        # GAN/application specific
        #metric = 'dcn' # vergleich sinnvolles vs default target! ### here user needs to define metric = <target_property>, use lowercase letters! ###
        la = MolGAN_params['training']['la'] #0 # what is this for? # paper: "hyperparameter λ regulates  a  trade-off  between  maximizing  the  desired  objective  and  regulating  the  generator  distribution  to  matchthe  data  distribution. [...] The λ value with the highest sum of scores isλ= 0. We use this value for subsequent experiments."
        n_critic = MolGAN_params['training']['n_critic'] #5 # whats that?
        n_samples = MolGAN_params['training']['n_samples'] #50223 # wo kommt dieser Wert her? (#molecules in QM9)
        z_dim = MolGAN_params['model']['latent_size'] #32
        model_storage_location = os.path.dirname(self.output().path) # directory where model is stored

        # load qm9 dataset (reduced or complete?)
        data = SparseMolecularDataset()
        training_data_path = os.path.join('INPUT', self.MolGAN_params['training']['data'])
        data.load(training_data_path)
        num_training_data_points = len(data.smiles)
        print("Dataset size:", num_training_data_points)
        if n_samples == -1: n_samples = num_training_data_points
            
        steps = len(data) // batch_dim + 1 # TODO: do we drop the last part? -> JR: added + 1

        # this determines what model parts are trained in step i
        # train generator (G) and value network? (V) if steps % n_critic == 0, else train discriminator (D)
        def train_fetch_dict(i, steps, epoch, epochs, min_epochs, model, optimizer):
            a = [optimizer.train_step_G] if i % n_critic == 0 else [optimizer.train_step_D]
            b = [optimizer.train_step_V] if i % n_critic == 0 and la < 1 else []
            return a + b


        def train_feed_dict(i, steps, epoch, epochs, min_epochs, model, optimizer, batch_dim):
            mols, _, _, a, x, _, _, _, _ = data.next_train_batch(batch_dim)
            embeddings = model.sample_z(batch_dim)

            print(f"\nLA: {la}\n")
            # This case distinction is needed because ...
            # ... if la < 1: RL (rewards w.r.t. target value defined in .reward()) are considered during training generator G (value network V is then active) - after half of training epochs (if epoch > epochs/2);
            # ... i la >= 1: RL (rewards w.r.t. target value defined in reward()) are not considered anytime during training generator G (value network V is thus not required).
            if la < 1: # this is a global varialbe

                # This case distinction is needed because ...
                # ... if i % n_ciritc == 0: generator and value network are trained -> reward is required, ...
                # ... if i % n_critic != 0: discriminator is trained -> reward is not required.
                if i % n_critic == 0: # again global
                    rewardR = reward(mols, 'train', epoch, epochs, i, steps)

                    n, e = session.run([model.nodes_gumbel_argmax, model.edges_gumbel_argmax],
                                       feed_dict={model.training: False, model.embeddings: embeddings})
                    n, e = np.argmax(n, axis=-1), np.argmax(e, axis=-1)
                    mols = [data.matrices2mol(n_, e_, strict=True) for n_, e_ in zip(n, e)]

                    rewardF = reward(mols, 'train', epoch, epochs, i, steps)

                    feed_dict = {model.edges_labels: a,
                                 model.nodes_labels: x,
                                 model.embeddings: embeddings,
                                 model.rewardR: rewardR,
                                 model.rewardF: rewardF,
                                 model.training: True,
                                 model.dropout_rate: dropout,
                                 optimizer.la: la if epoch > epochs/2 else 1.0}

                else:
                    feed_dict = {model.edges_labels: a,
                                 model.nodes_labels: x,
                                 model.embeddings: embeddings,
                                 model.training: True,
                                 model.dropout_rate: dropout,
                                 optimizer.la: la if epoch > epochs/2 else 1.0}
            else:
                feed_dict = {model.edges_labels: a,
                             model.nodes_labels: x,
                             model.embeddings: embeddings,
                             model.training: True,
                             model.dropout_rate: dropout,
                             optimizer.la: 1.0}

            return feed_dict



        def test_fetch_dict(model, optimizer):
            return {'loss D': optimizer.loss_D, 'loss G': optimizer.loss_G,
                    'loss RL': optimizer.loss_RL, 'loss V': optimizer.loss_V,
                    'la': optimizer.la}


        def eval_fetch_dict(i, epochs, min_epochs, model, optimizer):
            return test_fetch_dict(model,optimizer)


        # helper function for eval_feed_dict and test_feed_dict that is called for different data parts
        def _generate_feed_dict(model, mols, edges_labels, nodes_labels, tmp_vt_modus='test', tmp_vt_epoch=-1, tmp_vt_epochs=-1):
            embeddings = model.sample_z(edges_labels.shape[0])

            # reward before applying anything
            rewardR = reward(mols, tmp_vt_modus, tmp_vt_epoch, tmp_vt_epochs)

            # run the model on whatever it got at the moment and generate molecules from it
            n, e = session.run([model.nodes_gumbel_argmax, model.edges_gumbel_argmax],
                               feed_dict={model.training: False, model.embeddings: embeddings})
            n, e = np.argmax(n, axis=-1), np.argmax(e, axis=-1)
            mols2 = [data.matrices2mol(n_, e_, strict=True) for n_, e_ in zip(n, e)]

            # reward for forward pass?
            rewardF = reward(mols2, tmp_vt_modus, tmp_vt_epoch, tmp_vt_epoch)

            feed_dict = {model.edges_labels: edges_labels,
                         model.nodes_labels: nodes_labels,
                         model.embeddings: embeddings,
                         model.rewardR: rewardR,
                         model.rewardF: rewardF,
                         model.training: False}
            return feed_dict

        def eval_feed_dict(i, epochs, min_epochs, model, optimizer, batch_dim):
            mols, _, _, a, x, _, _, _, _ = data.next_validation_batch()
            return _generate_feed_dict(model, mols, a, x, 'val', i, epochs)


        def test_feed_dict(model, optimizer, batch_dim):
            mols, _, _, a, x, _, _, _, _ = data.next_test_batch()
            return _generate_feed_dict(model, mols, a, x, 'test')


        # compute rewards for the generated set of molecules
        def reward(mols, rw_modus='test', rw_epoch=0, rw_epochs=0, rw_step=0, rw_steps=0):
            # best possible score
            rr = 1.
            
            # uniqueness, validity, novelty
            #rr *= MolecularMetrics.unique_scores(mols)
            #rr *= MolecularMetrics.valid_scores(mols)
            #rr *= MolecularMetrics.novel_scores(mols, data)
            
            # main target property
            additional_target = True
            if additional_target:
                if TARGET in ['DCN', 'MON', 'RON', 'RON-MON', 'RON+OS']:
                    smiles = [Chem.MolToSmiles(mol) if mol is not None else None for mol in mols]
                    rewards = []
                    for s in smiles:
                        if s in look_up_predictions:
                            predicted_target_value = look_up_predictions[s]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores([s], TARGET, norm=True)[0]
                            look_up_predictions[s] = predicted_target_value
                        rewards.append(predicted_target_value)
                    #print(smiles)
                    #print(rewards)
                    #single_r = MolecularMetrics.target_scores(smiles, TARGET, norm=True) #.reshape(-1, 1) # shouldn't norm=False stand here?
                    #print(single_r)
                    rr *= np.array(rewards)
                    for tmp_smi_idx, tmp_smi in enumerate(smiles):
                        catch_all_smiles.append([tmp_smi, rewards[tmp_smi_idx], rr[tmp_smi_idx], rw_modus, rw_epoch, rw_epochs, rw_step, rw_steps])
                else:
                    raise RuntimeError(f"{TARGET} is not defined as a target")
                    
            print(rr.reshape(-1,1))
            
            return rr.reshape(-1, 1)



        def _eval_update(i, epochs, min_epochs, model, optimizer, batch_dim, eval_batch):
            mols = samples(data, model, session, model.sample_z(n_samples), sample=True)
            m0, m1 = all_scores(mols, data, norm=True)
            m0 = {k: np.array(v)[np.nonzero(v)].mean() for k, v in m0.items()}
            m0.update(m1)
            return m0


        # differs only in the signature from eval_update
        def _test_update(model, optimizer, batch_dim, test_batch):
            mols = samples(data, model, session, model.sample_z(n_samples), sample=True)
            m0, m1 = all_scores(mols, data, norm=True)
            m0 = {k: np.array(v)[np.nonzero(v)].mean() for k, v in m0.items()}
            m0.update(m1)
            return m0


        def best_fn(result):
            val = result[TARGET]
            return val if not math.isnan(val) else 0


        # model
        model = models.gan.GraphGANModel(data.vertexes,
                              data.bond_num_types,
                              data.atom_num_types,
                              z_dim,
                              decoder_units=(128, 256, 512),
                              discriminator_units=((128, 64), 128, (128, 64)),
                              decoder=models.decoder_adj,
                              discriminator=models.encoder_rgcn,
                              soft_gumbel_softmax=False,
                              hard_gumbel_softmax=False,
                              batch_discriminator=False)

        # optimizer
        optimizer = optimizers.gan.GraphGANOptimizer(model, learning_rate=learning_rate, feature_matching=False)

        # session - usually gets defined in the very beginning, before the model
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True # do not fill the whole GPU but take only what you need
        session = tf.Session(config=config)

        # Assume that you have 12GB of GPU memory and want to allocate ~4GB:
        # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
        # session = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
        # session = tf.Session()
        session.run(tf.global_variables_initializer())

        # trainer
        trainer = Trainer(model, optimizer, session)

        print('Parameters: {}'.format(np.sum([np.prod(e.shape) for e in session.run(tf.trainable_variables())])))
        
        catch_all_smiles = []
        look_up_predictions = {}

        tmp_t0 = time.time()
        trainer.train(batch_dim=batch_dim,
                      epochs=epochs,
                      steps=steps,
                      train_fetch_dict=train_fetch_dict,
                      train_feed_dict=train_feed_dict,
                      eval_fetch_dict=eval_fetch_dict,
                      eval_feed_dict=eval_feed_dict,
                      test_fetch_dict=test_fetch_dict,
                      test_feed_dict=test_feed_dict,
                      save_every=save_every,
                      directory=model_storage_location,
                      _eval_update=_eval_update,
                      _test_update=_test_update,
                      best_fn=best_fn)
        
        tmp_t1 = time.time()
        np.savetxt(self.output().path, [tmp_t1 - tmp_t0])
        
        with open(os.path.join(os.path.dirname(self.output().path), f'smiles_from_molgan.csv'), 'a+', newline="") as f_table:
            writer = csv.writer(f_table)
            writer.writerow(['smiles', TARGET, 'total rewards', 'modus', 'epoch', 'max_epoch', 'step', 'max_steps'])
            writer.writerows(catch_all_smiles)
                
    def load_output(self):
        return self.output().path
    
class TrainingPipeline(MainTask, AutoNamingTask):
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    target = luigi.Parameter(default=MolOpt_params['target'])
    use_gpu = luigi.BoolParameter()
    
    def requires(self):
        return [
            TrainingMolGAN(
                MolGAN_params=self.MolGAN_params, 
                target = self.target,
                use_gpu=self.use_gpu,
            )
        ]
    
    def output(self):
        return []
    
    def run(self):
        return []

    
class GenerateLatentFiles(AutoNamingTask):
    
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    target = luigi.Parameter(default=MolOpt_params['target'])
    molgan_target = luigi.Parameter(default=None)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="latent_files")
    output_name = 'latent_features'
    output_ext = 'txt'
    rename = False
    
    def requires(self):
        # molgan_target defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
        if not self.molgan_target: self.molgan_target = self.target
        return [
            TrainingMolGAN(
                MolGAN_params=self.MolGAN_params, 
                target = self.molgan_target,
                use_gpu=self.use_gpu,
            )
        ]
    
    # overwrite output function to define output file name
    def output(self):
        if not self.rename:
            self.output_name = self.output_name + f"_molgan{self.molgan_target}"
            self.rename = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        import pandas as pd
        
        # reset tf graph
        tf.reset_default_graph()
        
        VERBOSE = bool(self.MolGAN_params['model']['verbose'])
        

        #from models import decoder_adj, encoder_rgcn
        #from models.gan import GraphGANModel
        #from utils.molecular_metrics import MolecularMetrics
        #from utils.sparse_molecular_dataset import SparseMolecularDataset
        #from utils.utils import samples

        #from optparse import OptionParser

        #parser = OptionParser()
        #parser.add_option("-t", "--target", dest="target", default='RON-MON')
        #opts, args = parser.parse_args()

        # global variables
        root_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        print(self.target)

        # reduce output printed by rdkit
        lg = rdkit.RDLogger.logger() 
        lg.setLevel(rdkit.RDLogger.CRITICAL)

        # session - usually gets defined in the very beginning, before the model - TODO: check this
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True # do not fill the whole GPU but take only what you need
        session = tf.Session(config=config)

        # Assume that you have 12GB of GPU memory and want to allocate ~4GB:
        # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
        # session = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
        # session = tf.Session()
        session.run(tf.global_variables_initializer())


        def load_model(directory):
            saver = tf.train.Saver()
            saver.restore(session, '{}/{}.ckpt'.format(directory, 'model'))

            
        z_dim = MolGAN_params['model']['latent_size']
        model_path = os.path.dirname(self.requires()[0].load_output())

        data = SparseMolecularDataset()
        training_data_path = os.path.join('INPUT', self.MolGAN_params['training']['data'])
        data.load(training_data_path)
        n_samples = MolGAN_params['training']['n_samples'] #50223 # wo kommt dieser Wert her? (#molecules in QM9)
        num_training_data_points = len(data.smiles)
        if n_samples == -1: n_samples = num_training_data_points
        
        model = models.gan.GraphGANModel(data.vertexes,
                              data.bond_num_types,
                              data.atom_num_types,
                              z_dim,
                              decoder_units=(128, 256, 512),
                              discriminator_units=((128, 64), 128, (128, 64)),
                              decoder=models.decoder_adj,
                              discriminator=models.encoder_rgcn,
                              soft_gumbel_softmax=False,
                              hard_gumbel_softmax=False,
                              batch_discriminator=False)
        
        load_model(model_path)

        #sample_z = model.sample_z(50000) # TODO: why not all 50223? -> we do not get latents of qm9 but rather provide random samples to generator
        sample_z = model.sample_z(n_samples)
        #print(sample_z[:10])   # debug
        sample_mols = samples(data, model, session, sample_z, sample=False)

        smiles = [rdkit.Chem.MolToSmiles(mol) if mol is not None else None for mol in sample_mols]
        #unique_smiles = list(dict.fromkeys(smiles))   # debug
        # from collections import Counter # debug
        #print(Counter(smiles))   # debug
        #print(unique_smiles)   # debug

        # store latent vector embeddings of samples
        latent_points = np.vstack(sample_z)
        np.savetxt(self.output().path, latent_points)
        
        #target_values = MolecularMetrics.target_scores(smiles, self.target, std_score=False, norm=False) # -> now, in ComputeTargetValues
        #np.savetxt(os.path.join(os.path.dirname(self.output().path), f"values_{self.target}.txt"), target_values) # -> now, in ComputeTargetValues
        
        with gzip.open(os.path.join(os.path.dirname(self.output().path), f"smiles_{self.target}.pklz"), 'wb') as f:
            pickle.dump(smiles, f)

        tf.get_variable_scope().reuse_variables()
        '''
        # We store the results
        from collections import Counter
        # caluclate target values for all targets once and save with explicit file names
        if TARGET in ['DCN', 'MON', 'RON', 'RON-MON', 'DCN+MON+RON', 'NORM_DCN+MON_RON']:

            load_model(model_path)

            #sample_z = model.sample_z(50000) # why not all 50223? -> we do not get latents of qm9 but rather provide random samples to generator
            sample_z = model.sample_z(n_samples)
            #print(sample_z[:10])   # debug
            sample_mols = samples(data, model, session, sample_z, sample=False)

            smiles = [rdkit.Chem.MolToSmiles(mol) if mol is not None else None for mol in sample_mols]
            #unique_smiles = list(dict.fromkeys(smiles))   # debug
            #print(Counter(smiles))   # debug
            #print(unique_smiles)   # debug

            # store latent vector embeddings of samples
            latent_points = np.vstack(sample_z)
            np.savetxt(self.output().path, latent_points)
            
            # store smiles corresponding to latent vectors
            np.savetxt(os.path.join(os.path.dirname(self.output().path), f"smiles_{TARGET}.txt"), smiles)

            # target values
            #print(smiles[:10])   # debug
            target_values = MolecularMetrics.target_scores(smiles, TARGET, std_score=False, norm=False)
            np.savetxt(os.path.join(os.path.dirname(self.output().path), f"values_{TARGET}.txt"), target_values)
            
            # normalized target values
            #target_values_norm = MolecularMetrics.target_scores(smiles, TARGET, std_score=False, norm=True)
            #np.savetxt(f'model_data/values_{TARGET}_norm.txt', target_values_norm)

            # standardized target values
            #target_values_std = MolecularMetrics.target_scores(smiles, TARGET, std_score=True, norm=False)
            #np.savetxt(f'model_data/values_{TARGET}_std.txt', target_values_std)
        else:
            print(f'Invalid target: {TARGET}')
    '''       
        
    def load_output(self):
        with gzip.open(os.path.join(os.path.dirname(self.output().path), f"smiles_{self.target}.pklz"), 'rb') as f:
            smiles = pickle.load(f)
        return np.loadtxt(self.output().path), smiles
    
    
class ComputeTargetValues(AutoNamingTask):
    '''
    This task computes target values.
    ** only training set is used, which will be randomly split into train and test sets in the BO setting. 
    No shuffling of the data is performed here. Data that arrives "later" than 'part_train' is simply ignored. **
    '''

    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="target_values")
    target = luigi.Parameter(default=MolOpt_params['target'])
    molgan_target = luigi.Parameter(default=None)
    constrained = luigi.BoolParameter(default=False)
    
    output_name = 'target_values'
    output_ext = 'pklz'
    rename = False

    def requires(self):
        if not self.molgan_target: self.molgan_target = self.target
        return [
            GenerateLatentFiles(
                MolGAN_params=self.MolGAN_params,
                target=self.target,
                molgan_target = self.molgan_target,
                use_gpu=self.use_gpu
            ),
        ]
    
        # overwrite output function to define output file name
    def output(self):
        if not self.rename:
            self.output_name = self.output_name + f"_molgan{self.molgan_target}_opt{self.target}"
            if self.constrained:
                self.output_name = self.output_name + "_ADconstraint"
            self.rename = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        # find out which targets to compute
        perform_RON = False
        perform_MON = False
        perform_DCN = False
        # special_normalization = False # we will not consider norm case in final study and as it makes BO dataset generation more complex, it is excluded here

        # allowed values are RON, MON, DCN, RON-MON, DCN+MON+RON, norm_DCN+MON+RON
        if 'DCN' in self.target:
            perform_DCN = True
        if 'MON' in self.target:
            perform_MON = True
        if 'RON' in self.target:
            perform_RON = True
        if 'OS' in self.target:
            perform_RON = True
            perform_MON = True
        #if 'norm' in self.target:
        #    special_normalization = True

        # lists for raw regression data generated by the network
        dcn_list = []
        mon_list = []
        ron_list = []
        valid_mask = []
        molecule_counter = 0
        
        look_up_pred_dcn = {}
        look_up_pred_mon = {}
        look_up_pred_ron = {}
        look_up_valid_mask = {}

        _, smiles = self.requires()[0].load_output()
        print(smiles[:10])
        
        for smi in smiles:
            tmp_invalid = False
            molecule_counter = molecule_counter + 1
            valid_mask.append(True)
            
            if smi in look_up_valid_mask:
                valid_mask[-1] = look_up_valid_mask[smi]
                if perform_DCN: dcn_list.append(look_up_pred_dcn[smi])
                if perform_MON: mon_list.append(look_up_pred_mon[smi])
                if perform_RON: ron_list.append(look_up_pred_ron[smi])
                continue
            
            print(smi)
            if smi is not None and ('.' in smi or '*' in smi):
                tmp_invalid = True
                print(1)
            else:
                if self.constrained:
                    prediction_validity_score = PredictionValidity.validity_score([smi], "svm", model='kgnn', target=self.target)[0]
                    if prediction_validity_score == -1: tmp_invalid = True
            print(tmp_invalid)
            if tmp_invalid:
                dcn_list.append(-1000)
                look_up_pred_dcn[smi] = -1000
                mon_list.append(-1000)
                look_up_pred_mon[smi] = -1000
                ron_list.append(-1000)
                look_up_pred_ron[smi] = -1000
                valid_mask[-1] = False
                look_up_valid_mask[smi] = False
                print(f"Invalid {smi}")
                continue
            else:
                look_up_valid_mask[smi] = True
                if perform_DCN: 
                    tmp_dcn = MolecularMetrics.dcn_score([smi])[0]
                    dcn_list.append(tmp_dcn)
                    look_up_pred_dcn[smi] = tmp_dcn
                if perform_MON: 
                    tmp_mon = MolecularMetrics.mon_score([smi])[0]
                    mon_list.append(tmp_mon)
                    look_up_pred_mon[smi] = tmp_mon
                if perform_RON: 
                    tmp_ron = MolecularMetrics.ron_score([smi])[0]
                    ron_list.append(tmp_ron)
                    look_up_pred_ron[smi] = tmp_ron

        # compute final target values
        y_all = 0
        if perform_DCN: y_all = y_all + np.array(dcn_list)
        if perform_MON: y_all = y_all + np.array(mon_list)
        if perform_RON: y_all = y_all + np.array(ron_list)
        # special case RON-MON
        if self.target == 'RON-MON':
            y_all = np.array(ron_list) - np.array(mon_list)
        elif self.target == 'RON+OS':
            y_all = np.array(ron_list) * 2 - np.array(mon_list)
        elif self.target == 'wRON+wOS':
            y_all = (np.array(ron_list) * (0.5/120)) + ((np.array(ron_list) - np.array(mon_list)) * (0.5/30))
        # store raw data
        y_raw = {'dcn_list': dcn_list, 'mon_list': mon_list, 'ron_list': ron_list} # some of them may be empty lists
        # save to disk
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((y_all, y_raw, valid_mask), f)
        
        AD_extra = ''
        if self.constrained: AD_extra = '_ADconstraint'
        np.savetxt(os.path.join(os.path.dirname(self.output().path), f"values_molgan{self.molgan_target}_molopt{self.target}{AD_extra}.txt"), y_all)


    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            y_all, y_raw, valid_mask = pickle.load(f)
        return y_all, y_raw, valid_mask
    
    
class ConstructDatasetForBO(AutoNamingTask):
    '''
    This task constructs a dataset for Bayesian optimization.
    ** only training set is used, which will be randomly split into train and test sets in the BO setting. **
    '''

    #DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    #Train_params = luigi.DictParameter(default=Train_params)
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    target = luigi.Parameter(default=MolOpt_params['target'])
    molgan_target = luigi.Parameter(default=None)
    use_gpu = luigi.BoolParameter()
    constrained = luigi.BoolParameter(default=False)
    working_subdir = luigi.Parameter(default="bo_dataset")

    def requires(self):
        # molgan_target defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
        if not self.molgan_target: self.molgan_target = self.target
        return [
            GenerateLatentFiles(
                MolGAN_params=self.MolGAN_params,
                target=self.target,
                molgan_target=self.molgan_target,
                use_gpu=self.use_gpu
            ),
            ComputeTargetValues( 
                MolGAN_params=self.MolGAN_params,
                target=self.target,
                molgan_target=self.molgan_target,
                use_gpu=self.use_gpu,
                constrained=self.constrained,
            )
        ]
        
    def run(self):
        X_all, _smi = self.requires()[0].load_output()
        
        # target val
        y_all, y_raw, valid_mask = self.requires()[1].load_output()
        
        print("Potential smiles for pretraining of GP.")
        tmp_l = []
        for i, s in enumerate(_smi):
            if s not in tmp_l:
                print(f"{s}: {y_all[i]}")
                tmp_l.append(s)

        assert X_all.shape[0] == len(y_all.ravel()), 'X_all and y_all have inconsistent shapes: {}, {}'.format(X_all.shape[0], len(y_all.ravel()))
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((X_all, y_all, y_raw, valid_mask), f)

    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            X_all, y_all, y_raw, valid_mask = pickle.load(f)
        return X_all, y_all, y_raw, valid_mask
    
    
class DrawResultPlots(AutoNamingTask):
    
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    working_subdir = luigi.Parameter(default="result_plots")
    valid_smiles = luigi.Parameter(default=None)
    output_name = luigi.Parameter(default='best_mols')
    output_ext = luigi.Parameter(default='svg') 
    
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))
    
    
    def requires(self):
        return []
        
    def run(self):
        
        def plotting_results_and_save(valid_smiles_w_scores, training_smiles):
            import rdkit.Chem.Draw.IPythonConsole
            rdkit.Chem.Draw.IPythonConsole.ipython_useSVG=True

            # make unique list of molecules and sort it wrt target property
            unique_smiles = list(dict(valid_smiles_w_scores).items())
            unique_smiles.sort(key=lambda x: x[1], reverse=True) # sort smiles wrt target value descending

            num_mols = 30
            if len(unique_smiles) <= 30: num_mols = len(unique_smiles)
            smiles_to_plot = list(zip(*unique_smiles[:num_mols]))[0] # plot only smiles corresponding to top 30 highest target values
            # returns dcn, mon, and ron scores for a list of smiles strings
            def compute_scores(smiles):
                prediction_dcn = MolecularMetrics.dcn_score(smiles)
                prediction_ron = MolecularMetrics.ron_score(smiles)
                prediction_mon = MolecularMetrics.mon_score(smiles)
                return prediction_dcn, prediction_mon, prediction_ron
            dcn, mon, ron = compute_scores(smiles_to_plot)

            # check if which molecules are in training set
            training_canon_smiles = [(rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s))) for s in training_smiles]
            for i,s in enumerate(unique_smiles):
                tmp_s = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s[0]))
                tmp_v = ''
                if tmp_s in training_canon_smiles:
                    tmp_v = 'in training set'
                else:
                    tmp_v = 'not in training set'
                unique_smiles[i] = (*unique_smiles[i], tmp_v)

            # Plot
            mols = [rdkit.Chem.MolFromSmiles(s) for s in smiles_to_plot]
            vals = [f"SMILES: {smi}, \n DCN: {dcn[i]:.2f}, MON: {mon[i]:.2f}, RON: {ron[i]:.2f}, \n RON-MON: {ron[i]-mon[i]:.2f} \n RON+OS: {2*ron[i]-mon[i]:.2f}" for i, smi in enumerate(smiles_to_plot)]
            img = rdkit.Chem.Draw.MolsToGridImage(mols, molsPerRow=5, subImgSize=(300, 300), legends=vals)
            with open(self.output().path, 'w') as f_handle:
                f_handle.write(img.data)

            # Write final results to csv
            import csv
            with open(os.path.join(os.path.dirname(self.output().path), f'{self.output_name}_list.csv'), 'w', newline="") as f_table:
                writer = csv.writer(f_table)
                writer.writerows(unique_smiles)
        
        # Plot results and save to working subdir        
        # we train with HCO-qm9 so training data is equal to qm9 
        data = SparseMolecularDataset()
        training_data_path = os.path.join('INPUT', self.MolGAN_params['training']['data'])
        data.load(training_data_path)
        training_smiles = data.smiles
        plotting_results_and_save(valid_smiles_w_scores=self.valid_smiles, training_smiles=training_smiles)
        
    def load_output(self):
        return []


class BayesianOptimization(AutoNamingTask):
    '''
    This task calculates the reconstruction error rate and validity rate.
    In here the Bayesian Optimization loop is executed once.
    The output not only includes error and validity rate, but also the generated smiles files with corresponding performance values.
    '''
    constrained = luigi.BoolParameter(default=GeneticAlgorithm_params['target_params']['constrained'])

    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    
    target= luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    molgan_target = luigi.Parameter(default=None)
    max_num_mols= luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="bayesian_optimization")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False

    def requires(self):
        # molgan_target defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
        if not self.molgan_target: self.molgan_target = self.target
        return [
            TrainingMolGAN(
                MolGAN_params=self.MolGAN_params,
                target=self.molgan_target,
                use_gpu=self.use_gpu
            ),
            ConstructDatasetForBO(
                MolGAN_params=self.MolGAN_params,
                target=self.target,
                molgan_target=self.molgan_target,
                use_gpu=self.use_gpu,
                constrained=self.constrained,
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        import GPyOpt
        import GPy.kern
        import scipy as sp
        #import sklearn.random_projection.GaussianRandomProjection
        import sklearn.decomposition
        
        # reset tf graph
        tf.reset_default_graph()
        
        ############ INITIALIZE MolGAN MODEL #############
        
                
        # provide the TRAINED model path
        model_path = os.path.dirname(self.requires()[0].load_output())
        z_dim = MolGAN_params['model']['latent_size'] 
        
        # set seed
        seed_everything(self.seed)
        
        # general parameters for the genetic algorithm
        TARGET = self.target
        STD_TARGET = self.BayesianOptimization_params['target_params']['std_target']
        print(f"\nTarget property {TARGET} - standardized {STD_TARGET}")
        TIME_LIMIT_DECODING = self.MolGAN_params['inference']['time_limit_decoding']
        print(f"\nTime limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nOptimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nOptimization will be stopped after {MAX_OPT_TIME} seconds.")
        CONSTRAINED = self.constrained
        print(f"\nBO: Constrained by applicability domain of GNN: {CONSTRAINED}")
        
        # start session for MolGAN (BO does not use tensorflow)
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True # do not fill the whole GPU but take only what you need
        session = tf.Session(config=config)
        session.run(tf.global_variables_initializer())

        # load data for optimization
        data = SparseMolecularDataset()
        training_data_path = os.path.join('INPUT', self.MolGAN_params['training']['data'])
        data.load(training_data_path)

        # define the model 
        molgan_model = models.gan.GraphGANModel(data.vertexes,
                              data.bond_num_types,
                              data.atom_num_types,
                              z_dim,
                              decoder_units=(128, 256, 512),
                              discriminator_units=((128, 64), 128, (128, 64)),
                              decoder=models.decoder_adj,
                              discriminator=models.encoder_rgcn,
                              soft_gumbel_softmax=False,
                              hard_gumbel_softmax=False,
                              batch_discriminator=False)

        # fill the model with life (i.e. load the parameters)
        load_model_molgan(model_path, session)  
        
        
        ############ SET UP BO #############
        
        # copied from graph_grammar/src/graph_grammar/bo/mol_opt.py
        dim_reduction_catalog = {
            'PCA': sklearn.decomposition.PCA,
            'KernelPCA': sklearn.decomposition.KernelPCA,
            #'GaussianRandomProjection': GaussianRandomProjectionWithInverse
        }

        kern_catalog = {
            'Matern52': GPy.kern.Matern52,
            'RBF': GPy.kern.RBF
        }

        gp_catalog = {
            'GPModel': GPyOpt.models.gpmodel.GPModel,
            'GPModel_MCMC': GPyOpt.models.gpmodel.GPModel_MCMC
        }


        def run_gpyopt(
            X_train, y_train, 
            X_test=None, y_test=None,
            X_all=None, y_all=None,
            bo_num_iter=250,
            deterministic=True,
            min_coef=1.2, 
            max_coef=1.2,
            #num_train=1000,  # we directly provide correct number of training data points
            dim_reduction_method='PCA',
            dim_reduction_params={
                'n_components': 0.999, # if 0 < value < 1: n_components is selected according to explained varianece ratio
                'svd_solver': 'full', # should be full if 0 < n_compontens < 1
                #'fit_inverse_transform': True, # for kernelPCA
                #'kernel': 'Matern52' # for kernelPCA
            }, 
            fix_dim_reduction=True,
            bo_params={
                'model_type': 'GP', 
                'kernel': 'Matern52',
                'kernel_kwargs':{}, 
                'acquisition_type': 'EI',
                'acquisition_optimizer_type': 'lbfgs',
                'normalize_Y': True,
                'evaluator_type': 'thompson_sampling', 
                'batch_size': 10
            },
            ext_gp_method=None,
            ext_gp_params={
                'kernel': 'Matern52', 
                'sparse': True, 
                'num_inducing': 1500
            },
            logger=print
        ):
            ''' run molecular optimization

            Parameters
            ----------
            X_train: array-like, shape (num_train, dim)
                Latent vectors of molecules to be used for GP training, computed by MHG model.
            y_train: array-like, shape (num_train,)
                Predicted target values of molecules to be used for GP training.
            X_test: array-like, shape (num_test, dim)
                Latent vectors of molecules to be used for GP testing, computed by MHG model.
            y_test: array-like, shape (num_test,)
                Predicted target values of molecules to be used for GP testing (used to compute test RMSE and test log-likelihood for validation).
            X_all: array-like, shape (num_data, dim)
                Latent vectors of all molecules in the dataset, computed by MHG model.
            y_all: array-like, shape (num_train,)
                Predicted target values of all molecules in the dataset.
            bo_num_iter: int, optional
                The number of BO iterations.
                Default is 250.
            min_coef: float, optional
                The coefficient used to set the lower bounds for the search space for BO.
                Calculated as minimum of all latent vectors in the dataset in each dimension substracted with (min_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            max_coef: float, optional
                The coefficient used to set the upper bounds for the search space for BO.
                Calculated as maximum of all latent vectors in the dataset in each dimension increased by (max_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            dim_reduction_method: string, optional (ignored if None)
                Dimension reduction for latent vectors.
                If None, no dimension reduction is applied.
                Calculated based on X_all.
                Default is PCA.
            dim_redcution_params: dict, optional
                Specific parameters for dimension reduction method.
            fix_dim_reduction: Boolean, optional
                If dimension reduction is applied, it can be defined whether the dimension reduction method is fixed (True) or retrained after each BO iteration (False).
                Default: True
            bo_params: dict, optional
                Specific parameters used in BO of GPyOpt.
                Default similar to GPyOpt with evaluator_type: thompson_sampling, batch_size: 10, and GP model of type sparse: True, num_inducing: 1500
            ext_gp_method: string, optional (ignored if None)
                External surrogate for BO within GPyOpt.
                If None, internal surrogate model of GPyOpt is used that can be defined in bo_params.
                Default is None.
            ext_gp_params: dict, optional (ignored if ext GP method is None!)
                Specific parameters for surrogate model used for BO.
                If ext_gp_method is None, this is ignored! Please specifiy GP parameters in bo_params!
                Default kernel: Matern52, sparse: True, num_inducing: 1500.

            Returns
            -------
            history : list of dicts
                in each dict,
                - 'mol_list' is a list of molecules that BO chooses
                - 'feature' is an array containing latent representations of the molecules
                - 'score_list' is a list of scores obtained by applying `target_func` to the molecules
            '''
            
            print(f"\n####################### Start BO with GPyOpt with seed {self.seed} #######################")
            if X_test is None:
                X_test = X_train[0:1, :]
                y_test = y_train[0:1]
                valid_test = False
            else:
                valid_test = True
            
            if X_all is None:
                X_all = np.concatenate((X_train, X_test),0)
                y_all = np.concatenate((y_train, y_test),0)
                
            STD_TARGET = False
           
            latent_dimension = X_all.shape[1]
            print(f"Shape of all data: {X_all.shape}")
            dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params) if dim_reduction_method else None
            if dim_reduction: 
                print(f"Dim reduction will be done by {dim_reduction_method} with params {dim_reduction_params}")
                # catch PCA set-up failure
                if dim_reduction_method == 'PCA':
                    tmp_n_components = dim_reduction_params['n_components']
                    if tmp_n_components < 1:
                        tmp_svd_solver = dim_reduction_params['svd_solver']
                        if tmp_svd_solver != 'full': raise ValueError(f"Dimension reduction set-up is fragile: n_components {tmp_n_components}, svd_solver: {tmp_svd_solver}.")
                # fit dimension reduction method with all available data points
                dim_reduction.fit(X_all)
                latent_dimension = dim_reduction.n_components_
                expl_var_r = dim_reduction.explained_variance_ratio_
                logger(f"\n\t----- Dim reduction by PCA fitted. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                logger(f"Components: {latent_dimension}, Total explained variance: {np.sum(expl_var_r)}")
                
            
            # center around 0 with std dev of 1
            y_train_mean, y_train_std = 0, 1 # default values -> transformation does not change numbers
            if STD_TARGET:
                y_train_mean, y_train_std = np.mean(y_train), np.std(y_train)
                y_train = (y_train - y_train_mean) / y_train_std
                y_test = (y_test - y_train_mean) / y_train_std
                print(f"\n{TARGET} are standardized.")
                print(f"Mean value for {TARGET} training data {y_train_mean}")
                print(f"Std dev value for {TARGET} training data {y_train_std}")
            
            X_train_excerpt = X_train
            y_train_excerpt = y_train
            X_all_excerpt = X_all

            if X_train.shape[0] != len(y_train.ravel()):
                raise ValueError('X_train and y_train have inconsistent shapes')
            if X_test.shape[0] != len(y_test.ravel()):
                raise ValueError('X_test and y_test have inconsistent shapes')
            
            y_train_excerpt = - y_train_excerpt.reshape(-1, 1) # minus target value
            y_test = - y_test.reshape(-1, 1) # minus target value

            print(f"BO: Shape of the training set excerpt {np.shape(X_train_excerpt)}")
            
            def get_kernel(params, latent_dimension):
                params_ = deepcopy(dict(params))
                kernel_name = params_.pop('kernel')
                kernel_kwargs = params_.pop('kernel_kwargs')
                gp_kernel = kern_catalog[kernel_name](input_dim=latent_dimension,**kernel_kwargs)
                return params_, gp_kernel
            
            bo_params_ = deepcopy(dict(bo_params))
            if ext_gp_method is not None:
                ext_gp_params_ = deepcopy(dict(ext_gp_params))
                if 'kernel' in ext_gp_params_:
                    ext_gp_params_, ext_gp_kernel = get_kernel(ext_gp_params, latent_dimension)
                ext_gp_model = gp_catalog[ext_gp_method](kernel=ext_gp_kernel, **ext_gp_params_)
                print(f"\nBO is executed with following parameters: {bo_params_} \nand using external surrogate model for BO: {ext_gp_model} with parameters {ext_gp_params} with kernel {ext_gp_kernel}.")
            else:
                if 'kernel' in bo_params_:
                    bo_params_, gp_kernel = get_kernel(bo_params_, latent_dimension)
                    bo_params_['kernel'] = gp_kernel
                print(f"BO is executed with following parameters: {bo_params_} with kernel {gp_kernel}.")
               
            # run BO for #bo_num_iter iterations
            valid_smiles = [] # SMILES and property values of all valid molecules found during optimization (chronologically)
            latent_vectors_bo = []
            history = []
            look_up_predictions = {}
            look_up_validities = {}
            bo_start_time = time.time()
            stopping_criterion_reached = False
            for bo_iter in range(bo_num_iter):
                print(f"\nBO: starting {bo_iter} iteration")
                
                # dimension reduction
                if dim_reduction:
                    if (fix_dim_reduction == False) and (bo_iter != 0):
                        # make sure dimension of PCA stays constant during optimization (only important in case when n_components < 1 to determine n_components automatically)
                        dim_reduction_params_ = deepcopy(dict(dim_reduction_params))
                        dim_reduction_params_['n_components'] = latent_dimension
                        dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params_)
                        # refit dimension reduction method
                        dim_reduction.fit(X_all_excerpt)
                        expl_var_r = dim_reduction.explained_variance_ratio_
                        logger(f"\n\t----- BO: Dim reduction by PCA refitted in BO iteration {bo_iter}. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                        logger(f"Components: {dim_reduction.n_components_}, Total explained variance: {np.sum(expl_var_r)}")
                    X_train_excerpt_low = dim_reduction.transform(X_train_excerpt)
                    X_test_low = dim_reduction.transform(X_test)
                    X_all_low = dim_reduction.transform(X_all)
                else:
                    X_train_excerpt_low = X_train_excerpt
                    X_test_low = X_test
                    X_all_low = X_all
                    
                print(f"BO: Shape of the training set for GP X: {np.shape(X_train_excerpt_low)}, y: {np.shape(y_train_excerpt)}")
                # number of training points can be limited by num of inducing points for sparse GPs
                try:
                    num_ind_pts = bo_params_['num_inducing']
                    if bo_params_['sparse'] == True:
                        if num_ind_pts < np.shape(X_train_excerpt_low)[0]: print(f"BO: Number of training points for sparse GP is limited to: {num_ind_pts} which will be randomly selected from the training data.")
                except:
                    pass
                
                space = [
                    {
                        'name': f'x{each_idx}',
                        'type': 'continuous',
                        'domain': (
                            np.min(X_all_low[:, each_idx]) - (min_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx])),
                            np.max(X_all_low[:, each_idx]) + (max_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx]))
                        )
                    }
                    for each_idx in range(X_all_low.shape[1])
                ]
                

                
                iter_bo_time_t0 = time.time()
                if ext_gp_method is not None:
                    ext_gp_model.updateModel(X_train_excerpt_low, y_train_excerpt, None, None)
                    logger(f"BO: External GP model used - GP model updated.")
                    trans_space =  GPyOpt.core.task.space.Design_space(space, None)
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None, 
                        model=ext_gp_model,
                        acquisition=GPyOpt.acquisitions.EI.AcquisitionEI(
                            model=ext_gp_model, 
                            space=trans_space, 
                            optimizer=GPyOpt.optimization.acquisition_optimizer.AcquisitionOptimizer(trans_space, 'lfbgs'), 
                            cost_withGradients=GPyOpt.core.task.cost.CostModel(None).cost_withGradients, 
                            jitter=0.2),
                        domain=space, 
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )

                    # check ll and rmse on test set
                    test_pred, test_std = ext_gp_model.predict(X_test_low, False)
                    test_err = np.sqrt(np.mean((test_pred - y_test)**2))
                    test_ll = np.mean(sp.stats.norm.logpdf(test_pred - y_test,
                                                           scale=test_std))

                    # check ll and rmse on training set
                    train_pred, train_std = ext_gp_model.predict(X_train_excerpt_low, False)
                    train_err = np.sqrt(np.mean((train_pred - y_train_excerpt)**2))
                    train_ll = np.mean(sp.stats.norm.logpdf(train_pred - y_train_excerpt,
                                                            scale=train_std))
                    logger(f"BO: train_err: {train_err}\t train_ll: {train_ll}")
                    if valid_test:
                        logger(f"BO: test_err: {test_err}\t test_ll: {test_ll}")
                
                else:
                    # initialize BO, GP is initialized each time
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None,
                        domain=space,
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )
                
                # BO suggests next points to evaluate 
                X_next_low = bo_step.suggest_next_locations(ignored_X=X_train_excerpt_low)
                
                iter_bo_time_t1 = time.time()
                print(f"BO time needed for suggesting next locations (including initialization): {iter_bo_time_t1-iter_bo_time_t0:.1f} seconds.\n")
                
                # inverse dimension reduciton
                if dim_reduction:
                    X_next = dim_reduction.inverse_transform(X_next_low)
                else:
                    X_next = X_next_low

                # evaluate suggested points
                valid_smiles_current_batch = []
                num_uni_smiles_current_batch = []
                run_times_current_batch = []
                new_feature_array = []
                num_mol = len(valid_smiles)
                score_list = []
                raw_score_list = []
                for i in range(len(X_next)):
                    ### DECODING of the latent vectors by generative model ###
                    
                    # get smiles string from latent vector
                    latent_vector = X_next[i].flatten()
                    # store latent vectors
                    latent_vectors_bo.append(latent_vector.tolist())
                    
                     # get smiles string from parameters
                    # note that there long decoding times can occur
                    # restrict decoding time -> func-timeout)
                    @timeout(TIME_LIMIT_DECODING)
                    def get_smiles(new_x, molgan_model, tfsession):
                        smiles = get_decoded_smiles(new_x, molgan_model, tfsession, data)[0]
                        return smiles

                    smiles = get_smiles(latent_vector, molgan_model, session)
                    print(f"BO: Found following SMILES string: {smiles}\n")
                    
                    if smiles is not None and ('.' in smiles or '*' in smiles):
                        smiles = None
                    
                    if smiles is not None:
                        smiles = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(smiles)) # make sure that canonical SMILES are stored
                        valid_smiles_current_batch.append(smiles)
                        new_feature_array.append(X_next[i])
                        valid = True
                        # compute target score for the current molecule
                        if CONSTRAINED:
                            # only compute target score if the molecule is similar to the training data
                            if smiles in look_up_validities:
                                prediction_validity_score = look_up_validities[smiles]
                            else: 
                                prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                                look_up_validities[smiles] = prediction_validity_score
                            if prediction_validity_score == 1:
                                if smiles in look_up_predictions:
                                    predicted_target_value = look_up_predictions[smiles]
                                else:
                                    predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                    look_up_predictions[smiles] = predicted_target_value
                            elif prediction_validity_score == -1:
                                valid = False
                                predicted_target_value = -1000
                                print(f"Non valid prediction for {smiles}")
                            else:
                                raise ValueError("Validity class not valid.")
                        else: # no constrains on the applicability of the metric
                            if smiles in look_up_predictions:
                                predicted_target_value = look_up_predictions[smiles]
                            else:
                                predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                look_up_predictions[smiles] = predicted_target_value
                        if valid: 
                            valid_smiles.append((smiles,predicted_target_value))
                        raw_score_list.append(predicted_target_value)
                        # possibly recompute target value
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused
                    else:
                        new_feature_array.append(X_next[i])
                        predicted_target_value = -1000
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused

                    # current run time of optimization
                    current_time = time.time()
                    run_time = current_time - bo_start_time
                    run_times_current_batch.append(run_time)
                    
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    num_uni_smiles_current_batch.append(num_unique_mols)
                        
                    ## STOPPING CRITERIA
                    # stop BO if desired number of molecules is reached
                    stopping_criterion_active = False
                    if MAX_NUM_MOL:
                        stopping_criterion_active = True
                        print(f"---> Current number of unique molecules {num_unique_mols}.<---")
                        if num_unique_mols >= MAX_NUM_MOL:
                            logger(f'\n\t---> {MAX_NUM_MOL} unique molecules identified. BO stopped.<---')
                            stopping_criterion_reached = True
                            break
                    # stop BO after predefined time
                    if MAX_OPT_TIME:
                        if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                        stopping_criterion_active = True
                        if run_time >= MAX_OPT_TIME:
                            logger(f'\n\t---> {MAX_OPT_TIME} seconds optimization time reached. BO stopped. <---')
                            stopping_criterion_reached = True
                            break
                    # No stopping criteria active
                    if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')

                if new_feature_array != []:
                    new_feature_array = np.vstack(new_feature_array)
                logger(f"BO: {len(valid_smiles_current_batch)} molecules are found")
                
                for each_idx, each_mol in enumerate(valid_smiles_current_batch):
                    logger(f"BO: smiles: {each_mol}\n\t {TARGET}: {raw_score_list[each_idx]}")
                    
                print(score_list)

                if len(new_feature_array) > 0:
                    X_train_excerpt = np.concatenate([X_train_excerpt, new_feature_array], 0)
                    y_train_excerpt = np.concatenate([y_train_excerpt, np.array(score_list)[:, None]], 0)
                    X_all_excerpt = np.concatenate([X_all_excerpt, new_feature_array], 0) # required for dimension reduction refit if dim_reduction with fix_dimension_reduction is False

                # save list of all smiles after each iteration
                path_smiles = [
                    [
                        valid_smiles_current_batch[tmp_idx], 
                        raw_score_list[tmp_idx], 
                        bo_iter + 1, 
                        num_mol + tmp_idx + 1,
                        run_times_current_batch[tmp_idx],
                        num_uni_smiles_current_batch[tmp_idx]
                    ] 
                    for tmp_idx, _ in enumerate(valid_smiles_current_batch)
                ]
                
                with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                    writer = csv.writer(f_table)
                    writer.writerows(path_smiles)
                    
                if ext_gp_method is not None:
                    history.append({'mol_list': valid_smiles_current_batch, 'feature': new_feature_array, 'score_list': score_list,
                                    'train_err': train_err, 'train_ll': train_ll,
                                    'test_err': test_err if valid_test else np.nan,
                                    'test_ll': test_ll if valid_test else np.nan})
                else:
                    history.append({'mol_list': valid_smiles_current_batch, 'feature': new_feature_array, 'score_list': score_list})               
                    
                # check if stopping criterion was reached in current batch
                if stopping_criterion_reached: break

            print("\n####################### Finished BO with GPyOpt #######################")
            return history, valid_smiles, latent_vectors_bo
        
        def generate_initial_data_combustion(X, y, num_train_samples=500, num_test_samples=100, permute=True):

            # how many datapoints?
            n = X.shape[0]
            if(num_train_samples==-1):
                num_train_samples=n
            breakpoint_train = min(np.int(np.round(0.9 * n)),num_train_samples)
            breakpoint_test = max(np.int(np.round(0.1 * n)), min(n - num_train_samples, num_test_samples))

            # select datapoints
            if permute:
                permutation = torch.from_numpy(np.random.choice(n, n, replace=False))
                X_train = X[permutation, :][0: breakpoint_train, :]
                y_train = y[permutation][0: breakpoint_train]
                X_test = X[permutation, :][breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[permutation][breakpoint_train: breakpoint_train + breakpoint_test]
            else:
                X_train = X[0: breakpoint_train, :]
                y_train = y[0: breakpoint_train]
                X_test = X[breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[breakpoint_train: breakpoint_train + breakpoint_test]

            best_observed_value = np.max(y_train)

            return X_train, y_train, X_test, y_test, best_observed_value
        
        ############ Generate initial training data for BO ##############
        X_all, y_all, y_raw, valid_mask = self.requires()[1].load_output()        
        if (len(valid_mask) != len(X_all)) or (len(valid_mask) != len(y_all)):
            raise ValueError(f"WARNING: Validity mask does not match number of data points.")
        X_all = np.array(X_all)[valid_mask]
        y_all = np.array(y_all)[valid_mask]

        # call helper functions to generate initial training data
        X_train, y_train, X_test, y_test, best_observed_value_nei = generate_initial_data_combustion(X=X_all, y=y_all, num_train_samples=self.BayesianOptimization_params['training_params']['num_train_samples'], num_test_samples=self.BayesianOptimization_params['training_params']['num_test_samples'])
        # save initial points
        with gzip.open(f"{os.path.dirname(self.output().path)}/BO_initial.pklz", 'wb') as f1:
            pickle.dump(
                {
                    'X_train': X_train, 
                    'y_train': y_train, 
                    'X_test': X_test, 
                    'y_test': y_test, 
                    'best_initial_value': best_observed_value_nei
                }, 
                f1
            )
            
        ############## RUN BO with GPyOpt ##############
        # BO parameters
        bo_params = BayesianOptimization_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: bo_params['run_params']['bo_num_iter'] = int(sys.maxsize)
        bo_run_params = bo_params['run_params']
        # save bo_params
        with open(f"{os.path.dirname(self.output().path)}/BO_params.json", 'a+') as fp:
            json.dump(bo_params, fp)
        history, valid_smiles, latent_vectors = run_gpyopt(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, X_all=X_all, y_all=y_all, **bo_run_params)
        print("Finished running the optimization. Now saving the output.")

        ## SAVE RESULTS
        # save history
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'wb') as f:
            pickle.dump(history, f)
        # save valid smiles as pklz
        save_object(valid_smiles, self.output().path)
        # save valid smiles as txt
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
        
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors, l_f)  
        

    def load_output(self, ret_history=False):
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'rb') as f:
            history = pickle.load(f)
        output_valid_smiles = load_object(self.output().path)
        if ret_history: return history
        print(output_valid_smiles)
        return output_valid_smiles
    


class MultipleBO(MainTask, AutoNamingTask):
    '''
    Reapeating BO for seeds in seed_list (or one time execution if single_seed is set)
    '''
    
    # internal params accessed from INPUT/param.py
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    MultipleBO_params = luigi.DictParameter(default=MultipleBO_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    molgan_target = luigi.Parameter(default=None) # defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleBO") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
   
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target: 
            self.target = self.MolOpt_params['target']
            
        # special case: allow user to set different target property for MolGAN training than target property for optimization
        if not self.molgan_target:
            self.molgan_target = self.target
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {self.max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print(f'Stopping criteria may be conflicting:\n1. finding {self.max_num_mols} unique molecules.\n2. Maximum optimization time {self.max_opt_time} sec.\nThus maximum optimization time is choosen.')
        
        # allow user to set whether BO is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.BayesianOptimization_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleBO_params['seed_list']
            
        return [
            BayesianOptimization(
                MolGAN_params=self.MolGAN_params, 
                BayesianOptimization_params=self.BayesianOptimization_params, 
                target=self.target,
                molgan_target=self.molgan_target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/BO_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model
            )
            for each_seed in seed_list
        ]       
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            BO_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    MolGAN_params=self.MolGAN_params,
                    working_subdir=each_BO_run.working_subdir,
                    valid_smiles=each_BO_run.load_output()
                )
                for each_BO_run in BO_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []
    

class GeneticAlgorithm(AutoNamingTask):
    '''
    Genetic algorithm
    '''
    constrained = luigi.BoolParameter(default=GeneticAlgorithm_params['target_params']['constrained'])
    
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    molgan_target = luigi.Parameter(default=None)
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="GA")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False   # helper variable to avoid multiple renaming of working subdir when GA with applicability domain constraint is executed
    
    def requires(self):
        # molgan_target defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
        if not self.molgan_target: self.molgan_target = self.target
        return [
            TrainingMolGAN(
                MolGAN_params=self.MolGAN_params,
                target=self.molgan_target,
                use_gpu=self.use_gpu
            ),
            GenerateLatentFiles(
                MolGAN_params=self.MolGAN_params,
                target=self.target,
                molgan_target=self.molgan_target,
                use_gpu=self.use_gpu
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            # when GA with AD constraint is executed add this information to name of working subdir
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        # additional packages required for GA
        import geneticalgorithm.geneticalgorithm as ga
        
        # reset tf graph 
        tf.reset_default_graph()
        
        # helper class to stop GA when maximum number of molecules is reached by raising Error that can be catched
        class StoppingCriterionReachedError(Exception):
            '''
            Raised when maximum number of molecules is reached
            '''
            pass
        
                
        ############ INITIALIZE MolGAN MODEL #############
        # provide the TRAINED model path
        model_path = os.path.dirname(self.requires()[0].load_output())
        z_dim = MolGAN_params['model']['latent_size'] 
        
        # set seed
        seed_everything(self.seed)
        
                # start session for MolGAN (BO does not use tensorflow)
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True # do not fill the whole GPU but take only what you need
        session = tf.Session(config=config)
        session.run(tf.global_variables_initializer())

        # load data for optimization
        data = SparseMolecularDataset()
        training_data_path = os.path.join('INPUT', self.MolGAN_params['training']['data'])
        data.load(training_data_path)

        # define the model 
        molgan_model = models.gan.GraphGANModel(data.vertexes,
                              data.bond_num_types,
                              data.atom_num_types,
                              z_dim,
                              decoder_units=(128, 256, 512),
                              discriminator_units=((128, 64), 128, (128, 64)),
                              decoder=models.decoder_adj,
                              discriminator=models.encoder_rgcn,
                              soft_gumbel_softmax=False,
                              hard_gumbel_softmax=False,
                              batch_discriminator=False)

        # fill the model with life (i.e. load the parameters)
        load_model_molgan(model_path, session)  
        
        # general parameters for the genetic algorithm
        TARGET = self.target
        STD_TARGET = self.GeneticAlgorithm_params['target_params']['std_target']
        print(f"\nGA: Target property {TARGET} - standardized {STD_TARGET}")
        CONSTRAINED = self.constrained
        print(f"\nGA: Constrained by applicability domain of GNN: {CONSTRAINED}")
        TIME_LIMIT_DECODING = self.MolGAN_params['inference']['time_limit_decoding']
        print(f"\nGA: Time limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nGA: Optimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nGA: Optimization will be stopped after {MAX_OPT_TIME} seconds.")

        # global list within GeneticAlgorithm with SMILES and property values of all valid molecules found during optimization (chronologically)
        valid_smiles = []
        obj_fun_calls = []
        latent_vectors_ga = []
        look_up_predictions = {}
        look_up_validities = {}
      
        # objective function for ga
        def objective(new_x):
            latent_vectors_ga.append(new_x)
            
            # number of objective function calls
            obj_fun_calls.append(1)
            tmp_calls = len(obj_fun_calls)
            ga_iter = (tmp_calls - 1) // GeneticAlgorithm_params['algorithm_param']['population_size']
            
            # get current run time
            current_time = time.time()
            run_time = current_time - GA_START_TIME
            
            # get current number of unique molecules identified
            num_unique_mols = len(list(dict(valid_smiles).items()))
            
            ## STOPPING CRITERIA
            # stop BO if desired number of molecules is reached
            # TODO: decide if we want only count unique smiles -> vorzeitig, unterschiedlich, Moeglichkeit: -1 (keine Grenze), zeitlich Performance (target value) plotten ueber #Molekuels 
            stopping_criterion_active = False
            if MAX_NUM_MOL:
                stopping_criterion_active = True
                if num_unique_mols >= MAX_NUM_MOL:
                    print(f'\n---> {MAX_NUM_MOL} unique molecules identified. GA stopped.<---')
                    raise StoppingCriterionReachedError
            # stop BO after predefined time
            if MAX_OPT_TIME:
                if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                stopping_criterion_active = True
                if run_time >= MAX_OPT_TIME:
                    print(f'\n---> {MAX_OPT_TIME} seconds optimization time reached. GA stopped. <---')
                    raise StoppingCriterionReachedError
            # No stopping criteria active
            if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')
            
            # get smiles string from parameters
            # note that there long decoding times can occur
            # restrict decoding time -> func-timeout)
            print(TIME_LIMIT_DECODING)
            @timeout(TIME_LIMIT_DECODING)
            def get_smiles(new_x, molgan_model, tfsession):
                smiles = get_decoded_smiles(new_x, molgan_model, tfsession, data)[0]
                return smiles

            smiles = get_smiles(new_x, molgan_model, session)
            print(f"GA: Found following SMILES string: {smiles}\n")
            
            # Filter out valid smiles but invalid single molecules
            if smiles is not None and ('.' in smiles or '*' in smiles):
                smiles = None
            
            # evaluate the decoded molecule
            if smiles is not None:
                valid = True
                # compute target score for the current molecule
                if CONSTRAINED:
                    #if self.pred_model == 'CRaWl':
                    #    raise NotImplementedError('constrained for CRaWl not implemented yet.')
                    # only compute target score if the molecule is similar to the training data
                    if smiles in look_up_validities:
                        prediction_validity_score = look_up_validities[smiles]
                    else: 
                        prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                        look_up_validities[smiles] = prediction_validity_score
                    if prediction_validity_score == 1:
                        if smiles in look_up_predictions:
                            print("already predicted property for this smiles.")
                            predicted_target_value = look_up_predictions[smiles]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                            look_up_predictions[smiles] = predicted_target_value
                    elif prediction_validity_score == -1:
                        predicted_target_value = -1000
                        valid = False
                        print(f"Non valid prediction for {smiles}")
                    else:
                        valid = False
                        raise ValueError("Validity class not valid.")
                else: # no constrains on the applicability of the metric
                    if smiles in look_up_predictions:
                        print("already predicted property for this smiles.")
                        predicted_target_value = look_up_predictions[smiles]
                    else:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                        look_up_predictions[smiles] = predicted_target_value
                    

                if valid: # valid can be false if the constraints are not satisfied.
                    
                    # always store with raw predicted value
                    valid_smiles.append((smiles,predicted_target_value)) # valid_smiles is global!
                    
                    # save results - NOTE: we need to save here bc GA does not have explicit loop
                    # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                    num_mol = len(valid_smiles)
                    # get current number of unique molecules identified
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    path_smiles = [
                        smiles, 
                        predicted_target_value, 
                        ga_iter + 1, 
                        num_mol, 
                        run_time,
                        num_unique_mols
                    ]
                    
                    with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                        writer = csv.writer(f_table)
                        writer.writerow(path_smiles)
                        
                    # possibly recompute target value
                    if STD_TARGET:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, std_score=STD_TARGET)[0]
                print(f"Predicted value for GA: {-predicted_target_value}")
                return -predicted_target_value # return negative value bc ga minimizes
            else: # no molecule returned
                print("returned invalid")
                return 1000
        
        # set up params for GA
        X_all, _ = self.requires()[1].load_output() # load latent vectors
        print(X_all.shape)
        X_min = np.min(X_all , axis=0) - (self.GeneticAlgorithm_params['run_params']['min_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        X_max = np.max(X_all , axis=0) + (self.GeneticAlgorithm_params['run_params']['max_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        varbound = np.transpose([X_min, X_max])
        print(varbound)
        
        # GA parameters
        ga_params = GeneticAlgorithm_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: ga_params['algorithm_param']['max_num_iteration'] = int(sys.maxsize - 1)
        ga_algorithm_params = ga_params['algorithm_param']
        with open(f"{os.path.dirname(self.output().path)}/GA_params.json", 'a+') as fp:
            json.dump(ga_params, fp)

        
        # initialize GA
        ga_model = ga(
            function=objective,
            dimension=self.MolGAN_params['model']['latent_size'],
            variable_type='real',
            variable_boundaries=varbound,
            algorithm_parameters=ga_algorithm_params,
            function_timeout=1000,
        )
        
        # start the genetic algorithm (while measuring the runtime)
        print("\n####################### Start GA. #######################")
        
        GA_START_TIME = time.time()
        try:
            ga_model.run()
        except StoppingCriterionReachedError:
            print(f"Maximum number of molecules ({MAX_NUM_MOL}) is reached. GA stopped.")
        ga_end_time = time.time()
        
        print("\n####################### GA finished. #######################")
        
        save_object(valid_smiles, self.output().path)
        # create vobaculary and save it in the same dir as the training data
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
                
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors_ga, l_f)

        print("----------------------------------- RESULTS FOR PARAMS: -----------------------------------")
        print(ga_algorithm_params)
        print(f"\nGA: Total time for running: {ga_end_time - GA_START_TIME}.")
        

    def load_output(self):
        output_valid_smiles = load_object(self.output().path)
        return output_valid_smiles
    
class MultipleGA(MainTask, AutoNamingTask):
    '''
    Reapeating GA for seeds in seed_list (or one time execution if single_seed is set)
    '''
    
    # internal params accessed from INPUT/param.py
    MolGAN_params = luigi.DictParameter(default=MolGAN_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    MultipleGA_params = luigi.Parameter(default=MultipleGA_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    molgan_target = luigi.Parameter(default=None) # defines the target property the molgan model is trained on (this is typically the same as the target for later optimization with BO/GA)
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleGA") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted    
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
    
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # special case: allow user to set different target property for MolGAN training than target property for optimization
        if not self.molgan_target:
            self.molgan_target = self.target
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {self.max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print(f'Stopping criteria may be conflicting:\n1. finding {self.max_num_mols} unique molecules.\n2. Maximum optimization time {self.max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether GA is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.GeneticAlgorithm_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleGA_params['seed_list']
            
        # requirements (previous jobs to be executed)
        return [
            GeneticAlgorithm(
                MolGAN_params=self.MolGAN_params, 
                GeneticAlgorithm_params=self.GeneticAlgorithm_params, 
                target=self.target,
                molgan_target=self.molgan_target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/GA_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            GA_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    MolGAN_params=self.MolGAN_params,
                    working_subdir=each_GA_run.working_subdir,
                    valid_smiles=each_GA_run.load_output()
                )
                for each_GA_run in GA_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []
    
    

if __name__ == "__main__":
    for each_engine_status in glob.glob("./engine_status.*"):
        os.remove(each_engine_status)
    with open("engine_status.ready", "w") as f:
        f.write("ready: {}\n".format(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')))

    print("starting now.")
    main() # luigine.main()
    print("fertig")
