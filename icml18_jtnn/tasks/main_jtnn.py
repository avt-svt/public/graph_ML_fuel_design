#!/usr/bin/env python
# -*- coding: utf-8 -*-

#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/graph_ML_fuel_design.git
#
#*********************************************************************************

# original author and copyright
__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "Jan 24 2018"
# adapted by Stefanie Winkler, Jan Rittig, and Martin Ritzert

# set luigi_config_path BEFORE importing luigi
import argparse
import os
import sys
sys.path.append('..')
sys.path.append('../..')

os.environ['CUDA_VISIBLE_DEVICES'] = '1'


# import before new working dir is set
import jtnn
# utils needed for predictions, AD, saving/loading
from utils.molecular_metrics import MolecularMetrics
from utils.prediction_validity import PredictionValidity 
from utils.utils_base import save_object, load_object

# set up working dir
try:
    working_dir = sys.argv[1:][sys.argv[1:].index("--working-dir") + 1]
    os.chdir(working_dir)
except ValueError:
    raise argparse.ArgumentError("--working-dir option must be specified.")
# add a path to luigi.cfg
os.environ["LUIGI_CONFIG_PATH"] = os.path.abspath(os.path.join("INPUT", "luigi.cfg"))
sys.path.append(os.path.abspath(os.path.join("INPUT")))



# imports
import rdkit
from torch.utils.data import DataLoader
from torch.autograd import Variable
from copy import deepcopy
import glob
import gzip
import luigi
import logging
import numpy as np
import pickle
import torch
import traceback
import math
import random
import faulthandler; faulthandler.enable()
import json

# concurrency needed to abort long-running decoding processes
import time
import datetime
import concurrent.futures as futures

# TODO: check hyperparameters
from param import JTNN_params, MolOpt_params, BayesianOptimization_params, GeneticAlgorithm_params, RandomSearch_params, MultipleGA_params, MultipleBO_params, MultipleRandomSearch_params

# Luigine is an extension of Luigi so simplify naming the files
from luigine.abc import MainTask, AutoNamingTask, main

logger = logging.getLogger('luigi-interface')
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))


# ensure consistent results
def seed_everything(seed=1234):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
seed_everything(seed=int(JTNN_params['training']['seed']))

############### HELPER FUNCTIONS for OPTIMIZATION #################

# restores a model (no return value)
'''
def load_model_jtnn(jtnn_model, model_path, device):
    jtnn_model.load_state_dict(torch.load(model_path, map_location=device))
    jtnn_model = jtnn_model.to(device)
'''

def timeout(timelimit, verbose=True):
    '''Timeout function with timelimit.
    
    This function stops another function call after a given time limit is exceeded.
    
    Parameters
    ----------
    timelimit: float
        Time limit in seconds after which function call is stopped.
    '''
    
    def decorator(func):
        def decorated(*args, **kwargs):
            with futures.ThreadPoolExecutor(max_workers=1) as executor:
                future = executor.submit(func, *args, **kwargs)
                tmp_t0 = time.time()
                try:
                    result = future.result(timelimit)
                except (futures.TimeoutError, RuntimeError) as error:
                    if verbose: print(f"Timeout! -> Error: {error}")
                    result =  None
                else:
                    if verbose: print(result)
                    pass
                tmp_t1 = time.time()
                if verbose: print(f"\nTotal time for decoding: {tmp_t1-tmp_t0:.3f} sec, (time limit: {timelimit} sec).")
                executor._threads.clear()
                futures.thread._threads_queues.clear()
                return result
        return decorated
    return decorator

def get_decoded_smiles(new_x, model, device, prob_decode=False):
    '''Decoding a latent vector into a SMILES string with JTNN-model
    
    Parameters
    ----------
    new_x: np.array (latent_dim,)
        The latent vector to be encoded.
    model: object
        JTNN-model instance.
    decive: string
        Location ('cuda' or 'cpu') model is mapped on.
    prob_decode: boolean, optional.
        Whether decoding in JTNN-model is non-deterministic (True) or deterministic (False).
        Default is False.
    '''
    decoded_mols = []
    tree_vec, mol_vec = np.hsplit(new_x, 2)
    tree_vec = jtnn.create_var(torch.from_numpy(tree_vec).float().reshape(1,-1).to(device), device=device).to(device)
    mol_vec = jtnn.create_var(torch.from_numpy(mol_vec).float().reshape(1,-1).to(device), device=device).to(device)
    # note that not every latent vector can be converted to a valid molecule.
    smiles_mol = model.decode(tree_vec, mol_vec, prob_decode=prob_decode)
    decoded_mols.append(smiles_mol)
    return decoded_mols

# Generate vocabulary
class DataPreprocessing(AutoNamingTask):
    '''Data Preprocessing JTNN
    
    This tasks preprocesses data for JTNN and extracts vocabulary.
    '''
                
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="preporcess_data")
    output_ext = luigi.Parameter(default='txt')
    output_name = 'vocab'
                
    def requires(self):
        return []
    
    # overwrite output function to define output file name
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))
                
    def run(self):
                
        lg = rdkit.RDLogger.logger() 
        lg.setLevel(rdkit.RDLogger.CRITICAL)
                
        # get path of training data
        training_data_path = os.path.join('INPUT', self.JTNN_params['training']['data'])
                
        # create vobaculary and save it in the same dir as the training data
        with open(training_data_path, 'r') as train_file, open(self.output().path, 'w+') as vocab_file:
            ''' 
            To perform tree decomposition over a set of molecules, run
            python mol_tree.py < ../data/all.txt
            This gives you the vocabulary of all cluster labels over the entire dataset.
            '''
            cset = set()
            for i,line in enumerate(train_file):
                smiles = line.split()[0]
                mol = jtnn.MolTree(smiles)
                for c in mol.nodes:
                    cset.add(c.smiles)
            for x in cset:
                vocab_file.write(f'{x}\n')
                
    def load_output(self):
        print(f"Use vocab from: {self.output().path}")
        return [x.strip("\r\n ") for x in open(self.output().path)]
    

class PretrainingJTNN(AutoNamingTask):
    '''Pretraining JTNN
    
    This tasks pretrains a JTNN model.
    '''
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="pretrain_jtnn")
    output_ext = luigi.Parameter(default='')
    
    def requires(self):
        return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to define meaningful output file names
    def output(self):
        self.output_name = f"model.iter-{int(self.JTNN_params['pretraining']['max_epoch']-1)}"  #JR: JTNN saves from 0,..., max_epoch-1 
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}".format(self.output_name)))
        
        
    def run(self):
        VERBOSE = bool(self.JTNN_params['model']['verbose'])
        
        # provide the preprocessed vocabulary
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
        if VERBOSE: print(f"Vocab for JTNN training loaded. Examples {vocab_list[:10]}")
        
        # set SEED again, this may be redundant
        training_seed = int(self.JTNN_params['training']['seed'])
        seed_everything(training_seed)   # set an universal training seed and use different seeds for 
        if VERBOSE: print(f"Training seed set to {training_seed}")
        
        lg = rdkit.RDLogger.logger() 
        lg.setLevel(rdkit.RDLogger.CRITICAL)

        hidden_size = int(self.JTNN_params['model']['hidden_size'])
        latent_size = int(self.JTNN_params['model']['latent_size'])
        depth = int(self.JTNN_params['model']['depth'])
        batch_size = int(self.JTNN_params['training']['batch_size'])
        if VERBOSE: print(f"JTNN training hyperparameters: hidden_size {hidden_size}, latent_size {latent_size}, depth {depth}, batch_size {batch_size}")
        
        training_data_path = os.path.join('INPUT', self.JTNN_params['training']['data'])
        if VERBOSE: print(f"JTNN trained with data from {training_data_path}")

        model = jtnn.JTNNVAE(vocab, hidden_size, latent_size, depth)

        for param in model.parameters():
            if param.dim() == 1:
                torch.nn.init.constant(param, 0)
            else:
                torch.nn.init.xavier_normal(param)

        if self.use_gpu and torch.cuda.is_available():
            model.cuda()
            if VERBOSE: print("Model on cuda.")
        print ("Model #Params: %dK" % (sum([x.nelement() for x in model.parameters()]) / 1000,))
               
        optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.9)
        scheduler.step()

        dataset = jtnn.MoleculeDataset(training_data_path)

        MAX_EPOCH = int(self.JTNN_params['pretraining']['max_epoch'])
        PRINT_ITER = int(self.JTNN_params['pretraining']['print_iter'])

        # pretrain JTNN
        for epoch in range(MAX_EPOCH):
            dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=4, collate_fn=lambda x:x, drop_last=True)#, worker_init_fn=training_seed) # added seed

            word_acc,topo_acc,assm_acc,steo_acc = 0,0,0,0

            for it, batch in enumerate(dataloader):
                for mol_tree in batch:
                    for node in mol_tree.nodes:
                        if node.label not in node.cands:
                            node.cands.append(node.label)
                            node.cand_mols.append(node.label_mol)

                model.zero_grad()
                loss, kl_div, wacc, tacc, sacc, dacc = model(batch, beta=0)
                loss.backward()
                optimizer.step()

                word_acc += wacc
                topo_acc += tacc
                assm_acc += sacc
                steo_acc += dacc

                if (it + 1) % PRINT_ITER == 0:
                    word_acc = word_acc / PRINT_ITER * 100
                    topo_acc = topo_acc / PRINT_ITER * 100
                    assm_acc = assm_acc / PRINT_ITER * 100
                    steo_acc = steo_acc / PRINT_ITER * 100

                    print ("KL: %.1f, Word: %.2f, Topo: %.2f, Assm: %.2f, Steo: %.2f" % (kl_div, word_acc, topo_acc, assm_acc, steo_acc))
                    word_acc,topo_acc,assm_acc,steo_acc = 0,0,0,0
                    sys.stdout.flush()

            scheduler.step()
            print ("learning rate: %.6f" % scheduler.get_lr()[0])
            torch.save(model.state_dict(), f"{os.path.dirname(self.output().path)}/model.iter-{epoch}")
            #with gzip.open(self.output().path, 'wb') as f:
            #    pickle.dump((X_all, y_all, y_raw), f)
            #print(self.output().path)

    def load_output(self):
        return self.output().path

class TrainingJTNN(AutoNamingTask):
    '''Training JTNN
    
    This tasks loads a pretrained JTNN and trains it.
    '''
                
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="train_jtnn")
    output_ext = luigi.Parameter(default='')
    
    def requires(self):
        return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            PretrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            )
        ]
    
    def output(self):
        self.output_name = f"model.iter-{int(self.JTNN_params['training']['max_epoch']-1)}" #JR: JTNN saves from 0,..., max_epoch-1
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}".format(self.output_name)))
        
        
    def run(self):
        VERBOSE = bool(self.JTNN_params['model']['verbose'])
        
        # provide the preprocessed vocabulary
        #vocab_path = self.requires()[0].load_output()
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
        if VERBOSE: print(f"Vocab for JTNN training loaded. Examples {vocab_list[:10]}")
                
        # provide the pretrained model path
        model_path =  self.requires()[1].load_output()
                
        # set SEED again, this may be redundant
        training_seed = int(self.JTNN_params['training']['seed'])
        seed_everything(training_seed)   # set an universal training seed and use different seeds for 
        if VERBOSE: print(f"Training seed set to {training_seed}")

        lg = rdkit.RDLogger.logger() 
        lg.setLevel(rdkit.RDLogger.CRITICAL)
                
        hidden_size = int(self.JTNN_params['model']['hidden_size'])
        latent_size = int(self.JTNN_params['model']['latent_size'])
        depth = int(self.JTNN_params['model']['depth'])
        beta = self.JTNN_params['training']['beta']
        lr = self.JTNN_params['training']['lr']
        batch_size = int(self.JTNN_params['training']['batch_size'])
        stereo = self.JTNN_params['training']['stereo']
        stereo = True if int(stereo) == 1 else False      
        if VERBOSE: print(f"JTNN training hyperparameters: hidden_size {hidden_size}, latent_size {latent_size}, depth {depth}, batch_size {batch_size}, stereo {stereo}, beta {beta}, lr {lr}")
                
        training_data_path = os.path.join('INPUT', self.JTNN_params['training']['data'])
        if VERBOSE: print(f"JTNN trained with data from {training_data_path}")

        model = jtnn.JTNNVAE(vocab, hidden_size, latent_size, depth, stereo=stereo)

        # model from pretraining is loaded
        if model_path is not None:
            model.load_state_dict(torch.load(model_path))
        else:
            for param in model.parameters():
                if param.dim() == 1:
                    torch.nn.init.constant(param, 0)
                else:
                    torch.nn.init.xavier_normal(param)
        if VERBOSE: print(f"JTNN model loaded for training from {model_path}")

        if self.use_gpu and torch.cuda.is_available():
            model.cuda()
            if VERBOSE: print("Model on cuda.")
        print ("Model #Params: %dK" % (sum([x.nelement() for x in model.parameters()]) / 1000,))

        optimizer = torch.optim.Adam(model.parameters(), lr=lr)
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.9)
        scheduler.step()

        dataset = jtnn.MoleculeDataset(training_data_path)

        MAX_EPOCH = int(self.JTNN_params['training']['max_epoch'])
        PRINT_ITER = int(self.JTNN_params['training']['print_iter'])

        for epoch in range(MAX_EPOCH):
            dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=4, collate_fn=lambda x:x, drop_last=True) #, worrker_init_fn=training_seed) # added seed

            word_acc,topo_acc,assm_acc,steo_acc = 0,0,0,0

            for it, batch in enumerate(dataloader):
                for mol_tree in batch:
                    for node in mol_tree.nodes:
                        if node.label not in node.cands:
                            node.cands.append(node.label)
                            node.cand_mols.append(node.label_mol)

                try:
                    model.zero_grad()
                    loss, kl_div, wacc, tacc, sacc, dacc = model(batch, beta)
                    loss.backward()
                    optimizer.step()
                except Exception as e:
                    print (e)
                    continue

                word_acc += wacc
                topo_acc += tacc
                assm_acc += sacc
                steo_acc += dacc

                if (it + 1) % PRINT_ITER == 0:
                    word_acc = word_acc / PRINT_ITER * 100
                    topo_acc = topo_acc / PRINT_ITER * 100
                    assm_acc = assm_acc / PRINT_ITER * 100
                    steo_acc = steo_acc / PRINT_ITER * 100

                    print ("KL: %.1f, Word: %.2f, Topo: %.2f, Assm: %.2f, Steo: %.2f" % (kl_div, word_acc, topo_acc, assm_acc, steo_acc))
                    word_acc,topo_acc,assm_acc,steo_acc = 0,0,0,0
                    sys.stdout.flush()

                if (it + 1) % 15000 == 0: #Fast annealing
                    scheduler.step()
                    print( "learning rate: %.6f" % scheduler.get_lr()[0])

                if (it + 1) % 1000 == 0: #Fast annealing
                    torch.save(model.state_dict(), f"{os.path.dirname(self.output().path)}/model.iter-{epoch}-{it + 1}")# % (epoch + 1, it + 1))

            scheduler.step()
            print ("learning rate: %.6f" % scheduler.get_lr()[0])
            torch.save(model.state_dict(), f"{os.path.dirname(self.output().path)}/model.iter-{epoch}")
                
    def load_output(self):
        return self.output().path
    
class TrainingPipeline(MainTask, AutoNamingTask):
    '''Training Pipeline JTNN
    
    This tasks initializes the JTNN training steps.
    '''
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    use_gpu = luigi.BoolParameter()
    
    def requires(self):
        return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            PretrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            TrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            )
        ]
    
    def output(self):
        return []
    
    def run(self):
        return []

    
class GenerateLatentFiles(AutoNamingTask):
    '''Generate Latent Files Tasks
    
    This task generates latent vector embeddings of JTNN models.
    '''
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    working_subdir = luigi.Parameter(default="latent_files")
    output_name = 'latent_features'
    output_ext = 'txt'
    
    def requires(self):
         return [
            DataPreprocessing(JTNN_params=self.JTNN_params, use_gpu=self.use_gpu),
            TrainingJTNN(JTNN_params=self.JTNN_params, use_gpu=self.use_gpu),
        ]
    
    # overwrite output function to define output file name
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        import pandas as pd
        
        VERBOSE = bool(self.JTNN_params['model']['verbose'])
        
        # provide the preprocessed vocabulary
        #vocab_path = self.requires()[0].load_output()
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
                
        # provide the TRAINED model path
        model_path =  self.requires()[1].load_output()

        lg = rdkit.RDLogger.logger() 
        lg.setLevel(rdkit.RDLogger.CRITICAL)
        
        hidden_size = int(self.JTNN_params['model']['hidden_size'])
        latent_size = int(self.JTNN_params['model']['latent_size'])
        depth = int(self.JTNN_params['model']['depth'])
        if VERBOSE: print(f"JTNN training hyperparameters: hidden_size {hidden_size}, latent_size {latent_size}, depth {depth}")
                
        training_data_path = os.path.join('INPUT', self.JTNN_params['training']['data'])
        if VERBOSE: print(f"JTNN trained with data from {training_data_path}")

        batch_size = 1000
        hidden_size = int(hidden_size)
        latent_size = int(latent_size)
        depth = int(depth)

        # load trained JTNN model
        model = jtnn.JTNNVAE(vocab, hidden_size, latent_size, depth)
        model.load_state_dict(torch.load(model_path))
        if self.use_gpu and torch.cuda.is_available():
            model.cuda()
            if VERBOSE: print("Model on cuda.")
                
        # read in training data smiles      
        with open(training_data_path) as f:
            smiles = f.readlines()

        # extract latent vectors for training data smiles
        latent_points = []
        for i in range(0, len(smiles), batch_size):
            print(i)
            batch = smiles[i:i+batch_size]
            mol_vec = model.encode_latent_mean(batch)
            latent_points.append(mol_vec.data.cpu().numpy())

        # We store the results
        latent_points = np.vstack(latent_points)
        np.savetxt(self.output().path, latent_points)
        print(len(latent_points))
        
    def load_output(self):
        return np.loadtxt(self.output().path)
    
    
class ComputeTargetValues(AutoNamingTask):
    '''Compute Traget Values Tasks
    
    This task computes target values for provided molecules.
    '''

    JTNN_params = luigi.DictParameter(default=JTNN_params)
    working_subdir = luigi.Parameter(default="target_values")
    target = luigi.Parameter(default=MolOpt_params['target'])
    constrained = luigi.BoolParameter(default=False)

    def requires(self):
        return []

    def run(self):
        print(f"Calculate values for property: {self.target}")
        
        # find out which targets to compute
        perform_RON = False
        perform_MON = False
        perform_DCN = False

        # allowed values are RON, MON, DCN, RON-MON, RON+OS
        if 'DCN' in self.target:
            perform_DCN = True
        if 'MON' in self.target:
            perform_MON = True
        if 'RON' in self.target:
            perform_RON = True
        if 'OS' in self.target:
            perform_RON = True
            perform_MON = True

        # lists for raw regression data generated by the network
        dcn_list = []
        mon_list = []
        ron_list = []
        valid_mask = []
        molecule_counter = 0

        smiles_file = self.JTNN_params['training']['data']
        mol_gen = rdkit.Chem.SmilesMolSupplier(os.path.join("INPUT",smiles_file), titleLine=False)
        for each_mol in mol_gen:
            smi = [rdkit.Chem.MolToSmiles(each_mol)]
            molecule_counter = molecule_counter + 1
            valid_mask.append(True)
            
            if self.constrained:
                prediction_validity_score = PredictionValidity.validity_score(smi, "svm", model='kgnn', target=self.target)[0]
                if prediction_validity_score == -1:
                    dcn_list.append(-1000)
                    mon_list.append(-1000)
                    ron_list.append(-1000)
                    valid_mask[-1] = False
                    print(f"Invalid {smi}")
                    continue
            
            if perform_DCN: dcn_list.append(MolecularMetrics.dcn_score(smi)[0])
            if perform_MON: mon_list.append(MolecularMetrics.mon_score(smi)[0])
            if perform_RON: ron_list.append(MolecularMetrics.ron_score(smi)[0])

        # compute final target values
        y_all = 0
        if perform_DCN: y_all = y_all + np.array(dcn_list)
        if perform_MON: y_all = y_all + np.array(mon_list)
        if perform_RON: y_all = y_all + np.array(ron_list)
        # special case RON-MON
        if self.target == 'RON-MON':
            y_all = np.array(ron_list) - np.array(mon_list)
        elif self.target == 'RON+OS':
            y_all = np.array(ron_list) * 2 - np.array(mon_list)
        elif self.target == 'wRON+wOS':
            y_all = (np.array(ron_list) * (0.5/120)) + ((np.array(ron_list) - np.array(mon_list)) * (0.5/30))
        elif self.target == 'uRON+vOS':
            y_all = (np.array(ron_list) * (0.65/120)) + ((np.array(ron_list) - np.array(mon_list)) * (0.35/30))
        # store raw data
        y_raw = {'dcn_list': dcn_list, 'mon_list': mon_list, 'ron_list': ron_list} # some of them may be empty lists
        # save to disk
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((y_all, y_raw, valid_mask), f)


    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            y_all, y_raw, valid_mask = pickle.load(f)
        return y_all, y_raw, valid_mask
    
    
class ConstructDatasetForBO(AutoNamingTask):
    '''
    This task constructs a dataset for Bayesian optimization.
    ** only training set is used, which will be randomly split into train and test sets in the BO setting. **
    '''

    JTNN_params = luigi.DictParameter(default=JTNN_params)
    target = luigi.Parameter(default=MolOpt_params['target'])
    use_gpu = luigi.BoolParameter()
    constrained = luigi.BoolParameter(default=False)
    working_subdir = luigi.Parameter(default="bo_dataset")

    def requires(self):
        return [
            GenerateLatentFiles(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            ComputeTargetValues( 
                JTNN_params=self.JTNN_params,
                target=self.target,
                constrained=self.constrained,
            )
        ]
        
    def run(self):
        # load latent vectors
        X_all = self.requires()[0].load_output()

        # load target values
        y_all, y_raw, valid_mask = self.requires()[1].load_output()

        assert X_all.shape[0] == len(y_all.ravel()), 'X_all and y_all have inconsistent shapes: {}, {}'.format(X_all.shape[0], len(y_all.ravel()))
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((X_all, y_all, y_raw, valid_mask), f)

    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            X_all, y_all, y_raw, valid_mask = pickle.load(f)
        return X_all, y_all, y_raw, valid_mask
    
    
class DrawResultPlots(AutoNamingTask):
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    working_subdir = luigi.Parameter(default="result_plots")
    pred_model = luigi.Parameter(default='kgnn')
    valid_smiles = luigi.Parameter(default=None)
    output_name = luigi.Parameter(default='best_mols')
    output_ext = luigi.Parameter(default='svg') 
    
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))
    
    
    def requires(self):
        return []
        
    def run(self):
        
        def plotting_results_and_save(valid_smiles_w_scores, training_smiles):
            import rdkit.Chem.Draw.IPythonConsole
            rdkit.Chem.Draw.IPythonConsole.ipython_useSVG=True

            # make unique list of molecules and sort it wrt target property
            unique_smiles = list(dict(valid_smiles_w_scores).items())
            unique_smiles.sort(key=lambda x: x[1], reverse=True) # sort smiles wrt target value descending

            num_mols = 30
            if len(unique_smiles) <= 30: num_mols = len(unique_smiles)
            smiles_to_plot = list(zip(*unique_smiles[:num_mols]))[0] # plot only smiles corresponding to top 30 highest target values
            # returns dcn, mon, and ron scores for a list of smiles strings
            def compute_scores(smiles):
                prediction_dcn = MolecularMetrics.dcn_score(smiles=smiles, model=self.pred_model)
                prediction_ron = MolecularMetrics.ron_score(smiles=smiles, model=self.pred_model)
                prediction_mon = MolecularMetrics.mon_score(smiles=smiles, model=self.pred_model)
                return prediction_dcn, prediction_mon, prediction_ron
            dcn, mon, ron = compute_scores(smiles_to_plot)

            # check if which molecules are in training set
            training_canon_smiles = [(rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s))) for s in training_smiles]
            for i,s in enumerate(unique_smiles):
                tmp_s = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s[0]))
                tmp_v = ''
                if tmp_s in training_canon_smiles:
                    tmp_v = 'in training set'
                else:
                    tmp_v = 'not in training set'
                unique_smiles[i] = (*unique_smiles[i], tmp_v)

            # Plot
            mols = [rdkit.Chem.MolFromSmiles(s) for s in smiles_to_plot]
            vals = [f"SMILES: {smi}, \n DCN: {dcn[i]:.2f}, MON: {mon[i]:.2f}, RON: {ron[i]:.2f}, \n RON-MON: {ron[i]-mon[i]:.2f} \n RON+OS: {2*ron[i]-mon[i]:.2f}" for i, smi in enumerate(smiles_to_plot)]
            img = rdkit.Chem.Draw.MolsToGridImage(mols, molsPerRow=5, subImgSize=(300, 300), legends=vals)
            with open(self.output().path, 'w') as f_handle:
                f_handle.write(img.data)

            # Write final results to csv
            import csv
            with open(os.path.join(os.path.dirname(self.output().path), f'{self.output_name}_list.csv'), 'w', newline="") as f_table:
                writer = csv.writer(f_table)
                writer.writerows(unique_smiles)
        
        # Plot results and save to working subdir        
        # we train with HCO-qm9 so training data is equal to qm9        
        training_data_path = os.path.join('INPUT', self.JTNN_params['training']['data'])
        training_smiles = [x.strip("\r\n ") for x in open(training_data_path)]
        plotting_results_and_save(valid_smiles_w_scores=self.valid_smiles, training_smiles=training_smiles)
        
    def load_output(self):
        return []


class BayesianOptimization(AutoNamingTask):
    '''Bayesian Optimization Task
    
    This task executes Bayesian Optimization for JTNN.
    '''
    
    constrained = luigi.BoolParameter(default=BayesianOptimization_params['target_params']['constrained'])

    JTNN_params = luigi.DictParameter(default=JTNN_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="bayesian_optimization")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False

    def requires(self):
        return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            TrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            ConstructDatasetForBO(
                JTNN_params=self.JTNN_params,
                target=self.target,
                use_gpu=self.use_gpu,
                constrained=self.constrained,
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        import GPyOpt
        import GPy.kern
        import scipy as sp
        #import sklearn.random_projection.GaussianRandomProjection
        import sklearn.decomposition
        import csv
        
        ############ INITIALIZE JTNN MODEL #############
        
        # TODO: was hat es mit diesem vocabulary auf sich?
        # provide the preprocessed vocabulary
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
                
        # provide the TRAINED model path
        model_path =  self.requires()[1].load_output()
        
        # set seed
        seed_everything(self.seed)
        
        # general parameters for BO
        TARGET = self.target
        STD_TARGET = self.BayesianOptimization_params['target_params']['std_target']
        print(f"\nTarget property {TARGET} - standardized {STD_TARGET}")
        TIME_LIMIT_DECODING = self.JTNN_params['inference']['time_limit_decoding']
        print(f"\nTime limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nOptimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nOptimization will be stopped after {MAX_OPT_TIME} seconds.")
        CONSTRAINED = self.constrained
        print(f"\nBO: Constrained by applicability domain of GNN: {CONSTRAINED}")
        
        # select correct computation device (depending on availability and the use_gpu flag)
        device = torch.device('cpu')
        if torch.cuda.is_available() and self.use_gpu:
            device = torch.device('cuda')
        print(f"\nBO: performing NN computations on {device}")

        # define the model 
        jtnn_model = jtnn.JTNNVAE(vocab=vocab, 
                    hidden_size=self.JTNN_params['model']['hidden_size'], 
                    latent_size=self.JTNN_params['model']['latent_size'], 
                    depth=self.JTNN_params['model']['depth'])

        # fill the model with life (i.e. load the parameters)
        jtnn_model.load_state_dict(torch.load(model_path, map_location=device))
        jtnn_model = jtnn_model.to(device)
        
        
        ############ SET UP BO #############
        
        # copied from graph_grammar/src/graph_grammar/bo/mol_opt.py
        dim_reduction_catalog = {
            'PCA': sklearn.decomposition.PCA,
            'KernelPCA': sklearn.decomposition.KernelPCA,
            #'GaussianRandomProjection': GaussianRandomProjectionWithInverse
        }

        kern_catalog = {
            'Matern52': GPy.kern.Matern52,
            'RBF': GPy.kern.RBF
        }

        gp_catalog = {
            'GPModel': GPyOpt.models.gpmodel.GPModel,
            'GPModel_MCMC': GPyOpt.models.gpmodel.GPModel_MCMC
        }

        def run_gpyopt(
            X_train, y_train, 
            X_test=None, y_test=None,
            X_all=None, y_all=None,
            bo_num_iter=250,
            deterministic=True,
            min_coef=1.2, 
            max_coef=1.2,
            dim_reduction_method='PCA',
            dim_reduction_params={
                'n_components': 0.999, # if 0 < value < 1: n_components is selected according to explained varianece ratio
                'svd_solver': 'full', # should be full if 0 < n_compontens < 1
                #'fit_inverse_transform': True, # for kernelPCA
                #'kernel': 'Matern52' # for kernelPCA
            }, 
            fix_dim_reduction=True,
            bo_params={
                'model_type': 'GP', 
                'kernel': 'Matern52',
                'kernel_kwargs':{}, 
                'acquisition_type': 'EI',
                'acquisition_optimizer_type': 'lbfgs',
                'normalize_Y': True,
                'evaluator_type': 'thompson_sampling', 
                'batch_size': 10
            },
            ext_gp_method=None,
            ext_gp_params={
                'kernel': 'Matern52', 
                'sparse': True, 
                'num_inducing': 1500
            },
            logger=print
        ):
            ''' run molecular optimization

            Parameters
            ----------
            X_train: array-like, shape (num_train, dim)
                Latent vectors of molecules to be used for GP training, computed by MHG model.
            y_train: array-like, shape (num_train,)
                Predicted target values of molecules to be used for GP training.
            X_test: array-like, shape (num_test, dim)
                Latent vectors of molecules to be used for GP testing, computed by MHG model.
            y_test: array-like, shape (num_test,)
                Predicted target values of molecules to be used for GP testing (used to compute test RMSE and test log-likelihood for validation).
            X_all: array-like, shape (num_data, dim)
                Latent vectors of all molecules in the dataset, computed by MHG model.
            y_all: array-like, shape (num_train,)
                Predicted target values of all molecules in the dataset.
            bo_num_iter: int, optional
                The number of BO iterations.
                Default is 250.
            min_coef: float, optional
                The coefficient used to set the lower bounds for the search space for BO.
                Calculated as minimum of all latent vectors in the dataset in each dimension substracted with (min_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            max_coef: float, optional
                The coefficient used to set the upper bounds for the search space for BO.
                Calculated as maximum of all latent vectors in the dataset in each dimension increased by (max_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            dim_reduction_method: string, optional (ignored if None)
                Dimension reduction for latent vectors.
                If None, no dimension reduction is applied.
                Calculated based on X_all.
                Default is PCA.
            dim_redcution_params: dict, optional
                Specific parameters for dimension reduction method.
            fix_dim_reduction: Boolean, optional
                If dimension reduction is applied, it can be defined whether the dimension reduction method is fixed (True) or retrained after each BO iteration (False).
                Default: True
            bo_params: dict, optional
                Specific parameters used in BO of GPyOpt.
                Default similar to GPyOpt with evaluator_type: thompson_sampling, batch_size: 10, and GP model of type sparse: True, num_inducing: 1500
            ext_gp_method: string, optional (ignored if None)
                External surrogate for BO within GPyOpt.
                If None, internal surrogate model of GPyOpt is used that can be defined in bo_params.
                Default is None.
            ext_gp_params: dict, optional (ignored if ext GP method is None!)
                Specific parameters for surrogate model used for BO.
                If ext_gp_method is None, this is ignored! Please specifiy GP parameters in bo_params!
                Default kernel: Matern52, sparse: True, num_inducing: 1500.

            Returns
            -------
            history : list of dicts
                in each dict,
                - 'mol_list' is a list of molecules that BO chooses
                - 'feature' is an array containing latent representations of the molecules
                - 'score_list' is a list of scores obtained by applying `target_func` to the molecules
            '''
            
            print(f"\n####################### Start BO with GPyOpt with seed {self.seed} #######################")
            if X_test is None:
                X_test = X_train[0:1, :]
                y_test = y_train[0:1]
                valid_test = False
            else:
                valid_test = True
            
            if X_all is None:
                X_all = np.concatenate((X_train, X_test),0)
                y_all = np.concatenate((y_train, y_test),0)
                
            STD_TARGET = False
           
            latent_dimension = X_all.shape[1]
            dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params) if dim_reduction_method else None
            if dim_reduction: 
                print(f"Dim reduction will be done by {dim_reduction_method} with params {dim_reduction_params}")
                # catch PCA set-up failure
                if dim_reduction_method == 'PCA':
                    tmp_n_components = dim_reduction_params['n_components']
                    if tmp_n_components < 1:
                        tmp_svd_solver = dim_reduction_params['svd_solver']
                        if tmp_svd_solver != 'full': raise ValueError(f"Dimension reduction set-up is fragile: n_components {tmp_n_components}, svd_solver: {tmp_svd_solver}.")
                # fit dimension reduction method with all available data points
                dim_reduction.fit(X_all)
                latent_dimension = dim_reduction.n_components_
                expl_var_r = dim_reduction.explained_variance_ratio_
                logger(f"\n\t----- Dim reduction by PCA fitted. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                logger(f"Components: {latent_dimension}, Total explained variance: {np.sum(expl_var_r)}")
                
            
            # center around 0 with std dev of 1
            y_train_mean, y_train_std = 0, 1 # default values -> transformation does not change numbers
            if STD_TARGET:
                y_train_mean, y_train_std = np.mean(y_train), np.std(y_train)
                y_train = (y_train - y_train_mean) / y_train_std
                y_test = (y_test - y_train_mean) / y_train_std
                print(f"\n{TARGET} are standardized.")
                print(f"Mean value for {TARGET} training data {y_train_mean}")
                print(f"Std dev value for {TARGET} training data {y_train_std}")
            
            X_train_excerpt = X_train
            y_train_excerpt = y_train
            X_all_excerpt = X_all

            if X_train.shape[0] != len(y_train.ravel()):
                raise ValueError('X_train and y_train have inconsistent shapes')
            if X_test.shape[0] != len(y_test.ravel()):
                raise ValueError('X_test and y_test have inconsistent shapes')
            
            y_train_excerpt = - y_train_excerpt.reshape(-1, 1) # minus target value
            y_test = - y_test.reshape(-1, 1) # minus target value

            print(f"BO: Shape of the training set excerpt {np.shape(X_train_excerpt)}")
            
            def get_kernel(params, latent_dimension):
                params_ = deepcopy(dict(params))
                kernel_name = params_.pop('kernel')
                kernel_kwargs = params_.pop('kernel_kwargs')
                gp_kernel = kern_catalog[kernel_name](input_dim=latent_dimension,**kernel_kwargs)
                return params_, gp_kernel
            
            bo_params_ = deepcopy(dict(bo_params))
            if ext_gp_method is not None:
                ext_gp_params_ = deepcopy(dict(ext_gp_params))
                if 'kernel' in ext_gp_params_:
                    ext_gp_params_, ext_gp_kernel = get_kernel(ext_gp_params, latent_dimension)
                ext_gp_model = gp_catalog[ext_gp_method](kernel=ext_gp_kernel, **ext_gp_params_)
                print(f"\nBO is executed with following parameters: {bo_params_} \nand using external surrogate model for BO: {ext_gp_model} with parameters {ext_gp_params} with kernel {ext_gp_kernel}.")
            else:
                if 'kernel' in bo_params_:
                    bo_params_, gp_kernel = get_kernel(bo_params_, latent_dimension)
                    bo_params_['kernel'] = gp_kernel
                print(f"BO is executed with following parameters: {bo_params_} with kernel {gp_kernel}.")
               
            # run BO for #bo_num_iter iterations
            valid_smiles = [] # SMILES and property values of all valid molecules found during optimization (chronologically)
            latent_vectors_bo = []
            history = []
            look_up_predictions = {}
            look_up_validities = {}
            bo_start_time = time.time()
            stopping_criterion_reached = False
            for bo_iter in range(bo_num_iter):
                print(f"\nBO: starting {bo_iter} iteration")
                
                # dimension reduction
                if dim_reduction:
                    if (fix_dim_reduction == False) and (bo_iter != 0):
                        # make sure dimension of PCA stays constant during optimization (only important in case when n_components < 1 to determine n_components automatically)
                        dim_reduction_params_ = deepcopy(dict(dim_reduction_params))
                        dim_reduction_params_['n_components'] = latent_dimension
                        dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params_)
                        # refit dimension reduction method
                        dim_reduction.fit(X_all_excerpt)
                        expl_var_r = dim_reduction.explained_variance_ratio_
                        logger(f"\n\t----- BO: Dim reduction by PCA refitted in BO iteration {bo_iter}. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                        logger(f"Components: {dim_reduction.n_components_}, Total explained variance: {np.sum(expl_var_r)}")
                    X_train_excerpt_low = dim_reduction.transform(X_train_excerpt)
                    X_test_low = dim_reduction.transform(X_test)
                    X_all_low = dim_reduction.transform(X_all)
                else:
                    X_train_excerpt_low = X_train_excerpt
                    X_test_low = X_test
                    X_all_low = X_all
                    
                print(f"BO: Shape of the training set for GP X: {np.shape(X_train_excerpt_low)}, y: {np.shape(y_train_excerpt)}")
                # number of training points can be limited by num of inducing points for sparse GPs
                try:
                    num_ind_pts = bo_params_['num_inducing']
                    if bo_params_['sparse'] == True:
                        if num_ind_pts < np.shape(X_train_excerpt_low)[0]: print(f"BO: Number of training points for sparse GP is limited to: {num_ind_pts} which will be randomly selected from the training data.")
                except:
                    pass
                
                space = [
                    {
                        'name': f'x{each_idx}',
                        'type': 'continuous',
                        'domain': (
                            np.min(X_all_low[:, each_idx]) - (min_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx])),
                            np.max(X_all_low[:, each_idx]) + (max_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx]))
                        )
                    }
                    for each_idx in range(X_all_low.shape[1])
                ]
                

                
                iter_bo_time_t0 = time.time()
                if ext_gp_method is not None:
                    ext_gp_model.updateModel(X_train_excerpt_low, y_train_excerpt, None, None)
                    logger(f"BO: External GP model used - GP model updated.")
                    trans_space =  GPyOpt.core.task.space.Design_space(space, None)
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None, 
                        model=ext_gp_model,
                        acquisition=GPyOpt.acquisitions.EI.AcquisitionEI(
                            model=ext_gp_model, 
                            space=trans_space, 
                            optimizer=GPyOpt.optimization.acquisition_optimizer.AcquisitionOptimizer(trans_space, 'lfbgs'), 
                            cost_withGradients=GPyOpt.core.task.cost.CostModel(None).cost_withGradients, 
                            jitter=0.2),
                        domain=space, 
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )

                    # check ll and rmse on test set
                    test_pred, test_std = ext_gp_model.predict(X_test_low, False)
                    test_err = np.sqrt(np.mean((test_pred - y_test)**2))
                    test_ll = np.mean(sp.stats.norm.logpdf(test_pred - y_test,
                                                           scale=test_std))

                    # check ll and rmse on training set
                    train_pred, train_std = ext_gp_model.predict(X_train_excerpt_low, False)
                    train_err = np.sqrt(np.mean((train_pred - y_train_excerpt)**2))
                    train_ll = np.mean(sp.stats.norm.logpdf(train_pred - y_train_excerpt,
                                                            scale=train_std))
                    logger(f"BO: train_err: {train_err}\t train_ll: {train_ll}")
                    if valid_test:
                        logger(f"BO: test_err: {test_err}\t test_ll: {test_ll}")
                
                else:
                    # initialize BO, GP is initialized each time
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None,
                        domain=space,
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )
                
                # BO suggests next points to evaluate 
                X_next_low = bo_step.suggest_next_locations(ignored_X=X_train_excerpt_low)
                
                iter_bo_time_t1 = time.time()
                print(f"BO time needed for suggesting next locations (including initialization): {iter_bo_time_t1-iter_bo_time_t0:.1f} seconds.\n")
                
                # inverse dimension reduciton
                if dim_reduction:
                    X_next = dim_reduction.inverse_transform(X_next_low)
                else:
                    X_next = X_next_low

                # evaluate suggested points
                valid_smiles_current_batch = []
                num_uni_smiles_current_batch = []
                run_times_current_batch = []
                new_feature_array = []
                num_mol = len(valid_smiles)
                score_list = []
                raw_score_list = []
                for i in range(len(X_next)):
                    ### DECODING of the latent vectors by generative model ###
                    
                    # get smiles string from latent vector
                    latent_vector = X_next[i].flatten()
                    # store latent vectors
                    latent_vectors_bo.append(latent_vector.tolist())
                    
                     # get smiles string from parameters
                    # note that there long decoding times can occur
                    # restrict decoding time -> func-timeout)
                    @timeout(TIME_LIMIT_DECODING)
                    def get_smiles(new_x, jtnn_model, device):
                        smiles = get_decoded_smiles(new_x,jtnn_model, device)[0]
                        return smiles
                    
                    smiles = get_smiles(latent_vector, jtnn_model, device)
                    print(f"BO: Found following SMILES string: {smiles}\n")
 
                    if smiles is not None:
                        smiles = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(smiles)) # make sure that canonical SMILES are stored
                        valid_smiles_current_batch.append(smiles)
                        new_feature_array.append(X_next[i])
                        valid = True
                        # compute target score for the current molecule
                        if CONSTRAINED:
                            # only compute target score if the molecule is similar to the training data
                            if smiles in look_up_validities:
                                prediction_validity_score = look_up_validities[smiles]
                            else: 
                                prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                                look_up_validities[smiles] = prediction_validity_score
                            if prediction_validity_score == 1:
                                if smiles in look_up_predictions:
                                    predicted_target_value = look_up_predictions[smiles]
                                else:
                                    predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                    look_up_predictions[smiles] = predicted_target_value
                            elif prediction_validity_score == -1:
                                valid = False
                                predicted_target_value = -1000
                                print(f"Non valid prediction for {smiles}")
                            else:
                                raise ValueError("Validity class not valid.")
                        else: # no constrains on the applicability of the metric
                            if smiles in look_up_predictions:
                                predicted_target_value = look_up_predictions[smiles]
                            else:
                                predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                look_up_predictions[smiles] = predicted_target_value
                        if valid: 
                            valid_smiles.append((smiles,predicted_target_value))
                        raw_score_list.append(predicted_target_value)
                        # possibly recompute target value
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused
                    else:
                        new_feature_array.append(X_next[i])
                        predicted_target_value = -1000
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused
                        
                    # current run time of optimization
                    current_time = time.time()
                    run_time = current_time - bo_start_time
                    run_times_current_batch.append(run_time)
                    
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    num_uni_smiles_current_batch.append(num_unique_mols)
                        
                    ## STOPPING CRITERIA
                    # stop BO if desired number of molecules is reached
                    stopping_criterion_active = False
                    if MAX_NUM_MOL:
                        stopping_criterion_active = True
                        print(f"---> Current number of unique molecules {num_unique_mols}.<---")
                        if num_unique_mols >= MAX_NUM_MOL:
                            logger(f'\n\t---> {MAX_NUM_MOL} unique molecules identified. BO stopped.<---')
                            stopping_criterion_reached = True
                            break
                    # stop BO after predefined time
                    if MAX_OPT_TIME:
                        if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                        stopping_criterion_active = True
                        if run_time >= MAX_OPT_TIME:
                            logger(f'\n\t---> {MAX_OPT_TIME} seconds optimization time reached. BO stopped. <---')
                            stopping_criterion_reached = True
                            break
                    # No stopping criteria active
                    if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')

                if new_feature_array != []:
                    new_feature_array = np.vstack(new_feature_array)
                logger(f"BO: {len(valid_smiles_current_batch)} molecules are found")
                
                for each_idx, each_mol in enumerate(valid_smiles_current_batch):
                    logger(f"BO: smiles: {each_mol}\n\t {TARGET}: {raw_score_list[each_idx]}")

                if len(new_feature_array) > 0:
                    X_train_excerpt = np.concatenate([X_train_excerpt, new_feature_array], 0)
                    y_train_excerpt = np.concatenate([y_train_excerpt, np.array(score_list)[:, None]], 0)
                    X_all_excerpt = np.concatenate([X_all_excerpt, new_feature_array], 0) # required for dimension reduction refit if dim_reduction with fix_dimension_reduction is False

                # save list of all smiles after each iteration
                path_smiles = [
                    [
                        valid_smiles_current_batch[tmp_idx], 
                        raw_score_list[tmp_idx], 
                        bo_iter + 1, 
                        num_mol + tmp_idx + 1,
                        run_times_current_batch[tmp_idx],
                        num_uni_smiles_current_batch[tmp_idx]
                    ] 
                    for tmp_idx, _ in enumerate(valid_smiles_current_batch)
                ]
                
                with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                    writer = csv.writer(f_table)
                    writer.writerows(path_smiles)
                    
                if ext_gp_method is not None:
                    history.append({'mol_list': valid_smiles_current_batch, 'feature': new_feature_array, 'score_list': score_list,
                                    'train_err': train_err, 'train_ll': train_ll,
                                    'test_err': test_err if valid_test else np.nan,
                                    'test_ll': test_ll if valid_test else np.nan})
                else:
                    history.append({'mol_list': valid_smiles_current_batch, 'feature': new_feature_array, 'score_list': score_list})               
                    
                # check if stopping criterion was reached in current batch
                if stopping_criterion_reached: break

            print("\n####################### Finished BO with GPyOpt #######################")
            return history, valid_smiles, latent_vectors_bo
        
        def generate_initial_data_combustion(X, y, num_train_samples=500, num_test_samples=100, permute=True):
            n = X.shape[0]
            if(num_train_samples==-1):
                num_train_samples=n
            breakpoint_train = min(np.int(np.round(0.9 * n)),num_train_samples)
            breakpoint_test = max(np.int(np.round(0.1 * n)), min(n - num_train_samples, num_test_samples))

            # select datapoints
            if permute:
                permutation = torch.from_numpy(np.random.choice(n, n, replace=False))
                X_train = X[permutation, :][0: breakpoint_train, :]
                y_train = y[permutation][0: breakpoint_train]
                X_test = X[permutation, :][breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[permutation][breakpoint_train: breakpoint_train + breakpoint_test]
            else:
                X_train = X[0: breakpoint_train, :]
                y_train = y[0: breakpoint_train]
                X_test = X[breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[breakpoint_train: breakpoint_train + breakpoint_test]

            best_observed_value = np.max(y_train)

            return X_train, y_train, X_test, y_test, best_observed_value
        
        ############ Generate initial training data for BO ##############
        X_all, y_all, y_raw, valid_mask = self.requires()[2].load_output()
        if (len(valid_mask) != len(X_all)) or (len(valid_mask) != len(y_all)):
            raise ValueError(f"WARNING: Validity mask does not match number of data points.")
        X_all = np.array(X_all)[valid_mask]
        y_all = np.array(y_all)[valid_mask]
        

        # call helper functions to generate initial training data
        X_train, y_train, X_test, y_test, best_observed_value_nei = generate_initial_data_combustion(X=X_all, y=y_all, num_train_samples=self.BayesianOptimization_params['training_params']['num_train_samples'], num_test_samples=self.BayesianOptimization_params['training_params']['num_test_samples'])
        # save initial points
        with gzip.open(f"{os.path.dirname(self.output().path)}/BO_initial.pklz", 'wb') as f1:
            pickle.dump(
                {
                    'X_train': X_train, 
                    'y_train': y_train, 
                    'X_test': X_test, 
                    'y_test': y_test, 
                    'best_initial_value': best_observed_value_nei
                }, 
                f1
            )
            
        ############## RUN BO with GPyOpt ##############
        # BO parameters
        bo_params = BayesianOptimization_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: bo_params['run_params']['bo_num_iter'] = int(sys.maxsize)
        bo_run_params = bo_params['run_params']
        # save bo_params
        with open(f"{os.path.dirname(self.output().path)}/BO_params.json", 'a+') as fp:
            json.dump(bo_params, fp)
        # run BO
        history, valid_smiles, latent_vectors = run_gpyopt(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, X_all=X_all, y_all=y_all, **bo_run_params)
        print("Finished running the optimization. Now saving the output.")
        
        ## SAVE RESULTS
        # save history
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'wb') as f:
            pickle.dump(history, f)
        # save valid smiles as pklz
        save_object(valid_smiles, self.output().path)
        # save valid smiles as txt
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
        
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors, l_f)        

    def load_output(self, ret_history=False):
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'rb') as f:
            history = pickle.load(f)
        output_valid_smiles = load_object(self.output().path)
        if ret_history: return history
        print(output_valid_smiles)
        return output_valid_smiles
    


class MultipleBO(MainTask, AutoNamingTask):
    '''Multiple Bayesian Optimization Task
    
    This task executes the Bayesian Optimization Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    
    # internal params accessed from INPUT/param.py
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    MultipleBO_params = luigi.DictParameter(default=MultipleBO_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    working_subdir = luigi.Parameter(default="MultipleBO") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
   
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {self.max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print(f'Stopping criteria may be conflicting:\n1. finding {self.max_num_mols} unique molecules.\n2. Maximum optimization time {self.max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether BO is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.BayesianOptimization_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleBO_params['seed_list']
            
        return [
            BayesianOptimization(
                JTNN_params=self.JTNN_params, 
                BayesianOptimization_params=self.BayesianOptimization_params, 
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/BO_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]       
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            BO_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    JTNN_params=self.JTNN_params,
                    working_subdir=each_BO_run.working_subdir,
                    valid_smiles=each_BO_run.load_output(),
                    pred_model=each_BO_run.pred_model,
                )
                for each_BO_run in BO_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []
    

class GeneticAlgorithm(AutoNamingTask):
    '''Genetic Algorithm Optimization Task
    
    This task executes the Genetic Algorithm Optimization for MHG.
    '''
    constrained = luigi.BoolParameter(default=GeneticAlgorithm_params['target_params']['constrained'])
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="GA")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False   # helper variable to avoid multiple renaming of working subdir when GA with applicability domain constraint is executed
    
    def requires(self):
         return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            TrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            GenerateLatentFiles(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            # when GA with AD constraint is executed add this information to name of working subdir
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        # additional packages required for GA
        import geneticalgorithm.geneticalgorithm as ga
        import csv
        
        # helper class to stop GA when stopping criterion is reached by raising Error that can be catched
        class StoppingCriterionReachedError(Exception):
            '''
            Raised when maximum number of molecules is reached
            '''
            pass
        
        # provide the preprocessed vocabulary
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
                
        # provide the TRAINED model path
        model_path =  self.requires()[1].load_output()
        print(model_path)
        
        # set seed
        seed_everything(self.seed)
        
        # select correct computation device (depending on availability and the use_gpu flag)
        device = torch.device('cpu')
        if torch.cuda.is_available() and self.use_gpu:
            device = torch.device('cuda')
        print(f"\nGA: performing NN computations on {device}")

        # define the model 
        jtnn_model = jtnn.JTNNVAE(vocab=vocab, 
                    hidden_size=self.JTNN_params['model']['hidden_size'], 
                    latent_size=self.JTNN_params['model']['latent_size'], 
                    depth=self.JTNN_params['model']['depth'])

        # fill the model with life (i.e. load the parameters)
        #load_model_jtnn(jtnn_model, model_path, device)
        jtnn_model.load_state_dict(torch.load(model_path, map_location=device))
        jtnn_model = jtnn_model.to(device)
        
        # general parameters for the genetic algorithm
        TARGET = self.target
        STD_TARGET = self.GeneticAlgorithm_params['target_params']['std_target']
        print(f"\nGA: Target property {TARGET} - standardized {STD_TARGET}")
        CONSTRAINED = self.constrained
        print(f"\nGA: Constrained by applicability domain of GNN: {CONSTRAINED}")
        TIME_LIMIT_DECODING = self.JTNN_params['inference']['time_limit_decoding']
        print(f"\nGA: Time limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nGA: Optimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nGA: Optimization will be stopped after {MAX_OPT_TIME} seconds.")

        # global list within GeneticAlgorithm with SMILES and property values of all valid molecules found during optimization (chronologically)
        valid_smiles = []
        obj_fun_calls = []
        latent_vectors_ga = []
        look_up_predictions = {}
        look_up_validities = {}
      
        # objective function for ga
        def objective(new_x):
            latent_vectors_ga.append(new_x)
            
            # number of objective function calls
            obj_fun_calls.append(1)
            tmp_calls = len(obj_fun_calls)
            ga_iter = (tmp_calls - 1) // GeneticAlgorithm_params['algorithm_param']['population_size']
            
            # get current run time
            current_time = time.time()
            run_time = current_time - GA_START_TIME
            
            # get current number of unique molecules identified
            num_unique_mols = len(list(dict(valid_smiles).items()))
                
            ## STOPPING CRITERIA
            # stop BO if desired number of molecules is reached
            # TODO: decide if we want only count unique smiles -> vorzeitig, unterschiedlich, Moeglichkeit: -1 (keine Grenze), zeitlich Performance (target value) plotten ueber #Molekuels 
            stopping_criterion_active = False
            if MAX_NUM_MOL:
                stopping_criterion_active = True
                if num_unique_mols >= MAX_NUM_MOL:
                    print(f'\n---> {MAX_NUM_MOL} unique molecules identified. GA stopped.<---')
                    raise StoppingCriterionReachedError
            # stop BO after predefined time
            if MAX_OPT_TIME:
                if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                stopping_criterion_active = True
                if run_time >= MAX_OPT_TIME:
                    print(f'\n---> {MAX_OPT_TIME} seconds optimization time reached. GA stopped. <---')
                    raise StoppingCriterionReachedError
            # No stopping criteria active
            if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')
                
            
            # get smiles string from parameters
            # note that there long decoding times can occur
            # restrict decoding time -> func-timeout)
            print(TIME_LIMIT_DECODING)
            @timeout(TIME_LIMIT_DECODING)
            def get_smiles(new_x, jtnn_model, device):
                smiles = get_decoded_smiles(new_x,jtnn_model, device)[0]
                return smiles

            smiles = get_smiles(new_x, jtnn_model, device)
            print(f"GA: Found following SMILES string: {smiles}\n")
            
            # evaluate the decoded molecule
            if smiles is not None:
                valid = True
                # compute target score for the current molecule
                if CONSTRAINED:
                    if smiles in look_up_validities:
                        prediction_validity_score = look_up_validities[smiles]
                    else: 
                        prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                        look_up_validities[smiles] = prediction_validity_score
                    if prediction_validity_score == 1:
                        if smiles in look_up_predictions:
                            print("already predicted property for this smiles.")
                            predicted_target_value = look_up_predictions[smiles]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                            look_up_predictions[smiles] = predicted_target_value
                    elif prediction_validity_score == -1:
                        predicted_target_value = -1000
                        valid = False
                        print(f"Non valid prediction for {smiles}")
                    else:
                        valid = False
                        raise ValueError("Validity class not valid.")
                else: # no constrains on the applicability of the metric
                    if smiles in look_up_predictions:
                        print("already predicted property for this smiles.")
                        predicted_target_value = look_up_predictions[smiles]
                    else:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                        look_up_predictions[smiles] = predicted_target_value
                    

                if valid: # valid can be false if the constraints are not satisfied.
                    
                    # always store with raw predicted value
                    valid_smiles.append((smiles,predicted_target_value)) # valid_smiles is global!
                    
                    # save results - NOTE: we need to save here bc GA does not have explicit loop
                    num_mol = len(valid_smiles)
                    # get current number of unique molecules identified
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    path_smiles = [
                        smiles, 
                        predicted_target_value, 
                        ga_iter + 1, 
                        num_mol, 
                        run_time,
                        num_unique_mols
                    ]
                    
                    with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                        writer = csv.writer(f_table)
                        writer.writerow(path_smiles)
                        
                    # possibly recompute target value
                    if STD_TARGET:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, std_score=STD_TARGET, model=self.pred_model)[0]
                print(f"Predicted value for GA: {-predicted_target_value}")
                return -predicted_target_value # return negative value bc ga minimizes
            else: # no molecule returned
                print("returned invalid")
                return 1000
        
        # set up params for GA
        X_all = self.requires()[2].load_output() # load latent vectors
        X_min = np.min(X_all , axis=0) - (self.GeneticAlgorithm_params['run_params']['min_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        X_max = np.max(X_all , axis=0) + (self.GeneticAlgorithm_params['run_params']['max_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        varbound = np.transpose([X_min, X_max])
        print(varbound)
        
        # GA parameters
        ga_params = GeneticAlgorithm_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: ga_params['algorithm_param']['max_num_iteration'] = int(sys.maxsize - 1)
        ga_algorithm_params = ga_params['algorithm_param']
        with open(f"{os.path.dirname(self.output().path)}/GA_params.json", 'a+') as fp:
            json.dump(ga_params, fp)
        
        # initialize GA model
        ga_model = ga(
            function=objective,
            dimension=self.JTNN_params['model']['latent_size'],
            variable_type='real',
            variable_boundaries=varbound,
            algorithm_parameters=ga_algorithm_params,
            function_timeout=1000,
        )
        
        # start the genetic algorithm (while measuring the runtime)
        print("\n####################### Start GA. #######################")
        
        GA_START_TIME = time.time()
        try:
            ga_model.run()
        except StoppingCriterionReachedError:
            print(f"Maximum number of molecules ({MAX_NUM_MOL}) is reached. GA stopped.")
        ga_end_time = time.time()
        
        print("\n####################### GA finished. #######################")
        
        save_object(valid_smiles, self.output().path)
        # create vobaculary and save it in the same dir as the training data
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
                
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors_ga, l_f)

        print("----------------------------------- RESULTS FOR PARAMS: -----------------------------------")
        print(ga_algorithm_params)
        print(f"\nGA: Total time for running: {ga_end_time - GA_START_TIME}.")
        

    def load_output(self):
        output_valid_smiles = load_object(self.output().path)
        return output_valid_smiles
    
class MultipleGA(MainTask, AutoNamingTask):
    '''
    Reapeating GA for seeds in seed_list (or one time execution if single_seed is set)
    '''
    
    # internal params accessed from INPUT/param.py
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    MultipleGA_params = luigi.Parameter(default=MultipleGA_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleGA") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {self.max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print(f'Stopping criteria may be conflicting:\n1. finding {self.max_num_mols} unique molecules.\n2. Maximum optimization time {self.max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether GA is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.GeneticAlgorithm_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleGA_params['seed_list']
            
        # requirements (previous jobs to be executed)
        return [
            GeneticAlgorithm(
                JTNN_params=self.JTNN_params, 
                GeneticAlgorithm_params=self.GeneticAlgorithm_params, 
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/GA_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            GA_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    JTNN_params=self.JTNN_params,
                    working_subdir=each_GA_run.working_subdir,
                    valid_smiles=each_GA_run.load_output(),
                    pred_model=each_GA_run.pred_model,
                )
                for each_GA_run in GA_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []
    
class RandomSearch(AutoNamingTask):
    '''Random Search Heurisitc.
    
    This task applies random sampling from latent space to find molecules with desired properties.
    '''
    constrained = luigi.BoolParameter(default=RandomSearch_params['target_params']['constrained'])
    
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    RandomSearch_params = luigi.DictParameter(default=RandomSearch_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="Random")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False   # helper variable to avoid multiple renaming of working subdir when Random with applicability domain constraint is executed
    
    def requires(self):
         return [
            DataPreprocessing(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            TrainingJTNN(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
            GenerateLatentFiles(
                JTNN_params=self.JTNN_params, 
                use_gpu=self.use_gpu
            ),
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name            
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))


    def run(self):
        
        # additional packages required for Random
        import csv
        
        # helper class to stop Random when stopping criterion is reached by raising Error that can be catched
        class StoppingCriterionReachedError(Exception):
            '''
            Raised when maximum number of molecules is reached
            '''
            pass
        
        # provide the preprocessed vocabulary
        vocab_list = self.requires()[0].load_output()
        vocab = jtnn.Vocab(vocab_list)
                
        # provide the TRAINED model path
        model_path =  self.requires()[1].load_output()
        print(model_path)
        
        # set seed
        seed_everything(self.seed)
        
        # select correct computation device (depending on availability and the use_gpu flag)
        device = torch.device('cpu')
        if torch.cuda.is_available() and self.use_gpu:
            device = torch.device('cuda')
        print(f"\nRandom: performing NN computations on {device}")

        # define the model 
        jtnn_model = jtnn.JTNNVAE(vocab=vocab, 
                    hidden_size=self.JTNN_params['model']['hidden_size'], 
                    latent_size=self.JTNN_params['model']['latent_size'], 
                    depth=self.JTNN_params['model']['depth'])

        # fill the model with life (i.e. load the parameters)
        #load_model_jtnn(jtnn_model, model_path, device)
        jtnn_model.load_state_dict(torch.load(model_path, map_location=device))
        jtnn_model = jtnn_model.to(device)
        
        # general parameters for the Random Search 
        TARGET = self.target
        STD_TARGET = self.RandomSearch_params['target_params']['std_target']
        print(f"\nRandom: Target property {TARGET} - standardized {STD_TARGET}")
        CONSTRAINED = self.constrained
        print(f"\nRandom: Constrained by applicability domain of GNN: {CONSTRAINED}")
        TIME_LIMIT_DECODING = self.JTNN_params['inference']['time_limit_decoding']
        print(f"\nRandom: Time limit for decoding a single element: {TIME_LIMIT_DECODING}")        
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nGA: Optimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nGA: Optimization will be stopped after {MAX_OPT_TIME} seconds.") 

        # global list within Random Search with SMILES and property values of all valid molecules found during optimization (chronologically)
        valid_smiles = []
        obj_fun_calls = []
        latent_vectors_random = []
        look_up_predictions = {}
        look_up_validities = {}

        def objective(new_x):
            
            # store latent vector
            latent_vectors_random.append(new_x)
            
            # number of funciton calls
            obj_fun_calls.append(1)
            tmp_calls = len(obj_fun_calls)
            random_iter = tmp_calls
                        
            # get current run itme    
            current_time = time.time()
            run_time = current_time - rand_start_time
            
            # get current number of unique mols identified
            num_unique_mols = len(list(dict(valid_smiles).items()))
            
            ## STOPPING CRITERIA
            # stop BO if desired number of molecules is reached
            # TODO: decide if we want only count unique smiles -> vorzeitig, unterschiedlich, Moeglichkeit: -1 (keine Grenze), zeitlich Performance (target value) plotten ueber #Molekuels 
            stopping_criterion_active = False
            if MAX_NUM_MOL:
                stopping_criterion_active = True
                if num_unique_mols >= MAX_NUM_MOL:
                    print(f'\n---> {MAX_NUM_MOL} unique molecules identified. Random stopped.<---')
                    raise StoppingCriterionReachedError
            # stop BO after predefined time
            if MAX_OPT_TIME:
                if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                stopping_criterion_active = True
                if run_time >= MAX_OPT_TIME:
                    print(f'\n---> {MAX_OPT_TIME} seconds optimization time reached. Random stopped. <---')
                    raise StoppingCriterionReachedError
            # No stopping criteria active
            if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')
                
            # get smiles string from parameters
            # note that there long decoding times can occur
            # restrict decoding time -> func-timeout
            print(TIME_LIMIT_DECODING)
            @timeout(TIME_LIMIT_DECODING)
            def get_smiles(new_x, jtnn_model, device):
                smiles = get_decoded_smiles(new_x,jtnn_model, device)[0]
                return smiles

            smiles = get_smiles(new_x, jtnn_model, device)
            print(f"Random: Found following SMILES string: {smiles}\n")

            # evaluate the decoded molecule
            if smiles is not None:
                valid = True
                # compute target score for the current molecule
                if CONSTRAINED:
                    # only compute target score if the molecule is similar to the training data
                    if smiles in look_up_validities:
                        prediction_validity_score = look_up_validities[smiles]
                    else: 
                        prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                    if prediction_validity_score == 1:
                        if smiles in look_up_predictions:
                            print("already predicted property for this smiles.")
                            predicted_target_value = look_up_predictions[smiles]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                            look_up_predictions[smiles] = predicted_target_value
                    elif prediction_validity_score == -1:
                        predicted_target_value = -1000
                        valid = False
                        print(f"Non valid prediction for {smiles}")
                    else:
                        valid = False
                        raise ValueError("Validity class not valid.")
                else: # no constrains on the applicability of the metric
                    if smiles in look_up_predictions:
                        print("already predicted property for this smiles.")
                        predicted_target_value = look_up_predictions[smiles]
                    else:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                        look_up_predictions[smiles] = predicted_target_value

                if valid: # valid can be false if the constraints are not satisfied.
                    
                    # always store with raw predicted value
                    valid_smiles.append((smiles,predicted_target_value)) # valid_smiles is global!
                    
                    # save results - NOTE: we need to save here bc random does not have explicit loop
                    num_mol = len(valid_smiles)
                    # get current number of unique mols identified
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    path_smiles = [
                        smiles, 
                        predicted_target_value, 
                        random_iter, 
                        num_mol, 
                        run_time,
                        num_unique_mols
                    ]
                    
                    with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                        writer = csv.writer(f_table)
                        writer.writerow(path_smiles)
                        
                    # possibly recompute target value
                    if STD_TARGET:
                        predicted_target_value = MolecularMetrics.target_scores([smiles], TARGET, std_score=STD_TARGET)[0] #TODO: does not work for ron-mon
                
                # save results - NOTE: we need to save here bc random does not have explicit loop
                # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                print(f"Predicted value for Random: {-predicted_target_value}\n\n")
                return -predicted_target_value 
            else: # no molecule returned
                print("Returned penalty value: 1000\n\n")
                return 1000


        X_all = self.requires()[2].load_output() # load latent vectors
        X_min = np.min(X_all , axis=0) - (self.RandomSearch_params['run_params']['min_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        X_max = np.max(X_all , axis=0) + (self.RandomSearch_params['run_params']['max_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        varbound = np.transpose([X_min, X_max])
        #print(varbound)
        
        # Random Search parameters
        random_params = RandomSearch_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: RandomSearch_params['algorithm_param']['max_num_iteration'] = int(sys.maxsize - 1)
        random_search_params = random_params['algorithm_param']
        with open(f"{os.path.dirname(self.output().path)}/RandomSearch_params.json", 'a+') as fp:
            json.dump(random_search_params, fp)
            

        # start the random search (while measuring the runtime)
        print("\n####################### Start Random Search. #######################")
        
        rand_start_time = time.time()
        try:
            for r_iter in range(random_search_params['max_num_iteration']):
                new_latent = np.random.uniform(X_min, X_max, (self.JTNN_params['model']['latent_size'],))
                new_obj = objective(new_latent)
        except StoppingCriterionReachedError:
            if MAX_NUM_MOL:
                print(f"Stopping criterion (maximum number of molecules: {MAX_NUM_MOL}) is reached. Random stopped.")
            elif MAX_OPT_TIME: 
                print(f"Stopping criterion (maximum run time: {MAX_OPT_TIME}) is reached. Random stopped.")
            else:
                print("Random stopped for unknown reason.")
        rand_end_time = time.time()
        
        print("\n####################### Random Search finished. #######################")
        
        save_object(valid_smiles, self.output().path)
        # create vobaculary and save it in the same dir as the training data
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
                
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors_random, l_f)

        print("----------------------------------- RESULTS FOR PARAMS: -----------------------------------")
        print(f"\nRandom search parameters: {random_search_params}")
        print(f"Random: Total time for running: {rand_end_time - rand_start_time}.")

    def load_output(self):
        output_valid_smiles = load_object(self.output().path)
        return output_valid_smiles
    
class MultipleRandomSearch(MainTask, AutoNamingTask):
    '''Multiple Random Search Task
    
    This task executes the Random Search Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    
    
    # internal params accessed from INPUT/param.py
    JTNN_params = luigi.DictParameter(default=JTNN_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    RandomSearch_params = luigi.DictParameter(default=RandomSearch_params)
    MultipleRandomSearch_params = luigi.Parameter(default=MultipleRandomSearch_params)
    
    # parameters that can be set from outside when calling MultipleRandomSearch_params main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether RandomSearch is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleRandomSearch") # working dir for MultipleRandomSearch_params
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {self.max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print(f'Stopping criteria may be conflicting:\n1. finding {self.max_num_mols} unique molecules.\n2. Maximum optimization time {self.max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether Random Search is constrained by AD from outside and overwrite RandomSearch_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.RandomSearch_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleRandomSearch_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleRandomSearch_params['seed_list']
            
        # requirements (previous jobs to be executed)
        return [
            RandomSearch(
                JTNN_params=self.JTNN_params, 
                RandomSearch_params=self.RandomSearch_params, 
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/RandomSearch_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            random_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    JTNN_params=self.JTNN_params,
                    working_subdir=each_random_run.working_subdir,
                    valid_smiles=each_random_run.load_output(),
                    pred_model=each_random_run.pred_model,
                )
                for each_random_run in random_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []

if __name__ == "__main__":
    for each_engine_status in glob.glob("./engine_status.*"):
        os.remove(each_engine_status)
    with open("engine_status.ready", "w") as f:
        f.write("ready: {}\n".format(datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')))

    print("starting now.")
    main() # luigine.main()
    print("fertig")
