### begin of executable commands
export PATH=~/anaconda3/bin:$PATH
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/graph_neural_network_for_fuel_ignition_quality
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/gnn_validity
source activate jtnn_py37 # conda env

# choose one of the pipelines: TrainingPipeline, MultipleBO, MultipleGA, MultipleRandomSearch
python main_jtnn.py MultipleGA --working-dir working_dir_molOpt_JTNN --workers 1 --use-gpu --target RON+OS --max-opt-time 43200
# optional --single-seed 123 --max-num-mols 10000 --constrained 
