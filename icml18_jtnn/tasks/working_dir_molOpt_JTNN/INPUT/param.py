#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Title """

__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "Jan 25 2018"

from copy import deepcopy


JTNN_params = {
    'model': {
        'latent_size': 56,
        'hidden_size': 450,
        'depth': 3,
        'verbose': True,
    },
    'pretraining': {
        'max_epoch': 3,
        'print_iter': 20,
    },
    'training': {
        'data': '../../../../data/qm9/raw/HCO_smiles.txt',
        'batch_size': 40,
        'beta': 0.005,
        'lr': 0.0007,
        'stereo': 1,
        'max_epoch': 7,
        'print_iter': 20,
        'seed': 1234,
    },
    'inference': {
        'time_limit_decoding': 10, # seconds, we evaluated that for 95% of training data, the JTNN decoded in under 1 sec
    }
}

# Parameter for molecular optimization
MolOpt_params = {
	'target': 'RON+OS',
    'max_num_mols': 1000,
}

# The following parameter is used for global optimization in the limited oracle case.
# It uses GPyOpt
BayesianOptimization_params = {
    'run_params': {
        'bo_num_iter': 1000, # The number of Bayesian optimization iterations
        'dim_reduction_method': 'PCA', # a method to reduce the dimensionality of the latent space
        'dim_reduction_params': {
            'n_components': 0.999, 
            'svd_solver': 'full'
        }, # parameters for the dimension reduction method
        'fix_dim_reduction': True, # whether dimension reduction method for each iteration is fixed (True) or retrained after each BO iteration
        'bo_params': {
            'model_type': 'GP', 
            'kernel': 'Matern52',
            'kernel_kwargs':{}, 
            #'sparse': True,
            #'num_inducing': 1500,
            'acquisition_type': 'EI',
            'acquisition_optimizer_type': 'lbfgs',
            'normalize_Y': True,
            'evaluator_type': 'thompson_sampling', 
            'batch_size': 10,
            #'acquisition_jitter': 10,
        }, # BO parameter: evaluator_type (choose from: 'sequen10ial'*, 'random', 'thompson_sampling', *NOTE: 'sequential' ignores batch_size and always return 1 new point), batch size (int)
        'ext_gp_method': None, # GP name, e.g., 'GPModel'
        'ext_gp_params': {
            'kernel': 'Matern52', 
            'kernel_kwargs':{}, 
            'sparse': False,
            'num_inducing': 500,
        }, # ignored if gp_method is None! GP parameter # default: sparse = False
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # default = 0.8 (but with different definition of search space restriction)
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # default = 0.8 (but with different definition of search space restriction)
        'deterministic': True # whether the decoder is deterministic or not
    },
    'training_params': {
        'num_train_samples': 10, # number of initial training samples before first BO step
        'num_test_samples': 100, # number of test samples for BO
    },
    'target_params': {
        'std_target': False,
        'constrained': False,
    }
}

MultipleBO_params = {
    'seed_list' : [123, 920, 4781, 13944, 101011], # the length of this list determines the number of Bayesian optimization executions
}

# The following parameters are used for genetic algorithm.
GeneticAlgorithm_params = {
    'algorithm_param': {
        'max_num_iteration': 249, # note that counting starts at 0 (i.e., for 10 iterations use max_num_iteration = 9)
        'population_size': 50,
        'mutation_probability': 0.1,
        'elit_ratio': 0.01,
        'crossover_probability': 0.5,
        'parents_portion': 0.3,
        'crossover_type': 'uniform',
        'max_iteration_without_improv': None,
    },
    'run_params':{
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # JR: value in MHG is 0.8
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # JR: value in MHG is 0.8
        'deterministic': True # whether the decoder is deterministic or not
    },
    'target_params': {
        'std_target': False,
        'constrained': False, # True: with applicability domain for GNN, False: without applicability domain for GNN (applicability domain restricts search space to molecues similar to the ones the GNN is trained for)
    }
}

MultipleGA_params = {
    'seed_list': [123, 920, 4781, 13944, 101011], # the length of this list determines the number of GA optimization executions
}


# The following parameters are used for random search.
RandomSearch_params = {
    'algorithm_param': {
        'max_num_iteration': 20000,
    },
    'run_params':{
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # JR: value in MHG is 0.8
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # JR: value in MHG is 0.8
        'deterministic': True # whether the decoder is deterministic or not
    },
    'target_params': {
        'std_target': False,
        'constrained': False, # True: with applicability domain for GNN, False: without applicability domain for GNN (applicability domain restricts search space to molecues similar to the ones the GNN is trained for)
    }
}

MultipleRandomSearch_params = {
    'seed_list': [123, 920, 4781, 13944, 101011], # the length of this list determines the number of GA optimization executions
}

