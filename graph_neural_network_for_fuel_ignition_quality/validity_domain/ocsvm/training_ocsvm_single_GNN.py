#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 14:36:05 2020

@author: christian
"""
import os

import pandas as pd
import numpy as np

from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.svm import OneClassSVM

import matplotlib.pyplot as plt

from trained_model.infer_DCN_MON_RON_single_mol import get_lats


def set_train_data(train_path):
    train_data_smiles = []
    counter = 0
    with open(train_path, 'r') as raw_data:
        data = raw_data.read().split('\n')[1:-1]
        for line in data:
            train_smiles_i = line.split(';')[1][:]
            train_data_smiles.append(train_smiles_i)
            counter += 1
    raw_data.close()
    
    for smi in train_data_smiles:
        train_latent_i = get_lats(smi, gnn_model_id)
    return train_data


def generate_kernel_selection_plot(x, nu, s_set=[1 * i for i in range(1, 50)], ylim=([0, 0.1])):
    support_vector_fracs = []
    np.random.shuffle(x)
    n = x.shape[0]
    for s in s_set:
        clf = OneClassSVM(gamma=1 / s ** 2, kernel="rbf", nu=nu, tol=1e-6)
        clf.fit(x)
        support_vector_fracs += [clf.support_vectors_.shape[0] / n]

    plt.scatter(s_set, support_vector_fracs, color="#00549F")
    # plt.legend(["support vector fraction"])
    plt.ylabel("support vector fraction")
    plt.xlabel("s (kernel width)")

    plt.show()


def __main__():
    inp_train_path = "../../Data/DCN_MON_RON_joined/Default/Train/raw/raw.csv"
    inp_gnn_model_id = 1
    train_data = set_train_data(train_path = inp_train_path, gnn_model_id = inp_gnn_model_id)
    
__main__()


data = pd.read_csv(os.path.join(os.path.dirname(__file__), "ocsvm_data.txt"), header=None, sep=" ").values
data = data[0][0:-1]
data = data.reshape((-1, 128))

X = data

# zero mean unit variance scaling

input_scaler = StandardScaler()
input_scaler.fit(X)
X_scaled = input_scaler.transform(X)

"""
Nur zur Info:
    
größerer Kernel = Generalisierung
kleiner Kernel = Overfitting
SupportvectorFraction entrspricht ungefähr der error Fraction (Punkte die 
Inlier sind, aber als Outlier detektiert werden) in einem unabh.
Testset der gleichen Verteilung. Idee war, dass man den Kernel vergräßert, 
bis diese error fraction nicht mehr stark abnimmt. 

"""

generate_kernel_selection_plot(X_scaled, nu=0.02)

clf = OneClassSVM(nu=.02, gamma=1 / 19 ** 2)
clf.fit(X_scaled)


def constraint_function(latent):
    if type(latent) == dict:
        return -1
    return clf.predict([latent])[0]

    # # inlier sind > 0, outlier sind < 0
    # return clf.decision_function(input_scaler.transform(X))

