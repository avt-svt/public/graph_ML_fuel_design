import os
import os.path as osp
import csv

from trained_model.predict_DCN_MON_RON_single_mol import get_props


path = 'qm9_predicted_props.csv'
with open(path, 'r') as f, open('qm9_predicted_props_newer.csv', 'a') as out:
    writer = csv.writer(out, delimiter=',')
    data = f.read().split('\n')[3494:-1]
    counter = 0
    for line in data:
        c1 = line.split(',')[0][:]
        c2 = line.split(',')[1][:]
        c3 = line.split(',')[2][:]
        c5 = "<placeholder>" #line.split(',')[4][:]
        tmp_smiles = line.split(',')[3][:]
        print(tmp_smiles)
        props = get_props(tmp_smiles)
        t_dcn = props['DCN']
        t_mon = props['MON']
        t_ron = props['RON']
        writer.writerow([c1, c2, c3, tmp_smiles, c5, t_dcn, t_mon, t_ron])
            
