#!/usr/local_rwth/bin/zsh


#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4096M  
#SBATCH --nodes=1 
#SBATCH --time=0-10:00:00

### begin of executable commands

module load gcc/6
module load cuda

export PATH=~/anaconda3/bin:$PATH
export PYTHONPATH=$PYTHONPATH:/home/jr629406/VAE_MolDesign/2020_gnn_autoencoder/icml18_jtnn
export PYTHONPATH=$PYTHONPATH:/home/jr629406/VAE_MolDesign/2020_gnn_autoencoder/graph_neural_network_for_fuel_ignition_quality
source activate jtnn_py37

python process_qm9.py
