import os
import pickle
import gzip
from rdkit import DataStructs
from rdkit import Chem
from rdkit.Chem import QED
from rdkit.Chem import Crippen

import math
import numpy as np
import pandas as pd

from trained_model.infer_DCN_MON_RON_single_mol import get_lats
from ad_methods.support_vector_machine.ocsvm import SVM_AD

## pre-calculated validity scores for SMILES in QM9
# kgnn (multi-task ensemble)
df_validity_kgnn = pd.read_csv(os.path.join(os.path.dirname(__file__), '../data/qm9/validity_smiles_qm9.txt'), delimiter=',', names=['smiles', 'validity'])

class PredictionValidity(object):

    @staticmethod
    def validity_score(smiles, method, model='kgnn', target=None, **kwargs):
        if method == 'svm':
            return PredictionValidity.svm_classify(smiles, model, target, kwargs)
        else:
            raise RuntimeError('Method unknown: {}'.format(method))

    @staticmethod
    def svm_classify(smiles, model, target, kwargs={}):
    # svm either returns 1 (valid) or -1 (invalid)
        if model == 'kgnn':
            # Check if molecule is in qm9. If yes, get validity from preprocessed file.
            val_score = []
            for smi in smiles:
                if not df_validity_kgnn[df_validity_kgnn['smiles'] == smi].empty:
                    val_score.append(df_validity_kgnn[df_validity_kgnn['smiles'] == smi]['validity'].iloc[0])
            if len(val_score) == len(smiles):
                return np.array(val_score).reshape(-1)
            
            # get latent vectors of smiles
            smiles_latent = []
            for smi in smiles:
                if smi is None or not PredictionValidity.valid_lambda_special(Chem.MolFromSmiles(smi)):
                    smiles_latent.append(None)
                else:
                    smiles_latent.append(get_lats(smi))

            # initialize SVM
            svm_ad = SVM_AD()

            val_score = []
            # evaluate each SVM for a GNN within the ensemble (in GNN_FIQ_Schweidtmann -> 40 GNNs -> 40 SVMs evaluations)
            num_models = 40 # number of models in GNN ensemble, TODO: make it variable
            for model_id in range(1,num_models+1):
                kernel, gamma, nu, tol = 'rbf', 'scale', 0.05, 0.001 # select hyperparameters
                model_name = "hp_test_{}_{}_{}_{}_GNN{}".format(kernel, gamma, nu, tol, model_id)
                svm_ad.load_model(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "../gnn_validity/pretrained_models/support_vector_machine/GNN_FIQ_Schweidtmann"), model_name) # workaround -> TODO: change path dep
                val_score_i = []
                for smi_lat in smiles_latent:
                    if smi_lat is None:
                        val_score_i.append(-1)
                    else:
                        tmp_latent_vec = smi_lat[model_id-1][0][1]
                        tmp_val_score = svm_ad.predict([tmp_latent_vec])
                        val_score_i.append(tmp_val_score)
                val_score.append(val_score_i)

            # majority vote across SVMs for ensembled GNNs (in total: 40 votes of either 1 or -1)
            val_score = np.array(val_score).sum(axis=0) # sum votes (note that sum cannot be odd number)
            val_score = val_score.clip(max=1) # if sum >= 1 -> valid -> 1
            val_score[np.where(val_score<1)] = -1 # if sum <= 0 -> invalid -> -1
            return val_score.reshape(-1)          
        else: 
            return NotImplementedError(f'Validity score for {model} not implemented yet.')


    @staticmethod
    def valid_lambda(x):
        return x is not None and Chem.MolToSmiles(x) != ''

    @staticmethod
    def valid_lambda_special(x):
        s = Chem.MolToSmiles(x) if x is not None else ''
        return x is not None and '*' not in s and '.' not in s and s != ''
