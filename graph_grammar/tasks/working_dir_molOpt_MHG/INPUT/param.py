#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Title """

__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "Jan 25 2018"

from copy import deepcopy

# ** ALL OF THE PARAMETERS in DataPreprocessimg_params SHOULD NOT BE CHANGED **
DataPreprocessing_params = {
    'kekulize': True, # Use kekulized representation or not
    'add_Hs': False, # Add hydrogens explicitly or not
    'all_single': True, # Represent every bond as a labeled single edge
    'tree_decomposition': 'molecular_tree_decomposition', # Tree decomposition algorithm
    'tree_decomposition_kwargs': {}, # Parameters for the tree decomposition algorithm
    'ignore_order': False # Ignore the orders of nodes in production rules
}

# Parameters for training a variational autoencoder (default)
Train_params = {
    'model': 'GrammarSeq2SeqVAE', # Model name
    'model_params' : { # Parameter for the model
        'latent_dim': 72, # Dimension of the latent dimension
        'max_len': 80, # maximum length of input sequences (represented as sequences of production rules)
        'batch_size': 128, # batch size for training
        'padding_idx': -1, # integer used for padding
        'start_rule_embedding': False, # Embed the starting rule into the latent dimension explicitly
        'encoder_name': 'GRU', # Type of encoder
        'encoder_params': {'hidden_dim': 384, # hidden dim
                           'num_layers': 3, # the number of layers
                           'bidirectional': True, # use bidirectional one or not
                           'dropout': 0.1}, # dropout probability
        'decoder_name': 'GRU', # Type of decoder
        'decoder_params': {'hidden_dim': 384, # hidden dim
                           'num_layers': 3, # the number of layers
                           'dropout': 0.1}, # dropout probability
        'prod_rule_embed': ['Embedding',
                            'MolecularProdRuleEmbedding',
                            'MolecularProdRuleEmbeddingLastLayer',
                            'MolecularProdRuleEmbeddingUsingFeatures'][0], # Embedding method of a production rule. The simple embedding was the best, but each production rule could be embedded using GNN
        'prod_rule_embed_params': {'out_dim': 128, # embedding dimension
                                   'layer2layer_activation': 'relu', # not used for `Embedding`
                                   'layer2out_activation': 'softmax', # not used for `Embedding`
                                   'num_layers': 4}, # not used for `Embedding`
        'criterion_func': ['VAELoss', 'GrammarVAELoss'][1], # Loss function
        'criterion_func_kwargs': {'beta': 0.01}}, # Parameters for the loss function. `beta` specifies beta-VAE.
    'sgd': 'Adam', # SGD algorithm
    'sgd_params': {
        'lr': 5e-4 # learning rate of SGD
    },
    'seed_list': [123, 425, 552, 1004, 50243], # seeds used for restarting training
    'inversed_input': True, # the input sequence is in the reversed order or not.
    #'num_train': 220011, # the number of training examples # JR: redundant because of part_train
    #'num_val': 24445, # the number of validation examples # JR: redundant because of part_val
    #'num_test': 5000, # the number of test examples # JR: redundant because of part_test
    'num_early_stop': 50223, # the number of examples used to find better initializations (=seed)
    'num_epochs': 30, # the number of training epochs
    'part_train': 0.9,
    'part_val': 0.1,
    'part_test': 0, # JR is actually ignored because part_test = 1 - part_train - part_test
    'std_target': True, # whether target values are standardized for training
    'inference': {
        'time_limit_decoding': 10
    },
    'data': '../../../../data/qm9/raw/HCO_smiles.txt',
    'seed_dataloader': 123
}

TrainWithPred_params = deepcopy(Train_params) # used to train VAE + predictor
TrainWithPred_params['model'] = 'GrammarSeq2SeqVAEWithPred'
TrainWithPred_params['model_params']['predictor_list'] = ['Linear'] # Configuration of layers of predictors
TrainWithPred_params['model_params']['predictor_out_dim_list'] = [1] # each layer's output dimension


ComputeTargetValues_params = {
}

ConstructDatasetForBO_params = {
    'batch_size': 128,
    'seed': 123
}

# Parameter for molecular optimization
MolOpt_params = {
	'target': 'RON-MON',
    'max_num_mols': 1000,
}

# The following parameter is used for global optimization in the limited oracle case.
# It uses GPyOpt
BayesianOptimization_params = {
    'run_params': {
        'bo_num_iter': 1000, # The number of Bayesian optimization iterations
        'dim_reduction_method': 'PCA', #'PCA', # a method to reduce the dimensionality of the latent space
        'dim_reduction_params': {
            'n_components': 0.999, 
            'svd_solver': 'full'
        }, # parameters for the dimension reduction method
        'fix_dim_reduction': True, # whether dimension reduction method for each iteration is fixed (True) or retrained after each BO iteration
        'bo_params': {
            'model_type': 'GP', 
            'kernel': 'Matern52',
            'kernel_kwargs':{}, 
            #'num_inducing': 1500, # ignored when model_type != sparseGP
            'acquisition_type': 'EI',
            'acquisition_optimizer_type': 'lbfgs',
            'normalize_Y': True,
            'evaluator_type': 'thompson_sampling', 
            'batch_size': 10,
            #'acquisition_jitter': 0.1
        }, # BO parameter: evaluator_type (choose from: 'sequential'*, 'random', 'thompson_sampling', *NOTE: 'sequential' ignores batch_size and always return 1 new point), batch size (int)
        'ext_gp_method': None, # GP name, e.g., 'GPModel'
        'ext_gp_params': {
            'kernel': 'Matern52', 
            'kernel_kwargs':{}, 
            'sparse': True,
            'num_inducing': 1500,
        }, # ignored if gp_method is None! GP parameter # default: sparse = False
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # default = 0.8 (but with different definition of search space restriction)
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # default = 0.8 (but with different definition of search space restriction)
        'deterministic': True # whether the decoder is deterministic or not
    },
    'training_params': {
        'num_train_samples': 10, # number of initial training samples before first BO step
        'num_test_samples': 100, # number of test samples for BO
    },
    'target_params': {
        'std_target': False,
        'constrained': False,
    }
}

MultipleBO_params = {
    'seed_list' : [123, 920, 4781, 13944, 101011], # the length of this list determines the number of Bayesian optimization executions
}

# The following parameters are used for genetic algorithm.
GeneticAlgorithm_params = {
    'algorithm_param': {
        'max_num_iteration': 249, # note that counting starts at 0 (i.e., for 10 iterations use max_num_iteration = 9)
        'population_size': 50,
        'mutation_probability': 0.1,
        'elit_ratio': 0.01,
        'crossover_probability': 0.5,
        'parents_portion': 0.3,
        'crossover_type': 'uniform',
        'max_iteration_without_improv': None,
    },
    'run_params':{
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # JR: value in MHG is 0.8
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # JR: value in MHG is 0.8
        'deterministic': True # whether the decoder is deterministic or not
    },
    'target_params': {
        'std_target': False,
        'constrained': False, # True: with applicability domain for GNN, False: without applicability domain for GNN (applicability domain restricts search space to molecues similar to the ones the GNN is trained for)
    }
}

MultipleGA_params = {
    'seed_list': [123, 920, 4781, 13944, 101011], # the length of this list determines the number of GA optimization executions
}


# The following parameters are used for random search.
RandomSearch_params = {
    'algorithm_param': {
        'max_num_iteration': 20000,
    },
    'run_params':{
        'min_coef': 1.2, # the size of a search space relative to the empirical latent representations (negative sphere). # JR: value in MHG is 0.8
        'max_coef': 1.2, # the size of a search space relative to the empirical latent representations (positive sphere). # JR: value in MHG is 0.8
        'deterministic': True # whether the decoder is deterministic or not
    },
    'target_params': {
        'std_target': False,
        'constrained': False, # True: with applicability domain for GNN, False: without applicability domain for GNN (applicability domain restricts search space to molecues similar to the ones the GNN is trained for)
    }
}

MultipleRandomSearch_params = {
    'seed_list': [123, 920, 4781, 13944, 101011], # the length of this list determines the number of GA optimization executions
}

