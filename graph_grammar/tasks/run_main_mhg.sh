### begin of executable commands

export PATH=~/anaconda3/bin:$PATH
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/graph_neural_network_for_fuel_ignition_quality
export PYTHONPATH=$PYTHONPATH:/<path_to_repo>/gnn_validity
source activate graph_grammar_36

# choose one of the pipelines: Train, TrainWithPred, MultipleBO, MultipleBOWithPred, MultipleGA, MultipleGAWithPred, MultipleRandomSearch (WithPred: generator is additionally trained to predict target property -> different generator for each target property; we did use a generator without prediction)
python main_mhg.py MultipleBO --working-dir working_dir_molOpt_MHG --workers 1 --use-gpu --target RON+OS --max-opt-time 43200 
# optional: --single-seed 4781 --max-num-mols 1000 --constrained
