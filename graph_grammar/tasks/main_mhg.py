#!/usr/bin/env python
# -*- coding: utf-8 -*-

#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/graph_ML_fuel_design.git
#
#*********************************************************************************

# original author and copyright
__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "Jan 24 2018"
# adapted by Stefanie Winkler, Jan Rittig, and Martin Ritzert

# set luigi_config_path BEFORE importing luigi
import argparse
import os
import sys
sys.path.append('..')
sys.path.append('../..')

# utils needed for predictions, AD, saving/loading
from utils.molecular_metrics import MolecularMetrics
from utils.prediction_validity import PredictionValidity 
from utils.utils_base import save_object, load_object

# set up working dir
try:
    working_dir = sys.argv[1:][sys.argv[1:].index("--working-dir") + 1]
    os.chdir(working_dir)
except ValueError:
    raise argparse.ArgumentError("--working-dir option must be specified.")
# add a path to luigi.cfg
os.environ["LUIGI_CONFIG_PATH"] = os.path.abspath(os.path.join("INPUT", "luigi.cfg"))
sys.path.append(os.path.abspath(os.path.join("INPUT")))
from param import DataPreprocessing_params, Train_params, TrainWithPred_params, ConstructDatasetForBO_params, ComputeTargetValues_params, BayesianOptimization_params, MultipleBO_params, GeneticAlgorithm_params, MultipleGA_params, RandomSearch_params, MultipleRandomSearch_params, MolOpt_params

# imports
from copy import deepcopy
from datetime import datetime
from luigine.abc import MainTask, AutoNamingTask, main
from graph_grammar.algo.tree_decomposition import (tree_decomposition,
                                                   tree_decomposition_with_hrg,
                                                   tree_decomposition_from_leaf,
                                                   topological_tree_decomposition,
                                                   molecular_tree_decomposition)
from graph_grammar.bo.mol_opt import MolecularOptimization

from graph_grammar.graph_grammar.hrg import HyperedgeReplacementGrammar as HRG
from graph_grammar.io.smi import HGGen, hg_to_mol
from graph_grammar.nn.dataset import HRGDataset, batch_padding
from graph_grammar.nn.autoencoder import (GrammarSeq2SeqVAE)
from graph_grammar.nn.autoencoder_with_predictor import GrammarSeq2SeqVAEWithPred
import rdkit.Chem
from torch.utils.data import DataLoader
from torch.autograd import Variable
from torch.optim import Adagrad, RMSprop, Adam
import glob
import gzip
import luigi
import logging
import numpy as np
import pickle
import torch
import traceback
import random
import time
import concurrent.futures as futures
import json

import faulthandler; faulthandler.enable()

logger = logging.getLogger('luigi-interface')
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

td_catalog = {
    'tree_decomposition': tree_decomposition,
    'tree_decomposition_with_hrg': tree_decomposition_with_hrg,
    'tree_decomposition_from_leaf': tree_decomposition_from_leaf,
    'topological_tree_decomposition': topological_tree_decomposition,
    'molecular_tree_decomposition': molecular_tree_decomposition
}

ae_catalog = {
    'GrammarSeq2SeqVAE': GrammarSeq2SeqVAE,
    'GrammarSeq2SeqVAEWithPred': GrammarSeq2SeqVAEWithPred
}

sgd_catalog = {
    'Adagrad': Adagrad,
    'Adam': Adam,
    'RMSprop': RMSprop
}


# ensure consistent results
def seed_everything(seed=1234):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
seed_everything(seed=int(Train_params['seed_list'][0]))

############### HELPER FUNCTIONS for MHG #################

def get_dataloaders(hrg, prod_rule_seq_list, train_params,
                    target_val_list=None, batch_size=None, pre_shuffle=True, shuffle=True):
    
    '''Dataloader for train, and optional: val/test
    
    This function returns dataloader objects for training, validation (optional), and testing (optional) suited for the MHG model.

    Parameters
    ----------
    hrg: Hypergraph Rule Grammar
        This includes the grammar.
    prod_rule_seq_list: List of lists
        Each element corresponds to a sequence of production rules.
    train_params: dict
        Training parameters of MHG model, self.Train_params.
    target_val_list: List, optional
        Each element corresponds to the target value of the corresponding element in prod_rule_seq_list.
        Default is None.
    batch_size: int, optional
        Batch size of dataloaders.
        Default is specified in the model parameters of the MHG-model.
    pre_shuffle: Boolean, optional
        Whether the data is shuffled before setting up the dataloaders (True) or not (False).
        Default is True.
    shuffle: Boolean, optional
        Whether the dataloader object includes shuffling (True) or not (False).
        Default is True.

    Returns
    -------
    Dataloaders for train, val, test of autoencoders
        Each batch contains two torch Tensors, each of which corresponds to input and output of autoencoder.
    '''
    
    num_train = int(len(prod_rule_seq_list) * train_params['part_train'])
    num_val = int(len(prod_rule_seq_list) * train_params['part_val'])
    num_test = len(prod_rule_seq_list) - num_train - num_val

    print(f"\nLength prod rule: {len(prod_rule_seq_list)}")
    print(f"Num train {num_train}")
    print(f"Num val {num_val}")
    print(f"Num test {num_test}")
    
    if batch_size is None:
        batch_size = train_params['model_params']['batch_size']
        
    # shuffle data    
    if pre_shuffle:
        if target_val_list is not None:
            tmp_data = list(zip(prod_rule_seq_list, target_val_list))
            random.shuffle(tmp_data)
            prod_rule_seq_list, target_val_list = zip(*tmp_data)
        else:
            random.shuffle(prod_rule_seq_list)
        
    # select datapoints
    prod_rule_seq_list_train = prod_rule_seq_list[:num_train]
    prod_rule_seq_list_val = prod_rule_seq_list[num_train:num_train+num_val]
    prod_rule_seq_list_test = prod_rule_seq_list[num_train+num_val:num_train+num_val+num_test]
    
    # select correspodning target values if provided
    if target_val_list is None:
        target_val_list_train = None
        target_val_list_val = None
        target_val_list_test = None
    else:
        target_val_list_train = target_val_list[:num_train]
        target_val_list_val = target_val_list[num_train:num_train+num_val]
        target_val_list_test = target_val_list[num_train +num_val:num_train+num_val+num_test]
        # standardize targets
        if train_params['std_target']:
            target_val_list_train_mean, target_val_list_train_std = np.mean(target_val_list_train), np.std(target_val_list_train)
            target_val_list_train = (target_val_list_train - target_val_list_train_mean) / target_val_list_train_std
            target_val_list_val = (target_val_list_val - target_val_list_train_mean) / target_val_list_train_std
            target_val_list_test = (target_val_list_test - target_val_list_train_mean) / target_val_list_train_std
            
    # training dataloader
    hrg_dataset_train = HRGDataset(hrg,
                                   prod_rule_seq_list_train,
                                   train_params['model_params']['max_len'],
                                   target_val_list=target_val_list_train,
                                   inversed_input=train_params['inversed_input']
                                  )
    hrg_dataloader_train = DataLoader(dataset=hrg_dataset_train,
                                      batch_size=batch_size,
                                      shuffle=shuffle, drop_last=False
                                     )
    
    # validation dataloader
    if num_val != 0:
        hrg_dataset_val = HRGDataset(hrg,
                                     prod_rule_seq_list_val,
                                     train_params['model_params']['max_len'],
                                     target_val_list=target_val_list_val,
                                     inversed_input=train_params['inversed_input']
                                    )
        hrg_dataloader_val = DataLoader(dataset=hrg_dataset_val,
                                        batch_size=batch_size,
                                        shuffle=shuffle, drop_last=False
                                       )
    else:
        hrg_dataset_val = None
        hrg_dataloader_val = None
    
    # test dataloader
    if num_test != 0:
        hrg_dataset_test = HRGDataset(hrg,
                                      prod_rule_seq_list_test,
                                      train_params['model_params']['max_len'],
                                      target_val_list=target_val_list_test,
                                      inversed_input=train_params['inversed_input']
                                     )
        hrg_dataloader_test = DataLoader(dataset=hrg_dataset_test,
                                         batch_size=batch_size,
                                         shuffle=shuffle, drop_last=False
                                        )
    else:
        hrg_dataset_test = None
        hrg_dataloader_test = None
        
    return hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test

############### HELPER FUNCTIONS for OPTIMIZATION #################

def timeout(timelimit, verbose=True):
    '''Timeout function with timelimit.
    
    This function stops another function call after a given time limit is exceeded.
    
    Parameters
    ----------
    timelimit: float
        Time limit in seconds after which function call is stopped.
    '''
    
    def decorator(func):
        def decorated(*args, **kwargs):
            with futures.ThreadPoolExecutor(max_workers=1) as executor:
                future = executor.submit(func, *args, **kwargs)
                tmp_t0 = time.time()
                try:
                    result = future.result(timelimit)
                except (futures.TimeoutError, RuntimeError) as error:
                    if verbose: print(f"Timeout! -> Error: {error}")
                    result =  None
                else:
                    if verbose: print(result)
                    pass
                tmp_t1 = time.time()
                if verbose: print(f"\nTotal time for decoding: {tmp_t1-tmp_t0:.3f} sec, (time limit: {timelimit} sec).")
                executor._threads.clear()
                futures.thread._threads_queues.clear()
                return result
        return decorated
    return decorator

def get_decoded_smiles(new_x, model, batch_size, latent_dim=None, deterministic=True):
    '''Decoding a latent vector into a SMILES string with MHG-model
    
    Parameters
    ----------
    new_x: np.array (latent_dim,)
        The latent vector to be encoded.
    model: object
        MHG-model instance.
    batch_size: int
        Batch size used in MHG-model.
    latent_dim: int, optional
        Dimension of latent space in MHG-model.
        Default is dimension of new_x.
    deterministic: boolean, optional.
        Whether decoding in MHG-model is deterministic (True) or not (False).
        Default is True.
        
    Notes
    ----------
    Decoding similar to graph_grammar from bo script in Kajino's original repository (https://github.com/ibm-research-tokyo/graph_grammar).
    It is possible that MHG-models returns different results w.r.t. different seeds -> set torch.manual_seed  when model is loaded!
    '''

    if not latent_dim: latent_dim = new_x.shape[0]
    
    model.init_hidden()
    batch_z = torch.zeros(batch_size, latent_dim)
    batch_z[0, :] = torch.FloatTensor(new_x.flatten())
    _, hg_list = model.decode(z=batch_z, deterministic=deterministic, return_hg_list=True)
    try:
        mol = hg_to_mol(hg_list[0])
        smiles = rdkit.Chem.MolToSmiles(mol)
    except:
        smiles = None
    if model.use_gpu:
        torch.cuda.empty_cache()
    return smiles


############### TASKS #################

class DataPreprocessing(AutoNamingTask):
    '''Data Preprocessing MHG
    
    This tasks preprocesses data for MHG and extracts HRG.
    '''
    
    Train_params = luigi.DictParameter(default=Train_params)
    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    working_subdir = luigi.Parameter(default="data_prep")

    def requires(self):
        return []

    def run(self):
        hg_list = HGGen(os.path.join("INPUT", self.Train_params['data']),
                        kekulize=self.DataPreprocessing_params['kekulize'],
                        add_Hs=self.DataPreprocessing_params['add_Hs'],
                        all_single=self.DataPreprocessing_params['all_single'])
        hrg = HRG(tree_decomposition=td_catalog[self.DataPreprocessing_params['tree_decomposition']],
                  ignore_order=self.DataPreprocessing_params['ignore_order'],
                  **self.DataPreprocessing_params['tree_decomposition_kwargs'])
        prod_rule_seq_list = hrg.learn(hg_list, logger=logger.info,
                                       max_mol=self.DataPreprocessing_params.get('max_mol', np.inf))
        # prod_rule_seq_list = hrg.learn(hg_list, logger=logger.info, max_mol=2000)
        logger.info(" * the number of prod rules is {}".format(hrg.num_prod_rule))

        if self.DataPreprocessing_params.get('draw_prod_rule', False):
            if not os.path.exists(os.path.join('OUTPUT', self.working_subdir, 'prod_rules')):
                os.mkdir(os.path.join('OUTPUT', self.working_subdir, 'prod_rules'))
            for each_idx, each_prod_rule in enumerate(hrg.prod_rule_corpus.prod_rule_list):
                each_prod_rule.draw(os.path.join('OUTPUT', self.working_subdir, 'prod_rules', f'{each_idx}'))
        with gzip.open(self.output().path, "wb") as f:
            pickle.dump((hrg, prod_rule_seq_list), f)

    def load_output(self):
        with gzip.open(self.output().path, "rb") as f:
            hrg, prod_rule_seq_list = pickle.load(f)
        return hrg, prod_rule_seq_list


class Train(AutoNamingTask):
    '''Train MHG
    
    This tasks trains an MHG model without target values.
    '''
    output_ext = luigi.Parameter('pth')

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="train_mhg")

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            )
        ]

    def run(self):
        seed_everything(self.Train_params['seed_list'][0])
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        
        class_weight = None
        max_len = -1
        for each_prod_rule_seq in prod_rule_seq_list:
            if max_len < len(each_prod_rule_seq):
                max_len = len(each_prod_rule_seq)
        logger.info(f'max_len = {max_len}')
        min_val_loss = np.inf

        # pre-study to determine training seed
        best_seed = None
        for each_seed in self.Train_params['seed_list']:
            
            # load data for pre-study
            seed_everything(self.Train_params['seed_dataloader'])
            hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test \
                = get_dataloaders(hrg, prod_rule_seq_list, self.Train_params)
            if hrg_dataloader_train: print(f'Number of training data points: {len(hrg_dataloader_train)}')
            if hrg_dataloader_val: print(f'Number of validation data points: {len(hrg_dataloader_val)}')
            if hrg_dataloader_test: print(f'Number of testing data points: {len(hrg_dataloader_test)}')
                
            # load model with respective seeds
            seed_everything(each_seed)
            model = ae_catalog[self.Train_params['model']](
                hrg=hrg, class_weight=class_weight,
                **self.Train_params['model_params'], use_gpu=self.use_gpu)
            if self.use_gpu:
                model.cuda()
            train_loss, val_loss = model.fit(hrg_dataloader_train,
                                             data_loader_val=hrg_dataloader_val,
                                             max_num_examples=self.Train_params['num_early_stop'],
                                             print_freq=100,
                                             num_epochs=self.Train_params['num_epochs'],
                                             sgd=sgd_catalog[self.Train_params['sgd']],
                                             sgd_kwargs=self.Train_params['sgd_params'],
                                             logger=logger.info)
            if val_loss < min_val_loss:
                min_val_loss = val_loss
                best_seed = each_seed
        logger.info(f'best_seed = {best_seed}\tval_loss = {min_val_loss}')
        
        # load data for final training
        seed_everything(self.Train_params['seed_dataloader'])
        hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test \
            = get_dataloaders(hrg, prod_rule_seq_list, self.Train_params)
        if hrg_dataloader_train: print(f'Number of training data points: {len(hrg_dataloader_train)}')
        if hrg_dataloader_val: print(f'Number of validation data points: {len(hrg_dataloader_val)}')
        if hrg_dataloader_test: print(f'Number of testing data points: {len(hrg_dataloader_test)}')

        # load model with final training seed
        seed_everything(best_seed)
        model = ae_catalog[self.Train_params['model']](
            hrg=hrg, class_weight=class_weight,
            **self.Train_params['model_params'], use_gpu=self.use_gpu)
        if self.use_gpu:
            model.cuda()
        train_loss, val_loss = model.fit(hrg_dataloader_train,
                                         data_loader_val=hrg_dataloader_val,
                                         print_freq=100,
                                         num_epochs=self.Train_params['num_epochs'],
                                         sgd=sgd_catalog[self.Train_params['sgd']],
                                         sgd_kwargs=self.Train_params['sgd_params'],
                                         logger=logger.info)
        torch.save((model.state_dict(), best_seed), self.output().path)

    def load_output(self):
        state_dict, mhg_best_training_seed = torch.load(self.output().path)
        return state_dict, mhg_best_training_seed
    
    
class TrainWithPred(AutoNamingTask):
    '''Train MHG with Predictions
    
    This task trains an MHG model with target value predicitons
    '''
    output_ext = luigi.Parameter('pth')

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    TrainWithPred_params = luigi.DictParameter(default=TrainWithPred_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    target= luigi.DictParameter(default=MolOpt_params['target'])
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="train_with_pred")
    rename = False

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.TrainWithPred_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            ComputeTargetValues(
                Train_params=self.TrainWithPred_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                target=self.target
            )
        ]
    

    def run(self):
        seed_everything(self.TrainWithPred_params['seed_list'][0])
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        y_all, y_raw = self.requires()[1].load_output()
        
        num_train = int(len(prod_rule_seq_list) * self.TrainWithPred_params['part_train'])
        prod_rule_seq_list_train = prod_rule_seq_list[: num_train]
        class_weight = None
        min_val_loss = np.inf
        best_seed = None
        
        # pre-study to determine training seed
        for each_seed in self.TrainWithPred_params['seed_list']:
            
            # load data for pre-study
            seed_everything(self.Train_params['seed_dataloader'])
            hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test \
                = get_dataloaders(hrg, prod_rule_seq_list,
                                  self.TrainWithPred_params, target_val_list=y_all.ravel())
            if hrg_dataloader_train: print(f'Number of training data points: {len(hrg_dataloader_train)}')
            if hrg_dataloader_val: print(f'Number of validation data points: {len(hrg_dataloader_val)}')
            if hrg_dataloader_test: print(f'Number of testing data points: {len(hrg_dataloader_test)}')
            
            # load model with respective seed
            seed_everything(each_seed)
            model = ae_catalog[self.TrainWithPred_params['model']](
                hrg=hrg, class_weight=class_weight,
                **self.TrainWithPred_params['model_params'], use_gpu=self.use_gpu)
            if self.use_gpu:
                model.cuda()
            train_loss, val_loss = model.fit(hrg_dataloader_train,
                                             data_loader_val=hrg_dataloader_val,
                                             max_num_examples=self.TrainWithPred_params['num_early_stop'],
                                             print_freq=100,
                                             num_epochs=self.TrainWithPred_params['num_epochs'],
                                             sgd=sgd_catalog[self.TrainWithPred_params['sgd']],
                                             sgd_kwargs=self.TrainWithPred_params['sgd_params'],
                                             logger=logger.info)
            print(f"TRAIN LOSS, VAL LOSS: {train_loss}, {val_loss}")
            if val_loss < min_val_loss:
                min_val_loss = val_loss
                best_seed = each_seed
        logger.info(f'best_seed = {best_seed}\tval_loss = {min_val_loss}')

        # load data for final training
        seed_everything(self.Train_params['seed_dataloader'])
        hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test \
            = get_dataloaders(hrg, prod_rule_seq_list,
                              self.TrainWithPred_params, target_val_list=y_all.ravel())
        if hrg_dataloader_train: print(f'Number of training data points: {len(hrg_dataloader_train)}')
        if hrg_dataloader_val: print(f'Number of validation data points: {len(hrg_dataloader_val)}')
        if hrg_dataloader_test: print(f'Number of testing data points: {len(hrg_dataloader_test)}')

        # load model with final training seed
        seed_everything(best_seed)
        model = ae_catalog[self.TrainWithPred_params['model']](
            hrg=hrg, class_weight=class_weight,
            **self.TrainWithPred_params['model_params'], use_gpu=self.use_gpu)
        if self.use_gpu:
            model.cuda()
        train_loss, val_loss = model.fit(hrg_dataloader_train,
                                         data_loader_val=hrg_dataloader_val,
                                         print_freq=100,
                                         num_epochs=self.TrainWithPred_params['num_epochs'],
                                         sgd=sgd_catalog[self.TrainWithPred_params['sgd']],
                                         sgd_kwargs=self.TrainWithPred_params['sgd_params'],
                                         logger=logger.info)
        print(f"TRAIN LOSS, VAL LOSS: {train_loss}, {val_loss}")
        torch.save((model.state_dict(), best_seed), self.output().path)

    def load_output(self):
        state_dict, mhg_best_training_seed = torch.load(self.output().path)
        return state_dict, mhg_best_training_seed
    
    
class TrainingPipeline(MainTask, AutoNamingTask):
    '''Training Pipeline MHG
    
    This tasks initializes the MHG training steps.
    '''

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = '' # required by AutoNamingTask
    
    def requires(self):
        return [
            Train(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                use_gpu=self.use_gpu
            )
        ]
    
    def output(self):
        return []
    
    def run(self):
        return []
    
    
class TrainingWithPredPipeline(TrainingPipeline):
    '''Training Pipeline MHG with Predictions
    
    This tasks initializes the MHG with predictions training steps.
    '''
    
    Train_params = luigi.DictParameter(default=TrainWithPred_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    target = luigi.DictParameter(default=MolOpt_params['target'])
    
    def requires(self):
        return [
            TrainWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                TrainWithPred_params=self.Train_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                use_gpu=self.use_gpu
            )
        ]
    
    def output(self):
        return []
    
    def run(self):
        return []


class ComputeTargetValues(AutoNamingTask):
    '''Compute Traget Values Tasks
    
    This task computes target values for provided molecules.
    '''
    Train_params = luigi.DictParameter(default=Train_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    target = luigi.DictParameter(default=MolOpt_params['target'])
    working_subdir = luigi.Parameter(default="target_values")
    constrained = luigi.BoolParameter(default=False)

    def requires(self):
        return []

    def run(self):
        print(f"Calculate values for property: {self.target}")
# find out which targets to compute

        perform_RON = False
        perform_MON = False
        perform_DCN = False
        special_normalization = False

        # allowed values are RON, MON, DCN, RON-MON, DCN+MON+RON, norm_DCN+MON+RON, RON+OS
        if 'DCN' in self.target:
            perform_DCN = True
        if 'MON' in self.target:
            perform_MON = True
        if 'RON' in self.target:
            perform_RON = True
        if 'OS' in self.target:
            perform_MON = True
            perform_RON = True

        # lists for raw regression data generated by the network
        dcn_list = []
        mon_list = []
        ron_list = []
        valid_mask = []
        molecule_counter = 0

        mol_gen = rdkit.Chem.SmilesMolSupplier(os.path.join("INPUT", self.Train_params['data']), titleLine=False)
        for each_mol in mol_gen:
            smi = [rdkit.Chem.MolToSmiles(each_mol)]
            molecule_counter = molecule_counter + 1
            valid_mask.append(True)
            
            if self.constrained:
                prediction_validity_score = PredictionValidity.validity_score(smi, "svm", model='kgnn', target=self.target)[0]
                if prediction_validity_score == -1:
                    dcn_list.append(-1000)
                    mon_list.append(-1000)
                    ron_list.append(-1000)
                    valid_mask[-1] = False
                    print(f"Invalid {smi}")
                    continue
            
            if perform_DCN: dcn_list.append(MolecularMetrics.dcn_score(smi)[0])
            if perform_MON: mon_list.append(MolecularMetrics.mon_score(smi)[0])
            if perform_RON: ron_list.append(MolecularMetrics.ron_score(smi)[0])


        # compute final target values
        y_all = 0
        if perform_DCN: y_all = y_all + np.array(dcn_list)
        if perform_MON: y_all = y_all + np.array(mon_list)
        if perform_RON: y_all = y_all + np.array(ron_list)
        # special case RON-MON
        if self.target == 'RON-MON':
            y_all = np.array(ron_list) - np.array(mon_list)
        elif self.target == 'RON+OS':
            y_all = np.array(ron_list) * 2 - np.array(mon_list)
        elif self.target == 'wRON+wOS':
            y_all = (np.array(ron_list) * (0.5/120)) + ((np.array(ron_list) - np.array(mon_list)) * (0.5/30))
        elif self.target == 'uRON+vOS':
            y_all = (np.array(ron_list) * (0.65/120)) + ((np.array(ron_list) - np.array(mon_list)) * (0.35/30))
        # store raw data
        y_raw = {'dcn_list': dcn_list, 'mon_list': mon_list, 'ron_list': ron_list} # some of them may be empty lists
        # save to disk
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((y_all, y_raw, valid_mask), f)


    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            y_all, y_raw, valid_mask = pickle.load(f)
        return y_all, y_raw, valid_mask
    
    
class GenerateLatentFiles(AutoNamingTask):
    '''Generate Latent Files Tasks
    
    This task generates latent vector embeddings of MHG models.
    '''
    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    use_gpu = luigi.BoolParameter()
    working_subdir = luigi.Parameter(default="latent_files")

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            Train(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                use_gpu=self.use_gpu
            ),
        ]
    
    def run(self):
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        state_dict, mhg_best_training_seed = self.requires()[1].load_output()

        #torch.manual_seed(mhg_best_training_seed)
        #np.random.seed(self.ConstructDatasetForBO_params['seed'])
        seed_everything(self.Train_params['seed_dataloader'])
        hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test \
            = get_dataloaders(hrg, prod_rule_seq_list, self.Train_params,
                              batch_size=self.ConstructDatasetForBO_params['batch_size'],
                              shuffle=False)
        seed_everything(mhg_best_training_seed)
        model_params = deepcopy(dict(self.Train_params['model_params']))
        model_params['batch_size'] = self.ConstructDatasetForBO_params['batch_size']
        model = ae_catalog[self.Train_params['model']](hrg=hrg, **model_params,
                                                       use_gpu=self.use_gpu)
        if self.use_gpu:
            model.cuda()
        model.load_state_dict(state_dict)
        torch.no_grad()

        if hrg_dataloader_train: print(f'Number of training data points: {len(hrg_dataloader_train)}')
        if hrg_dataloader_val: print(f'Number of validation data points: {len(hrg_dataloader_val)}')
        if hrg_dataloader_test: print(f'Number of testing data points: {len(hrg_dataloader_test)}')
        #print(len(hrg_dataloader_test), len(hrg_dataloader_train), len(hrg_dataloader_val))
        
        latent_vector_list = []
        for each_dataloader in [hrg_dataloader_train, hrg_dataloader_val, hrg_dataloader_test]:
            if each_dataloader is None: continue
            for each_batch in each_dataloader:
                each_batch, num_pad = batch_padding(each_batch,
                                                    self.ConstructDatasetForBO_params['batch_size'],
                                                    self.Train_params['model_params']['padding_idx'])
                in_batch, _ = each_batch
                in_batch = torch.LongTensor(np.mod(in_batch, model.vocab_size))
                model.init_hidden()
                in_batch_var = Variable(in_batch, requires_grad=False)
                if self.use_gpu:
                    in_batch_var = in_batch_var.cuda()

                mu, logvar = model.encode(in_batch_var)
                z = model.reparameterize(mu, logvar, False)
                z = z.cpu().cpu().detach().numpy()
                if num_pad:
                    z = z[:-num_pad]
                latent_vector_list.append(z)
        X_all = np.concatenate(latent_vector_list, 0)
        
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((X_all), f)

    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            X_all = pickle.load(f)
        return X_all 
    
class GenerateLatentFilesWithPred(GenerateLatentFiles):
    '''Generate Latent Files and Predictions Tasks
    This task generates latent vector embeddings of MHG models trained with predictions.
    '''
    Train_params = luigi.DictParameter(default=TrainWithPred_params)
    target = luigi.DictParameter(default=MolOpt_params['target'])
    
    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            TrainWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                TrainWithPred_params=self.Train_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                use_gpu=self.use_gpu
            ),
        ]

class ConstructDatasetForBO(AutoNamingTask):
    '''Construct Dataset for Bayesian Optimization Tasks
    
    This task prepares the dataset for Bayesian optimization, i.e., generates latent files of MHG model and corresponding predicitons.
    '''

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    target = luigi.DictParameter(default=MolOpt_params['target'])
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    use_gpu = luigi.BoolParameter()
    constrained = luigi.BoolParameter(default=False)
    working_subdir = luigi.Parameter(default="bo_dataset")

    def requires(self):
        return [
            GenerateLatentFiles(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params = self.Train_params,
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                use_gpu = self.use_gpu
            ),
            ComputeTargetValues(
                Train_params=self.Train_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                target=self.target,
                constrained=self.constrained,
            ),
        ]

    def run(self):
        X_all = self.requires()[0].load_output()

        # target val
        y_all, y_raw, valid_mask = self.requires()[1].load_output()

        assert X_all.shape[0] == len(y_all.ravel()), 'X_all and y_all have inconsistent shapes: {}, {}'.format(X_all.shape[0], len(y_all.ravel()))
        with gzip.open(self.output().path, 'wb') as f:
            pickle.dump((X_all, y_all, y_raw, valid_mask), f)

    def load_output(self):
        with gzip.open(self.output().path, 'rb') as f:
            X_all, y_all, y_raw, valid_mask = pickle.load(f)
        return X_all, y_all, y_raw, valid_mask


class ConstructDatasetForBOWithPred(ConstructDatasetForBO):
    '''Construct Dataset for Bayesian Optimization with Predictions Tasks
    
    This task prepares the dataset for Bayesian optimization, i.e., generates latent files of MHG model trained with predictions and corresponding predicitons.
    '''

    Train_params = luigi.DictParameter(default=TrainWithPred_params)

    def requires(self):
        return [
            GenerateLatentFilesWithPred(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params = self.Train_params,
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                use_gpu = self.use_gpu,
                target = self.target
            ),
            ComputeTargetValues(
                Train_params=self.Train_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                target=self.target,
                constrained=self.constrained,
            ),
        ]
    
class DrawResultPlots(AutoNamingTask):
    '''Draw Result Plots Task
    
    This tasks analyses the results and creates results plots of identified molecules.
    '''
    
    Train_params = luigi.DictParameter(default=Train_params)
    working_subdir = luigi.Parameter(default="result_plots")
    valid_smiles = luigi.Parameter(default=None)
    output_name = luigi.Parameter(default='best_mols')
    output_ext = luigi.Parameter(default='svg') 
    
    def output(self):
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.mkdir(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))
    
    
    def requires(self):
        return []
        
    def run(self):
        
        def plotting_results_and_save(valid_smiles_w_scores, training_smiles):
            import rdkit.Chem.Draw.IPythonConsole
            rdkit.Chem.Draw.IPythonConsole.ipython_useSVG=True

            # make unique list of molecules and sort it wrt target property
            unique_smiles = list(dict(valid_smiles_w_scores).items())
            unique_smiles.sort(key=lambda x: x[1], reverse=True) # sort smiles wrt target value descending

            num_mols = 30
            if len(unique_smiles) <= 30: num_mols = len(unique_smiles)
            smiles_to_plot = list(zip(*unique_smiles[:num_mols]))[0] # plot only smiles corresponding to top 30 highest target values
            # returns dcn, mon, and ron scores for a list of smiles strings
            def compute_scores(smiles):
                prediction_dcn = MolecularMetrics.dcn_score(smiles)
                prediction_ron = MolecularMetrics.ron_score(smiles)
                prediction_mon = MolecularMetrics.mon_score(smiles)
                return prediction_dcn, prediction_mon, prediction_ron
            dcn, mon, ron = compute_scores(smiles_to_plot)

            # check if which molecules are in training set
            training_canon_smiles = [(rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s))) for s in training_smiles]
            for i,s in enumerate(unique_smiles):
                tmp_s = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(s[0]))
                tmp_v = ''
                if tmp_s in training_canon_smiles:
                    tmp_v = 'in training set'
                else:
                    tmp_v = 'not in training set'
                unique_smiles[i] = (*unique_smiles[i], tmp_v)

            # Plot
            mols = [rdkit.Chem.MolFromSmiles(s) for s in smiles_to_plot]
            vals = [f"SMILES: {smi}, \n DCN: {dcn[i]:.2f}, MON: {mon[i]:.2f}, RON: {ron[i]:.2f}, \n RON-MON: {ron[i]-mon[i]:.2f} \n RON+OS: {2*ron[i]-mon[i]:.2f}" for i, smi in enumerate(smiles_to_plot)]
            img = rdkit.Chem.Draw.MolsToGridImage(mols, molsPerRow=5, subImgSize=(300, 300), legends=vals)
            with open(self.output().path, 'w') as f_handle:
                f_handle.write(img.data)

            # Write final results to csv
            import csv
            with open(os.path.join(os.path.dirname(self.output().path), f'{self.output_name}_list.csv'), 'w', newline="") as f_table:
                writer = csv.writer(f_table)
                writer.writerows(unique_smiles)

        # Plot results and save to working subdir        
        # we train with HCO-qm9 so training data is equal to qm9        
        training_data_path = os.path.join('INPUT', self.Train_params['data'])
        training_smiles = [x.strip("\r\n ") for x in open(training_data_path)]
        plotting_results_and_save(valid_smiles_w_scores=self.valid_smiles, training_smiles=training_smiles)
        
    def load_output(self):
        return []

    
class BayesianOptimization(AutoNamingTask):
    '''Bayesian Optimization Task
    
    This task executes Bayesian Optimization for MHG.
    '''
    
    constrained = luigi.BoolParameter(default=BayesianOptimization_params['target_params']['constrained'])

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    
    target= luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols= luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="bayesian_optimization")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            Train(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                use_gpu=self.use_gpu
            ),
            ConstructDatasetForBO(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params,
                ConstructDatasetForBO_params=self.ConstructDatasetForBO_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                use_gpu=self.use_gpu,
                target=self.target,
                constrained=self.constrained,
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))

    def run(self):
        
        import GPyOpt
        import GPy.kern
        import scipy as sp
        #import sklearn.random_projection.GaussianRandomProjection
        import sklearn.decomposition
        import csv
        
        ############ INITIALIZE MHG MODEL #############
        
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        state_dict, mhg_best_training_seed = self.requires()[1].load_output()
        
                
        # seed everything
        seed_everything(self.seed)
        
        torch.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed_all(mhg_best_training_seed)
        model_params = deepcopy(dict(self.Train_params['model_params']))
        model_params['batch_size'] = self.ConstructDatasetForBO_params['batch_size']
        model = ae_catalog[self.Train_params['model']](hrg=hrg, **model_params,
                                                       use_gpu=self.use_gpu)
        
        if self.use_gpu:
            model.cuda()
        model.load_state_dict(state_dict)
        torch.no_grad()

        
        
        ############ SET UP BO #############
        
        # general parameters for the genetic algorithm
        TARGET = self.target
        STD_TARGET = self.BayesianOptimization_params['target_params']['std_target']
        print(f"\nTarget property {TARGET} - standardized {STD_TARGET}")
        TIME_LIMIT_DECODING = self.Train_params['inference']['time_limit_decoding']
        print(f"\nTime limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nOptimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nOptimization will be stopped after {MAX_OPT_TIME} seconds.")
        CONSTRAINED = self.constrained
        print(f"\nBO: Constrained by applicability domain of GNN: {CONSTRAINED}")
        
        # copied from graph_grammar/src/graph_grammar/bo/mol_opt.py
        dim_reduction_catalog = {
            'PCA': sklearn.decomposition.PCA,
            'KernelPCA': sklearn.decomposition.KernelPCA,
            #'GaussianRandomProjection': GaussianRandomProjectionWithInverse
        }

        kern_catalog = {
            'Matern52': GPy.kern.Matern52,
            'RBF': GPy.kern.RBF
        }

        gp_catalog = {
            'GPModel': GPyOpt.models.gpmodel.GPModel,
            'GPModel_MCMC': GPyOpt.models.gpmodel.GPModel_MCMC
        }
        
        def run_gpyopt(
            X_train, y_train, 
            X_test=None, y_test=None,
            X_all=None, y_all=None,
            bo_num_iter=250,
            deterministic=True,
            min_coef=1.2, 
            max_coef=1.2,
            #num_train=1000,  # we directly provide correct number of training data points
            dim_reduction_method='PCA',
            dim_reduction_params={
                'n_components': 0.999, # if 0 < value < 1: n_components is selected according to explained varianece ratio
                'svd_solver': 'full', # should be full if 0 < n_compontens < 1
                #'fit_inverse_transform': True, # for kernelPCA
                #'kernel': 'Matern52' # for kernelPCA
            }, 
            fix_dim_reduction=True,
            bo_params={
                'model_type': 'GP', 
                'kernel': 'Matern52',
                'kernel_kwargs':{}, 
                'sparse': True,
                'num_inducing': 1500,
                'acquisition_type': 'EI',
                'acquisition_optimizer_type': 'lbfgs',
                'normalize_Y': True,
                'evaluator_type': 'thompson_sampling', 
                'batch_size': 10
            },
            ext_gp_method=None,
            ext_gp_params={
                'kernel': 'Matern52', 
                'sparse': True, 
                'num_inducing': 1500
            },
            logger=print
        ):
            ''' run molecular optimization

            Parameters
            ----------
            X_train: array-like, shape (num_train, dim)
                Latent vectors of molecules to be used for GP training, computed by MHG model.
            y_train: array-like, shape (num_train,)
                Predicted target values of molecules to be used for GP training.
            X_test: array-like, shape (num_test, dim)
                Latent vectors of molecules to be used for GP testing, computed by MHG model.
            y_test: array-like, shape (num_test,)
                Predicted target values of molecules to be used for GP testing (used to compute test RMSE and test log-likelihood for validation).
            X_all: array-like, shape (num_data, dim)
                Latent vectors of all molecules in the dataset, computed by MHG model.
            y_all: array-like, shape (num_train,)
                Predicted target values of all molecules in the dataset.
            bo_num_iter: int, optional
                The number of BO iterations.
                Default is 250.
            min_coef: float, optional
                The coefficient used to set the lower bounds for the search space for BO.
                Calculated as minimum of all latent vectors in the dataset in each dimension substracted with (min_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            max_coef: float, optional
                The coefficient used to set the upper bounds for the search space for BO.
                Calculated as maximum of all latent vectors in the dataset in each dimension increased by (max_coef-1)*latent_vector 
                span with latent_vector span being the difference of the maximum and minimum of all latent vectors in the dataset in each dimension.
                Default is 1.2.
            dim_reduction_method: string, optional (ignored if None)
                Dimension reduction for latent vectors.
                If None, no dimension reduction is applied.
                Calculated based on X_all.
                Default is PCA.
            dim_redcution_params: dict, optional
                Specific parameters for dimension reduction method.
            fix_dim_reduction: Boolean, optional
                If dimension reduction is applied, it can be defined whether the dimension reduction method is fixed (True) or retrained after each BO iteration (False).
                Default: True
            bo_params: dict, optional
                Specific parameters used in BO of GPyOpt.
                Default similar to GPyOpt with evaluator_type: thompson_sampling, batch_size: 10, and GP model of type sparse: True, num_inducing: 1500
            ext_gp_method: string, optional (ignored if None)
                External surrogate for BO within GPyOpt.
                If None, internal surrogate model of GPyOpt is used that can be defined in bo_params.
                Default is None.
            ext_gp_params: dict, optional (ignored if ext GP method is None!)
                Specific parameters for surrogate model used for BO.
                If ext_gp_method is None, this is ignored! Please specifiy GP parameters in bo_params!
                Default kernel: Matern52, sparse: True, num_inducing: 1500.

            Returns
            -------
            history : list of dicts
                in each dict,
                - 'mol_list' is a list of molecules that BO chooses
                - 'feature' is an array containing latent representations of the molecules
                - 'score_list' is a list of scores obtained by applying `target_func` to the molecules
            '''
            
            print(f"\n####################### Start BO with GPyOpt with seed {self.seed} #######################")
            if X_test is None:
                X_test = X_train[0:1, :]
                y_test = y_train[0:1]
                valid_test = False
            else:
                valid_test = True
            
            if X_all is None:
                X_all = np.concatenate((X_train, X_test),0)
                y_all = np.concatenate((y_train, y_test),0)
                
            STD_TARGET = False
           
            latent_dimension = X_all.shape[1]
            dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params) if dim_reduction_method else None
            if dim_reduction: 
                print(f"Dim reduction will be done by {dim_reduction_method} with params {dim_reduction_params} and samples: {X_all.shape}")
                # catch PCA set-up failure
                if dim_reduction_method == 'PCA':
                    tmp_n_components = dim_reduction_params['n_components']
                    if tmp_n_components < 1:
                        tmp_svd_solver = dim_reduction_params['svd_solver']
                        if tmp_svd_solver != 'full': raise ValueError(f"Dimension reduction set-up is fragile: n_components {tmp_n_components}, svd_solver: {tmp_svd_solver}.")
                # fit dimension reduction method with all available data points
                dim_reduction.fit(X_all)
                latent_dimension = dim_reduction.n_components_
                expl_var_r = dim_reduction.explained_variance_ratio_
                logger(f"\n\t----- Dim reduction by PCA fitted. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                logger(f"Components: {latent_dimension}, Total explained variance: {np.sum(expl_var_r)}")
                
            
            # center around 0 with std dev of 1
            y_train_mean, y_train_std = 0, 1 # default values -> transformation does not change numbers
            if STD_TARGET:
                y_train_mean, y_train_std = np.mean(y_train), np.std(y_train)
                y_train = (y_train - y_train_mean) / y_train_std
                y_test = (y_test - y_train_mean) / y_train_std
                print(f"\n{TARGET} are standardized.")
                print(f"Mean value for {TARGET} training data {y_train_mean}")
                print(f"Std dev value for {TARGET} training data {y_train_std}")
            
            X_train_excerpt = X_train
            y_train_excerpt = y_train
            X_all_excerpt = X_all

            if X_train.shape[0] != len(y_train.ravel()):
                raise ValueError('X_train and y_train have inconsistent shapes')
            if X_test.shape[0] != len(y_test.ravel()):
                raise ValueError('X_test and y_test have inconsistent shapes')
            
            y_train_excerpt = - y_train_excerpt.reshape(-1, 1) # minus target value
            y_test = - y_test.reshape(-1, 1) # minus target value

            print(f"BO: Shape of the training set excerpt {np.shape(X_train_excerpt)}")
            
            def get_kernel(params, latent_dimension):
                params_ = deepcopy(dict(params))
                kernel_name = params_.pop('kernel')
                kernel_kwargs = params_.pop('kernel_kwargs')
                gp_kernel = kern_catalog[kernel_name](input_dim=latent_dimension,**kernel_kwargs)
                return params_, gp_kernel
            
            bo_params_ = deepcopy(dict(bo_params))
            if ext_gp_method is not None:
                ext_gp_params_ = deepcopy(dict(ext_gp_params))
                if 'kernel' in ext_gp_params_:
                    ext_gp_params_, ext_gp_kernel = get_kernel(ext_gp_params, latent_dimension)
                ext_gp_model = gp_catalog[ext_gp_method](kernel=ext_gp_kernel, **ext_gp_params_)
                print(f"\nBO is executed with following parameters: {bo_params_} \nand using external surrogate model for BO: {ext_gp_model} with parameters {ext_gp_params} with kernel {ext_gp_kernel}.")
            else:
                if 'kernel' in bo_params_:
                    bo_params_, gp_kernel = get_kernel(bo_params_, latent_dimension)
                    bo_params_['kernel'] = gp_kernel
                print(f"BO is executed with following parameters: {bo_params_} with kernel {gp_kernel}.")
               
            # run BO for #bo_num_iter iterations
            valid_smiles = [] # SMILES and property values of all valid molecules found during optimization (chronologically)
            latent_vectors_bo = []
            look_up_predictions = {}
            look_up_validities = {}
            history = []
            bo_start_time = time.time()
            stopping_criterion_reached = False
            for bo_iter in range(bo_num_iter):
                print(f"\nBO: starting {bo_iter} iteration")
                
                # dimension reduction
                if dim_reduction:
                    if (fix_dim_reduction == False) and (bo_iter != 0):
                        # make sure dimension of PCA stays constant during optimization (only important in case when n_components < 1 to determine n_components automatically)
                        dim_reduction_params_ = deepcopy(dict(dim_reduction_params))
                        dim_reduction_params_['n_components'] = latent_dimension
                        dim_reduction = dim_reduction_catalog[dim_reduction_method](**dim_reduction_params_)
                        # refit dimension reduction method
                        dim_reduction.fit(X_all_excerpt)
                        expl_var_r = dim_reduction.explained_variance_ratio_
                        logger(f"\n\t----- BO: Dim reduction by PCA refitted in BO iteration {bo_iter}. -----") #": explained variance ratio {expl_var_r.cumsum()} -----")
                        logger(f"Components: {dim_reduction.n_components_}, Total explained variance: {np.sum(expl_var_r)}")
                    X_train_excerpt_low = dim_reduction.transform(X_train_excerpt)
                    X_test_low = dim_reduction.transform(X_test)
                    X_all_low = dim_reduction.transform(X_all)
                else:
                    X_train_excerpt_low = X_train_excerpt
                    X_test_low = X_test
                    X_all_low = X_all
                    
                print(f"BO: Shape of the training set for GP X: {np.shape(X_train_excerpt_low)}, y: {np.shape(y_train_excerpt)}")
                # number of training points can be limited by num of inducing points for sparse GPs
                try:
                    num_ind_pts = float('inf')
                    if bo_params_['model_type'] == 'sparseGP':
                        try:
                            num_ind_pts = bo_params_['num_inducing']
                        except:
                            num_ind_pts = 10 # default value in GPy
                    elif ext_gp_model:
                        try:
                            num_ind_pts = ext_gp_params_['num_inducing']
                        except:
                            num_ind_pts = 10 # default value in GPy
                    if num_ind_pts < np.shape(X_train_excerpt_low)[0]: print(f"BO: Number of training points for sparse GP is limited to: {num_ind_pts} which will be randomly selected from the training data.")
                except:
                    pass
                
                space = [
                    {
                        'name': f'x{each_idx}',
                        'type': 'continuous',
                        'domain': (
                            np.min(X_all_low[:, each_idx]) - (min_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx])),
                            np.max(X_all_low[:, each_idx]) + (max_coef - 1) * (np.max(X_all_low[:, each_idx]) - np.min(X_all_low[:, each_idx]))
                        )
                    }
                    for each_idx in range(X_all_low.shape[1])
                ]

                
                iter_bo_time_t0 = time.time()
                if ext_gp_method is not None:
                    ext_gp_model.updateModel(X_train_excerpt_low, y_train_excerpt, None, None)
                    logger(f"BO: External GP model used - GP model updated.")
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None, 
                        model=ext_gp_model,
                        domain=space, 
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )

                    # check ll and rmse on test set
                    test_pred, test_std = gp_model.predict(X_test_low, False)
                    test_err = np.sqrt(np.mean((test_pred - y_test)**2))
                    test_ll = np.mean(sp.stats.norm.logpdf(test_pred - y_test,
                                                           scale=test_std))

                    # check ll and rmse on training set
                    train_pred, train_std = gp_model.predict(X_train_excerpt_low, False)
                    train_err = np.sqrt(np.mean((train_pred - y_train_excerpt)**2))
                    train_ll = np.mean(sp.stats.norm.logpdf(train_pred - y_train_excerpt,
                                                            scale=train_std))
                    logger(f"BO: train_err: {train_err}\t train_ll: {train_ll}")
                    if valid_test:
                        logger(f"BO: test_err: {test_err}\t test_ll: {test_ll}")
                
                else:
                    # initialize BO, GP is initialized each time
                    bo_step = GPyOpt.methods.BayesianOptimization(
                        f=None,
                        domain=space,
                        X=X_train_excerpt_low, 
                        Y=y_train_excerpt,
                        **bo_params_
                    )
                
                # BO suggests next points to evaluate 
                X_next_low = bo_step.suggest_next_locations(ignored_X=X_train_excerpt_low)
                
                iter_bo_time_t1 = time.time()
                print(f"BO time needed for suggesting next locations (including initialization): {iter_bo_time_t1-iter_bo_time_t0:.1f} seconds.\n")
                
                # inverse dimension reduciton
                if dim_reduction:
                    X_next = dim_reduction.inverse_transform(X_next_low)
                else:
                    X_next = X_next_low

                # evaluate suggested points
                valid_smiles_current_batch = []
                num_uni_smiles_current_batch = []
                run_times_current_batch = []
                new_feature_array = []
                num_mol = len(valid_smiles)
                score_list = []
                raw_score_list = []
                for i in range(len(X_next)):
                    ### DECODING of the latent vectors by generative model ###
                    
                    # get smiles string from latent vector
                    latent_vector = X_next[i].flatten()
                    # store latent vectors
                    latent_vectors_bo.append(latent_vector.tolist())
                    
                    # get smiles string from parameters
                    # note that there long decoding times can occur
                    # restrict decoding time -> func-timeout)
                    @timeout(TIME_LIMIT_DECODING)
                    def get_smiles(new_x): 
                        smiles = get_decoded_smiles(new_x=new_x, model=model, batch_size=self.Train_params['model_params']['batch_size'], latent_dim=self.Train_params['model_params']['latent_dim'], deterministic=deterministic)
                        return smiles

                    smiles = get_smiles(latent_vector)
                    print(f"BO: Found following SMILES string: {smiles}\n")
                    
                    if smiles is not None:
                        smiles = rdkit.Chem.MolToSmiles(rdkit.Chem.MolFromSmiles(smiles)) # make sure that canonical SMILES are stored
                        valid_smiles_current_batch.append(smiles)
                        new_feature_array.append(X_next[i])
                        valid = True
                        # compute target score for the current molecule
                        if CONSTRAINED:
                            # only compute target score if the molecule is similar to the training data
                            if smiles in look_up_validities:
                                prediction_validity_score = look_up_validities[smiles]
                            else: 
                                prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                                look_up_validities[smiles] = prediction_validity_score
                            if prediction_validity_score == 1:
                                if smiles in look_up_predictions:
                                    predicted_target_value = look_up_predictions[smiles]
                                else:
                                    predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                    look_up_predictions[smiles] = predicted_target_value
                            elif prediction_validity_score == -1:
                                valid = False
                                predicted_target_value = -1000
                                print(f"Non valid prediction for {smiles}")
                            else:
                                raise ValueError("Validity class not valid.")
                        else: # no constrains on the applicability of the metric
                            if smiles in look_up_predictions:
                                predicted_target_value = look_up_predictions[smiles]
                            else:
                                predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                                look_up_predictions[smiles] = predicted_target_value
                        if valid: 
                            valid_smiles.append((smiles,predicted_target_value))
                        raw_score_list.append(predicted_target_value)
                        # possibly recompute target value
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused
                    else:
                        new_feature_array.append(X_next[i])
                        predicted_target_value = -1000
                        if STD_TARGET:
                            predicted_target_value = (predicted_target_value - y_train_mean) / y_train_std #TODO: does not work for ron-mon
                        score_list.append(-predicted_target_value) #target is always minused
                        
                    # current run time of optimization
                    current_time = time.time()
                    run_time = current_time - bo_start_time
                    run_times_current_batch.append(run_time)
                    
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    num_uni_smiles_current_batch.append(num_unique_mols)
                        
                    ## STOPPING CRITERIA
                    # stop BO if desired number of molecules is reached
                    stopping_criterion_active = False
                    if MAX_NUM_MOL:
                        stopping_criterion_active = True
                        print(f"---> Current number of unique molecules {num_unique_mols}.<---")
                        if num_unique_mols >= MAX_NUM_MOL:
                            logger(f'\n\t---> {MAX_NUM_MOL} unique molecules identified. BO stopped.<---')
                            stopping_criterion_reached = True
                            break
                    # stop BO after predefined time
                    if MAX_OPT_TIME:
                        if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                        stopping_criterion_active = True
                        if run_time >= MAX_OPT_TIME:
                            logger(f'\n\t---> {MAX_OPT_TIME} seconds optimization time reached. BO stopped. <---')
                            stopping_criterion_reached = True
                            break
                    # No stopping criteria active
                    if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')

                if new_feature_array != []:
                    new_feature_array = np.vstack(new_feature_array)
                logger(f"BO: {len(valid_smiles_current_batch)} molecules are found")
                
                for each_idx, each_mol in enumerate(valid_smiles_current_batch):
                    logger(f"BO: smiles: {each_mol}\n\t {TARGET}: {raw_score_list[each_idx]}")

                if len(new_feature_array) > 0:
                    X_train_excerpt = np.concatenate([X_train_excerpt, new_feature_array], 0)
                    y_train_excerpt = np.concatenate([y_train_excerpt, np.array(score_list)[:, None]], 0)
                    X_all_excerpt = np.concatenate([X_all_excerpt, new_feature_array], 0) # required for dimension reduction refit if dim_reduction with fix_dimension_reduction is False

                # save list of all smiles after each iteration
                path_smiles = [
                    [
                        valid_smiles_current_batch[tmp_idx], 
                        raw_score_list[tmp_idx], 
                        bo_iter + 1, 
                        num_mol + tmp_idx + 1,
                        run_times_current_batch[tmp_idx],
                        num_uni_smiles_current_batch[tmp_idx]
                    ] 
                    for tmp_idx, _ in enumerate(valid_smiles_current_batch)
                ]
                
                with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                    writer = csv.writer(f_table)
                    writer.writerows(path_smiles)
                    
                if ext_gp_method is not None:
                    history.append({'mol_list': valid_mol_list, 'feature': new_feature_array, 'score_list': score_list,
                                    'train_err': train_err, 'train_ll': train_ll,
                                    'test_err': test_err if valid_test else np.nan,
                                    'test_ll': test_ll if valid_test else np.nan})
                else:
                    history.append({'mol_list': valid_smiles_current_batch, 'feature': new_feature_array, 'score_list': score_list})               
                    
                # check if stopping criterion was reached in current batch
                if stopping_criterion_reached: break

            print("\n####################### Finished BO with GPyOpt #######################")
            return history, valid_smiles, latent_vectors_bo
        
        def generate_initial_data_combustion(X, y, num_train_samples=500, num_test_samples=100, permute=True):
            n = X.shape[0]
            if(num_train_samples==-1):
                num_train_samples=n
            breakpoint_train = min(np.int(np.round(0.9 * n)),num_train_samples)
            breakpoint_test = max(np.int(np.round(0.1 * n)), min(n - num_train_samples, num_test_samples))

            # select datapoints
            if permute:
                permutation = torch.from_numpy(np.random.choice(n, n, replace=False))
                X_train = X[permutation, :][0: breakpoint_train, :]
                y_train = y[permutation][0: breakpoint_train]
                X_test = X[permutation, :][breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[permutation][breakpoint_train: breakpoint_train + breakpoint_test]
            else:
                X_train = X[0: breakpoint_train, :]
                y_train = y[0: breakpoint_train]
                X_test = X[breakpoint_train: breakpoint_train + breakpoint_test, :]
                y_test = y[breakpoint_train: breakpoint_train + breakpoint_test]

            best_observed_value = np.max(y_train)

            return X_train, y_train, X_test, y_test, best_observed_value
        
        ############ Generate initial training data for BO ##############
        X_all, y_all, y_raw, valid_mask = self.requires()[2].load_output()
        if (len(valid_mask) != len(X_all)) or (len(valid_mask) != len(y_all)):
            raise ValueError(f"WARNING: Validity mask does not match number of data points.")
        X_all = np.array(X_all)[valid_mask]
        y_all = np.array(y_all)[valid_mask]
        

        # call helper functions to generate initial training data
        X_train, y_train, X_test, y_test, best_observed_value_nei = generate_initial_data_combustion(X=X_all, y=y_all, num_train_samples=self.BayesianOptimization_params['training_params']['num_train_samples'], num_test_samples=self.BayesianOptimization_params['training_params']['num_test_samples'])
        # save initial points
        with gzip.open(f"{os.path.dirname(self.output().path)}/BO_initial.pklz", 'wb') as f1:
            pickle.dump(
                {
                    'X_train': X_train, 
                    'y_train': y_train, 
                    'X_test': X_test, 
                    'y_test': y_test, 
                    'best_initial_value': best_observed_value_nei
                }, 
                f1
            )
            
        ############## RUN BO with GPyOpt ##############
        # BO parameters
        bo_params = BayesianOptimization_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: bo_params['run_params']['bo_num_iter'] = int(sys.maxsize)
        bo_run_params = bo_params['run_params']
        # save bo_params
        with open(f"{os.path.dirname(self.output().path)}/BO_params.json", 'a+') as fp:
            json.dump(bo_params, fp)
        # run BO
        history, valid_smiles, latent_vectors = run_gpyopt(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, X_all=X_all, y_all=y_all, **bo_run_params)
        print("Finished running the optimization. Now saving the output.")
        
        ## SAVE RESULTS
        # save history
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'wb') as f:
            pickle.dump(history, f)
        # save valid smiles as pklz
        save_object(valid_smiles, self.output().path)
        # save valid smiles as txt
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
        
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors, l_f)
        
    def load_output(self, ret_history=False):
        with gzip.open(f"{os.path.dirname(self.output().path)}/history.pklz", 'rb') as f:
            history = pickle.load(f)
        output_valid_smiles = load_object(self.output().path)
        if ret_history: return history
        return output_valid_smiles


class BayesianOptimizationWithPred(BayesianOptimization):
    '''Bayesian Optimization with Prediction Task
    
    This task executes Bayesian Optimization for MHG that was trained with predictions for the target property.
    '''

    Train_params = luigi.DictParameter(default=TrainWithPred_params)

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            TrainWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                TrainWithPred_params=self.Train_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                use_gpu=self.use_gpu
            ),
            ConstructDatasetForBOWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params,
                ConstructDatasetForBO_params=self.ConstructDatasetForBO_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                use_gpu=self.use_gpu,
                target=self.target
            )
        ]


class MultipleBO(MainTask, AutoNamingTask):
    '''Multiple Bayesian Optimization Task
    
    This task executes the Bayesian Optimization Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    BayesianOptimization_params = luigi.DictParameter(default=BayesianOptimization_params)
    MultipleBO_params = luigi.DictParameter(default=MultipleBO_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    working_subdir = luigi.Parameter(default="MultipleBO") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties

    def requires(self):
         # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print('Stopping criteria may be conflicting:\n1. finding {max_num_mols} unique molecules.\n2. Maximum optimization time {max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether BO is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.BayesianOptimization_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleBO_params['seed_list']
        
        return [
            BayesianOptimization(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params,
                BayesianOptimization_params=self.BayesianOptimization_params,
                ConstructDatasetForBO_params=self.ConstructDatasetForBO_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/BO_{each_seed}", 
                use_gpu=self.use_gpu
            ) 
            for each_seed in seed_list
        ]

    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            BO_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    Train_params=self.Train_params,
                    working_subdir=each_BO_run.working_subdir,
                    valid_smiles=each_BO_run.load_output()
                )
                for each_BO_run in BO_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        return []

class MultipleBOWithPred(MultipleBO):
    '''Multiple Bayesian Optimization with Prediction Task
    
    This task executes the Bayesian Optimization with Prediction Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    Train_params = luigi.DictParameter(default=TrainWithPred_params)
    working_subdir = luigi.Parameter(default="MultipleBOWithPred")

    def requires(self):
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleBO_params['seed_list']
        
        return [
            BayesianOptimizationWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params,
                BayesianOptimization_params=self.BayesianOptimization_params,
                ConstructDatasetForBO_params=self.ConstructDatasetForBO_params,
                ComputeTargetValues_params=self.ComputeTargetValues_params,
                target=self.target,
                max_num_mols=self.max_num_mols,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/BO_{each_seed}", 
                use_gpu=self.use_gpu
            ) 
            for each_seed in seed_list
        ]
    
    
class GeneticAlgorithm(AutoNamingTask):
    '''Genetic Algorithm Optimization Task
    
    This task executes the Genetic Algorithm Optimization for MHG.
    '''
    constrained = luigi.BoolParameter(default=GeneticAlgorithm_params['target_params']['constrained'])

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="genetic_algorithm")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False   # helper variable to avoid multiple renaming of working subdir when GA with applicability domain constraint is executed

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            Train( # TODO: do we need withPred?
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                use_gpu=self.use_gpu
            ),
            GenerateLatentFiles(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params = self.Train_params,
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                use_gpu = self.use_gpu
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name            
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))


    def run(self):
        
        # additional packages required for GA
        import geneticalgorithm.geneticalgorithm as ga
        import csv
        
        # helper class to stop GA when stopping criterion is reached by raising Error that can be catched
        class StoppingCriterionReachedError(Exception):
            '''
            Raised when maximum number of molecules is reached
            '''
            pass
        
        # seed everything
        seed_everything(self.seed)
        
        
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        state_dict, mhg_best_training_seed = self.requires()[1].load_output()

        torch.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed_all(mhg_best_training_seed)
        model_params = deepcopy(dict(self.Train_params['model_params']))
        model_params['batch_size'] = self.ConstructDatasetForBO_params['batch_size']
        model = ae_catalog[self.Train_params['model']](hrg=hrg, **model_params,
                                                       use_gpu=self.use_gpu)
        
        if self.use_gpu:
            model.cuda()
        model.load_state_dict(state_dict)
        torch.no_grad()
        
        # general parameters for the genetic algorithm
        TARGET = self.target
        STD_TARGET = self.GeneticAlgorithm_params['target_params']['std_target']
        print(f"\nGA: Target property {TARGET} - standardized {STD_TARGET}")
        CONSTRAINED = self.constrained
        print(f"\nGA: Constrained by applicability domain of GNN: {CONSTRAINED}")
        TIME_LIMIT_DECODING = self.Train_params['inference']['time_limit_decoding']
        print(f"\nGA: Time limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nGA: Optimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nGA: Optimization will be stopped after {MAX_OPT_TIME} seconds.")

        # global list within GeneticAlgorithm with SMILES and property values of all valid molecules found during optimization (chronologically)
        valid_smiles = []
        obj_fun_calls = []
        latent_vectors_ga = []
        look_up_predictions = {}
        look_up_validities = {}

        def objective(new_x):
            latent_vectors_ga.append(new_x)
            
            # number of objective function calls
            obj_fun_calls.append(1)
            tmp_calls = len(obj_fun_calls)
            ga_iter = (tmp_calls - 1) // GeneticAlgorithm_params['algorithm_param']['population_size']
            
            # get current run time
            current_time = time.time()
            run_time = current_time - GA_START_TIME
            
            # get current number of unique mols identified
            num_unique_mols = len(list(dict(valid_smiles).items()))

            ## STOPPING CRITERIA
            # stop GA if desired number of molecules is reached
            # TODO: decide if we want only count unique smiles -> vorzeitig, unterschiedlich, Moeglichkeit: -1 (keine Grenze), zeitlich Performance (target value) plotten ueber #Molekuels 
            stopping_criterion_active = False
            if MAX_NUM_MOL:
                stopping_criterion_active = True
                if num_unique_mols >= MAX_NUM_MOL:
                    print(f'\n---> {MAX_NUM_MOL} unique molecules identified. GA stopped.<---')
                    raise StoppingCriterionReachedError
            # stop BO after predefined time
            if MAX_OPT_TIME:
                if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                stopping_criterion_active = True
                if run_time >= MAX_OPT_TIME:
                    print(f'\n---> {MAX_OPT_TIME} seconds optimization time reached. GA stopped. <---')
                    raise StoppingCriterionReachedError
            # No stopping criteria active
            if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')
                
            # get smiles string from parameters
            # note that there long decoding times can occur
            # restrict decoding time -> func-timeout)
            @timeout(TIME_LIMIT_DECODING)
            def get_smiles(new_x): 
                smiles = get_decoded_smiles(new_x=new_x, model=model, batch_size=self.Train_params['model_params']['batch_size'], latent_dim=self.Train_params['model_params']['latent_dim'], deterministic=self.GeneticAlgorithm_params['run_params']['deterministic'])
                return smiles

            smiles = get_smiles(new_x)
            print(f"GA: Found following SMILES string: {smiles}\n")

            # evaluate the decoded molecule
            if smiles is not None:
                valid = True
                # compute target score for the current molecule
                if CONSTRAINED:
                    # only compute target score if the molecule is similar to the training data
                    if smiles in look_up_validities:
                        prediction_validity_score = look_up_validities[smiles]
                    else: 
                        prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model="kgnn", target=TARGET)[0]
                        look_up_validities[smiles] = prediction_validity_score
                    if prediction_validity_score == 1:
                        if smiles in look_up_predictions:
                            print("already predicted property for this smiles.")
                            predicted_target_value = look_up_predictions[smiles]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model="kgnn")[0]
                            look_up_predictions[smiles] = predicted_target_value
                    elif prediction_validity_score == -1:
                        predicted_target_value = -1000
                        valid = False
                        print(f"Non valid prediction for {smiles}")
                    else:
                        valid = False
                        raise ValueError("Validity class not valid.")
                else: # no constrains on the applicability of the metric
                    if smiles in look_up_predictions:
                        print("already predicted property for this smiles.")
                        predicted_target_value = look_up_predictions[smiles]
                    else:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model="kgnn")[0]
                        look_up_predictions[smiles] = predicted_target_value

                if valid: # valid can be false if the constraints are not satisfied.
                    
                    # always store with raw predicted value
                    valid_smiles.append((smiles,predicted_target_value)) # valid_smiles is global!
                    
                    # save results - NOTE: we need to save here bc GA does not have explicit loop
                    # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                    num_mol = len(valid_smiles)
                    # get current number of unique mols identified
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    path_smiles = [
                        smiles, 
                        predicted_target_value, 
                        ga_iter + 1, 
                        num_mol, 
                        run_time,
                        num_unique_mols
                    ]
                    
                    with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                        writer = csv.writer(f_table)
                        writer.writerow(path_smiles)
                        
                    # possibly recompute target value
                    if STD_TARGET:
                        predicted_target_value = MolecularMetrics.target_scores([smiles], TARGET, std_score=STD_TARGET)[0] 
                
                # save results - NOTE: we need to save here bc GA does not have explicit loop
                # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                print(f"Predicted value for GA: {-predicted_target_value}\n\n")
                return -predicted_target_value # return negative value bc ga minimizes
            else: # no molecule returned
                print("Returned penalty value: 1000\n\n")
                return 1000


        # set up bounds for optimization search space
        X_all = self.requires()[2].load_output() # load latent vectors
        X_min = np.min(X_all , axis=0) - (self.GeneticAlgorithm_params['run_params']['min_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        X_max = np.max(X_all , axis=0) + (self.GeneticAlgorithm_params['run_params']['max_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        varbound = np.transpose([X_min, X_max])
        
        # GA parameters
        ga_params = GeneticAlgorithm_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: ga_params['algorithm_param']['max_num_iteration'] = int(sys.maxsize - 1)
        ga_algorithm_params = ga_params['algorithm_param']
        # save GA params
        with open(f"{os.path.dirname(self.output().path)}/GA_params.json", 'a+') as fp:
            json.dump(ga_params, fp)
        
        # initialize GA model
        ga_model = ga(
            function=objective,
            dimension=self.Train_params['model_params']['latent_dim'],
            variable_type='real',
            variable_boundaries=varbound,
            algorithm_parameters=ga_algorithm_params,
            function_timeout=1000
        )

        # start the genetic algorithm (while measuring the runtime)
        print("\n####################### Start GA. #######################")
        
        GA_START_TIME = time.time()
        try:
            ga_model.run()
        except StoppingCriterionReachedError:
            if MAX_NUM_MOL:
                print(f"Stopping criterion (maximum number of molecules: {MAX_NUM_MOL}) is reached. GA stopped.")
            elif MAX_OPT_TIME: 
                print(f"Stopping criterion (maximum run time: {MAX_OPT_TIME}) is reached. GA stopped.")
            else:
                print("GA stopped for unknown reason.")
        ga_end_time = time.time()
        
        print("\n####################### GA finished. #######################")
        
        ## SAVE RESULTS
        save_object(valid_smiles, self.output().path)
        # create vobaculary and save it in the same dir as the training data
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
                
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors_ga, l_f)

        print("----------------------------------- RESULTS FOR PARAMS: -----------------------------------")
        print(f"GA parameters: {ga_algorithm_params}")
        print(f"\nGA: Total time for running: {ga_end_time - GA_START_TIME}.")

    def load_output(self):
        output_valid_smiles = load_object(self.output().path)
        return output_valid_smiles
    
    
class GeneticAlgorithmWithPred(GeneticAlgorithm):
    '''Genetic Algorithm Optimization with Prediction Task
    
    This task executes the Genetic Algorithm Optimization for MHG that was trained with predictions for the target property.
    '''

    Train_params = luigi.DictParameter(default=TrainWithPred_params)

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            TrainWithPred(
                DataPreprocessing_params=self.DataPreprocessing_params,
                TrainWithPred_params=self.Train_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                use_gpu=self.use_gpu
            ),
            GenerateLatentFilesWithPred(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params = self.Train_params,
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                use_gpu = self.use_gpu,
                target = self.target
            )
        ]
    

    
class MultipleGA(MainTask, AutoNamingTask):
    '''Multiple Genetic Algorithm Optimization Task
    
    This task executes the Genetic Algorithm Optimization Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    
    # internal params accessed from INPUT/param.py
    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    GeneticAlgorithm_params = luigi.DictParameter(default=GeneticAlgorithm_params)
    MultipleGA_params = luigi.Parameter(default=MultipleGA_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    
    # parameters that can be set from outside when calling MultipleGA main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether GA is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleGA") # working dir for MultipleGA
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
    
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print('Stopping criteria may be conflicting:\n1. finding {max_num_mols} unique molecules.\n2. Maximum optimization time {max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether GA is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.GeneticAlgorithm_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleGA_params['seed_list']
            
        # requirements (previous jobs to be executed)
        return [
            GeneticAlgorithm(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                GeneticAlgorithm_params=self.GeneticAlgorithm_params, 
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/GA_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            GA_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    Train_params=self.Train_params,
                    working_subdir=each_GA_run.working_subdir,
                    valid_smiles=each_GA_run.load_output()
                )
                for each_GA_run in GA_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        
        return []
    
class MultipleGAWithPred(MultipleGA):
    '''Multiple Genetic Algorithm Optimization with Prediction Task
    
    This task executes the Genetic Algorithm Optimization with Prediction Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    Train_params = luigi.DictParameter(default=TrainWithPred_params)
    working_subdir = luigi.Parameter(default="MultipleGAWithPred") # working dir for MultipleGA
    
    def requires(self):
                # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            
        # allow user to set whether GA is constrained by AD from outside and overwrite GeneticAlgorithm_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.GeneticAlgorithm_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleGA_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleGA_params['seed_list']
            
        return [
            GeneticAlgorithmWithPred(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                GeneticAlgorithm_params=self.GeneticAlgorithm_params, 
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                max_num_mols=self.max_num_mols,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/GA_{each_seed}", 
                use_gpu=self.use_gpu
            ) 
            for each_seed in seed_list
        ]
    
class RandomSearch(AutoNamingTask):
    '''Random Search Heurisitc.
    
    This task applies random sampling from latent space to find molecules with desired properties.
    '''
    constrained = luigi.BoolParameter(default=RandomSearch_params['target_params']['constrained'])

    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    RandomSearch_params = luigi.DictParameter(default=RandomSearch_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    
    target = luigi.Parameter(default=MolOpt_params['target'])
    pred_model = luigi.Parameter(default='kgnn')
    max_num_mols = luigi.IntParameter(default=MolOpt_params['max_num_mols'])
    max_opt_time = luigi.IntParameter(default=None)
    use_gpu = luigi.BoolParameter()
    seed = luigi.IntParameter(default=123)
    
    working_subdir = luigi.Parameter(default="Random")
    output_name = luigi.Parameter(default="valid_smiles")
    output_ext = luigi.Parameter(default='pklz')
    renamed = False   # helper variable to avoid multiple renaming of working subdir when Random with applicability domain constraint is executed

    def requires(self):
        return [
            DataPreprocessing(
                Train_params=self.Train_params,
                DataPreprocessing_params=self.DataPreprocessing_params
            ),
            Train( # TODO: do we need withPred?
                DataPreprocessing_params=self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                use_gpu=self.use_gpu
            ),
            GenerateLatentFiles(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params = self.Train_params,
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                use_gpu = self.use_gpu
            )
        ]
    
    # overwrite luigine -> AutoNamingTask -> output() to include constraint information into subfolder name
    def output(self):
        # add information to subdir name
        if not self.renamed:
            self.working_subdir = self.working_subdir + f"_{self.target}" # add TARGET property to subdir name            
            # add set-up to output folder name
            if self.max_num_mols:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxNumMol{self.max_num_mols}"
            elif self.max_opt_time:
                self.working_subdir = self.working_subdir + f"_StopCrit_MaxOptTime{self.max_opt_time}"
            if self.constrained:
                self.working_subdir = self.working_subdir + "_ADconstraint"
            self.renamed = True
        if not os.path.exists('OUTPUT'):
            os.mkdir('OUTPUT')
        if not os.path.exists(os.path.join(
                "OUTPUT",
                self.working_subdir)):
            os.makedirs(os.path.join(
                "OUTPUT",
                self.working_subdir))
        return luigi.LocalTarget(os.path.join(
            "OUTPUT",
            self.working_subdir,
            "{}.{}".format(self.output_name, self.output_ext)))


    def run(self):
        
        # additional packages required for Random
        import csv
        
        # helper class to stop Random when stopping criterion is reached by raising Error that can be catched
        class StoppingCriterionReachedError(Exception):
            '''
            Raised when maximum number of molecules is reached
            '''
            pass
        
        
        # seed everything
        seed_everything(self.seed)
        
        
        hrg, prod_rule_seq_list = self.requires()[0].load_output()
        state_dict, mhg_best_training_seed = self.requires()[1].load_output()
        
        print(mhg_best_training_seed)
        torch.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed(mhg_best_training_seed)
        torch.cuda.manual_seed_all(mhg_best_training_seed)
        model_params = deepcopy(dict(self.Train_params['model_params']))
        model_params['batch_size'] = self.ConstructDatasetForBO_params['batch_size']
        model = ae_catalog[self.Train_params['model']](hrg=hrg, **model_params,
                                                       use_gpu=self.use_gpu)
        
        if self.use_gpu:
            model.cuda()
        model.load_state_dict(state_dict)
        torch.no_grad()
        
        # general parameters for the random search
        TARGET = self.target
        STD_TARGET = self.RandomSearch_params['target_params']['std_target']
        print(f"\nRandom: Target property {TARGET} - standardized {STD_TARGET}")
        CONSTRAINED = self.constrained
        print(f"\nRandom: Constrained by applicability domain of GNN: {CONSTRAINED}")
        TIME_LIMIT_DECODING = self.Train_params['inference']['time_limit_decoding']
        print(f"\nRandom: Time limit for decoding a single element: {TIME_LIMIT_DECODING} seconds")
        MAX_NUM_MOL = None
        MAX_OPT_TIME = None
        if self.max_num_mols:
            MAX_NUM_MOL = int(self.max_num_mols)
            print(f"\nRandom: Optimization will be stopped after #{MAX_NUM_MOL} molecules are found.")
        elif self.max_opt_time:
            MAX_OPT_TIME = int(self.max_opt_time)
            print(f"\nRandom: Optimization will be stopped after {MAX_OPT_TIME} seconds.")

        # global list within Random Search with SMILES and property values of all valid molecules found during optimization (chronologically)
        valid_smiles = []
        obj_fun_calls = []
        latent_vectors_random = []
        look_up_predictions = {}
        look_up_validities = {}

        def objective(new_x):
            
            # store latent vector
            latent_vectors_random.append(new_x)
            
            # number of funciton calls
            obj_fun_calls.append(1)
            tmp_calls = len(obj_fun_calls)
            random_iter = tmp_calls - 1
                        
            # get current run itme    
            current_time = time.time()
            run_time = current_time - rand_start_time
            
            # get current number of unique mols identified
            num_unique_mols = len(list(dict(valid_smiles).items()))
            

            ## STOPPING CRITERIA
            # stop BO if desired number of molecules is reached
            # TODO: decide if we want only count unique smiles -> vorzeitig, unterschiedlich, Moeglichkeit: -1 (keine Grenze), zeitlich Performance (target value) plotten ueber #Molekuels 
            stopping_criterion_active = False
            if MAX_NUM_MOL:
                stopping_criterion_active = True
                if num_unique_mols >= MAX_NUM_MOL:
                    print(f'\n---> {MAX_NUM_MOL} unique molecules identified. Random stopped.<---')
                    raise StoppingCriterionReachedError
            # stop BO after predefined time
            if MAX_OPT_TIME:
                if stopping_criterion_active == True: raise ValueError('\t--- Multiple stopping criteria active. ---')
                stopping_criterion_active = True
                if run_time >= MAX_OPT_TIME:
                    print(f'\n---> {MAX_OPT_TIME} seconds optimization time reached. Random stopped. <---')
                    raise StoppingCriterionReachedError
            # No stopping criteria active
            if not stopping_criterion_active: raise ValueError('\t--- No stopping criterion active. ---')
                
            # get smiles string from parameters
            # note that there long decoding times can occur
            # restrict decoding time -> func-timeout
            @timeout(TIME_LIMIT_DECODING)
            def get_smiles(new_x): 
                smiles = get_decoded_smiles(new_x=new_x, model=model, batch_size=self.Train_params['model_params']['batch_size'], latent_dim=self.Train_params['model_params']['latent_dim'], deterministic=self.RandomSearch_params['run_params']['deterministic'])
                return smiles

            smiles = get_smiles(new_x)

            # evaluate the decoded molecule
            if smiles is not None:
                valid = True
                # compute target score for the current molecule
                if CONSTRAINED:
                    # only compute target score if the molecule is similar to the training data
                    if smiles in look_up_validities:
                        prediction_validity_score = look_up_validities[smiles]
                    else: 
                        prediction_validity_score = PredictionValidity.validity_score([smiles], "svm", model=self.pred_model, target=TARGET)[0]
                    if prediction_validity_score == 1:
                        if smiles in look_up_predictions:
                            print("already predicted property for this smiles.")
                            predicted_target_value = look_up_predictions[smiles]
                        else:
                            predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                            look_up_predictions[smiles] = predicted_target_value
                    elif prediction_validity_score == -1:
                        predicted_target_value = -1000
                        valid = False
                        print(f"Non valid prediction for {smiles}")
                    else:
                        valid = False
                        raise ValueError("Validity class not valid.")
                else: # no constrains on the applicability of the metric
                    if smiles in look_up_predictions:
                        print("already predicted property for this smiles.")
                        predicted_target_value = look_up_predictions[smiles]
                    else:
                        predicted_target_value = MolecularMetrics.target_scores(smiles=[smiles], target=TARGET, model=self.pred_model)[0]
                        look_up_predictions[smiles] = predicted_target_value

                if valid: # valid can be false if the constraints are not satisfied.
                    
                    # always store with raw predicted value
                    valid_smiles.append((smiles,predicted_target_value)) # valid_smiles is global!
                    
                    # save results - NOTE: we need to save here bc random does not have explicit loop
                    # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                    num_mol = len(valid_smiles)
                    # get current number of unique mols identified
                    num_unique_mols = len(list(dict(valid_smiles).items()))
                    path_smiles = [
                        smiles, 
                        predicted_target_value, 
                        random_iter + 1, 
                        num_mol, 
                        run_time,
                        num_unique_mols
                    ]
                    
                    with open(os.path.join(os.path.dirname(self.output().path), f'smiles_list_path.csv'), 'a+', newline="") as f_table:
                        writer = csv.writer(f_table)
                        writer.writerow(path_smiles)
                        
                    # possibly recompute target value
                    if STD_TARGET:
                        predicted_target_value = MolecularMetrics.target_scores([smiles], TARGET, std_score=STD_TARGET)[0] #TODO: does not work for ron-mon
                
                # save results - NOTE: we need to save here bc random does not have explicit loop
                # save_object(valid_smiles, f'{os.path.dirname(self.output().path)}/valid_smiles_w_predictions.pklz')
                print(f"Predicted value for Random: {-predicted_target_value}\n\n")
                return -predicted_target_value # return negative value bc ga minimizes
            else: # no molecule returned
                print("Returned penalty value: 1000\n\n")
                return 1000


        X_all = self.requires()[2].load_output() # load latent vectors
        X_min = np.min(X_all , axis=0) - (self.RandomSearch_params['run_params']['min_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        X_max = np.max(X_all , axis=0) + (self.RandomSearch_params['run_params']['max_coef'] - 1) * (np.max(X_all , axis=0) - np.min(X_all , axis=0))
        varbound = np.transpose([X_min, X_max])
        
        # Random Search parameters
        random_params = RandomSearch_params
        # if max run time is selected as stopping criterion, num of iterations is set max possible value (because run time is limited in contrast to as in max_num_mols stopping criterion)
        if MAX_OPT_TIME: RandomSearch_params['algorithm_param']['max_num_iteration'] = int(sys.maxsize - 1)
        random_search_params = random_params['algorithm_param']
        with open(f"{os.path.dirname(self.output().path)}/RandomSearch_params.json", 'a+') as fp:
            json.dump(random_search_params, fp)
            
        #print(varbound)
        #print(self.Train_params['model_params']['latent_dim'])

        # start the random search (while measuring the runtime)
        print(f"\n####################### Start Random Search with seed {self.seed}. #######################")
        
        rand_start_time = time.time()
        try:
            for r_iter in range(random_search_params['max_num_iteration']):
                new_latent = np.random.uniform(X_min, X_max, (self.Train_params['model_params']['latent_dim'],))
                #print(f"Latent: {new_latent}")
                new_obj = objective(new_latent)
        except StoppingCriterionReachedError:
            if MAX_NUM_MOL:
                print(f"Stopping criterion (maximum number of molecules: {MAX_NUM_MOL}) is reached. Random stopped.")
            elif MAX_OPT_TIME: 
                print(f"Stopping criterion (maximum run time: {MAX_OPT_TIME}) is reached. Random stopped.")
            else:
                print("Random stopped for unknown reason.")
        rand_end_time = time.time()
        
        print("\n####################### Random Search finished. #######################")
        
        save_object(valid_smiles, self.output().path)
        # create vobaculary and save it in the same dir as the training data
        with open(f"{os.path.dirname(self.output().path)}/valid_smiles.txt", 'a+') as result_file:
            for x in valid_smiles:
                result_file.write(f'{x}\n')
                
        with gzip.open(f"{os.path.dirname(self.output().path)}/latent_vectors.pklz", "wb") as l_f:
            pickle.dump(latent_vectors_random, l_f)

        print("----------------------------------- RESULTS FOR PARAMS: -----------------------------------")
        print(f"\nRandom search parameters: {random_search_params}")
        print(f"Random: Total time for running: {rand_end_time - rand_start_time}.")

    def load_output(self):
        output_valid_smiles = load_object(self.output().path)
        return output_valid_smiles
    
class MultipleRandomSearch(MainTask, AutoNamingTask):
    '''Multiple Random Search Task
    
    This task executes the Random Search Task for multiple times.
    The number of executions is determined by the length of seed list provided.
    '''
    
    
    # internal params accessed from INPUT/param.py
    DataPreprocessing_params = luigi.DictParameter(default=DataPreprocessing_params)
    Train_params = luigi.DictParameter(default=Train_params)
    MolOpt_params = luigi.DictParameter(default=MolOpt_params)
    RandomSearch_params = luigi.DictParameter(default=RandomSearch_params)
    MultipleRandomSearch_params = luigi.Parameter(default=MultipleRandomSearch_params)
    ComputeTargetValues_params = luigi.DictParameter(default=ComputeTargetValues_params)
    ConstructDatasetForBO_params = luigi.DictParameter(default=ConstructDatasetForBO_params)
    
    # parameters that can be set from outside when calling MultipleRandomSearch main.py
    target = luigi.Parameter(default=None) # set target property from outside
    max_num_mols = luigi.Parameter(default=None) # set maximum number of unique molecules after which optimization is stopped
    max_opt_time = luigi.Parameter(default=None) # set time limit for optimization
    constrained = luigi.BoolParameter(default=None) # whether Random Search is constrained by applicability domain
    single_seed = luigi.Parameter(default=None) # set single seed from outside
    working_subdir = luigi.Parameter(default="MultipleRandomSearch") # working dir for MultipleRandomSearch
    use_gpu = luigi.BoolParameter() # whether gpu is used
    plot_results = luigi.BoolParameter(default=True) # whether results of optimization runs are plotted
    pred_model = luigi.Parameter(default='kgnn') # model that is used for predicting the target properties
    
    # TODO: move plotting outside optimization tasks
    def requires(self):
        
        # allow user to set target property from outside and overwrite MolOpt_params['target'] in INPUT/params.py
        if not self.target:
            self.target = self.MolOpt_params['target']
            
        # allow user to set maximum number of mols from outside and overwrite MolOpt_params['max_num_mols'] in INPUT/params.py
        if not self.max_num_mols and not self.max_opt_time:
            self.max_num_mols = self.MolOpt_params['max_num_mols']
            print(f'Stopping criterion for optimization set to finding {max_num_mols} unique molecules.')
                
        if self.max_num_mols and self.max_opt_time: 
            self.max_num_mols = None
            print('Stopping criteria may be conflicting:\n1. finding {max_num_mols} unique molecules.\n2. Maximum optimization time {max_opt_time} sec.\nThus maximum optimization time is choosen.')
            
        # allow user to set whether Random Search is constrained by AD from outside and overwrite RandomSearch_params['target_params']['constrained'] in INPUT/params.py
        if not self.constrained:
            self.constrained = self.RandomSearch_params['target_params']['constrained']
            
        # allow user to set single seed from outside and overwrite MultipleRandomSearch_params['seed_list'] in INPUT/params.py
        if self.single_seed:
            seed_list = [self.single_seed]
        else:
            seed_list = self.MultipleRandomSearch_params['seed_list']
            
        # requirements (previous jobs to be executed)
        return [
            RandomSearch(
                DataPreprocessing_params = self.DataPreprocessing_params,
                Train_params=self.Train_params, 
                RandomSearch_params=self.RandomSearch_params, 
                ConstructDatasetForBO_params = self.ConstructDatasetForBO_params,
                ComputeTargetValues_params = self.ComputeTargetValues_params,
                target=self.target,
                max_num_mols=self.max_num_mols,
                max_opt_time=self.max_opt_time,
                constrained=self.constrained,
                seed=int(each_seed), 
                working_subdir=f"{self.working_subdir}/{self.pred_model}/RandomSearch_{each_seed}", 
                use_gpu=self.use_gpu,
                pred_model=self.pred_model,
            )
            for each_seed in seed_list
        ]
    
    def run(self):
        # run plotting after optimization (this solution is not ideal because it does not schedule an extra Task for plotting but at least it is outside the optimization)
        if self.plot_results:
            random_runs = self.requires()
            plotting = [
                DrawResultPlots(
                    Train_params=self.Train_params,
                    working_subdir=each_random_run.working_subdir,
                    valid_smiles=each_random_run.load_output()
                )
                for each_random_run in random_runs
            ]
            for each_plot_task in plotting:
                each_plot_task.run()
        
        return []
    

if __name__ == "__main__":
    for each_engine_status in glob.glob("./engine_status.*"):
        os.remove(each_engine_status)
    with open("engine_status.ready", "w") as f:
        f.write("ready: {}\n".format(datetime.now().strftime('%Y/%m/%d %H:%M:%S')))
    
    print("\nLuigine & Luigi: We start!")
    main() # luigine.main()
    print("\nLugine & Luigi: We finished!")
