#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Title """

__author__ = "Hiroshi Kajino <KAJINO@jp.ibm.com>"
__copyright__ = "(c) Copyright IBM Corp. 2018"
__version__ = "0.1"
__date__ = "July 18 2018"

import os
from rdkit import Chem
import pandas as pd
import numpy as np

from graph_neural_network_for_fuel_ignition_quality.trained_model.predict_DCN_MON_RON_single_mol import get_props


dcn_buff = {}
mon_buff = {}
ron_buff = {}
props = pd.read_csv('data/qm9/raw/qm9_predicted_props.csv')

dcn_mean = np.mean(props['DCN'])
mon_mean = np.mean(props['MON'])
ron_mean = np.mean(props['RON'])
dcn_std = np.std(props['DCN'])
mon_std = np.std(props['MON'])
ron_std = np.std(props['RON'])
dcn_max = np.max(props['DCN'])
mon_max = np.max(props['MON'])
ron_max = np.max(props['RON'])
dcn_min = np.min(props['DCN'])
mon_min = np.min(props['MON'])
ron_min = np.min(props['RON'])


def valid_lambda_special(x):
    s = Chem.MolToSmiles(x) if x is not None else ''
    return x is not None and '*' not in s and '.' not in s and s != ''


def target_score(mol, target, std_score=False, norm=False):
    if target == 'DCN':
        return dcn_score(mol, std_score, norm)
    elif target == 'MON':
        return mon_score(mol, std_score, norm)
    elif target == 'RON':
        return ron_score(mol, std_score, norm)
    elif target in ['DCN+MON+RON', 'norm_DCN+MON+RON']:
        dcn = dcn_score(mol, std_score, norm)
        mon = mon_score(mol, std_score, norm)
        ron = ron_score(mol, std_score, norm)
        return dcn+mon+ron
    elif target == 'RON-MON':
        mon = mon_score(mol, std_score, norm)
        ron = ron_score(mol, std_score, norm)
        return ron-mon
    else:
        raise RuntimeError('target unknown: {}'.format(target))


def dcn_score(mol, std_score=False, norm=False):
    smi = Chem.MolToSmiles(mol)
    if smi is None or not valid_lambda_special(Chem.MolFromSmiles(smi)):
        # return 0
        v = 0
    elif smi in dcn_buff:
        v = dcn_buff[smi]
        # return dcn_buff[smi]
    else:
        try:
            df = props[props['SMILES'] == smi]
            # return df['DCN'].iloc[0]
            v = df['DCN'].iloc[0]
        except IndexError:
            v = get_props(smi)['DCN']
            dcn_buff[smi] = v
            # return v
    if std_score:
        return (v - dcn_mean) / dcn_std
    elif norm:
        return (v - dcn_min) / (dcn_max - dcn_min)
    else:
        return v


def mon_score(mol, std_score=False, norm=False):
    smi = Chem.MolToSmiles(mol)
    if smi is None or not valid_lambda_special(Chem.MolFromSmiles(smi)):
        v = 0
    elif smi in mon_buff:
        v = mon_buff[smi]
    else:
        try:
            df = props[props['SMILES'] == smi]
            v = df['MON'].iloc[0]
        except IndexError:
            v = get_props(smi)['MON']
            mon_buff[smi] = v
    if std_score:
        return (v - mon_mean) / mon_std
    elif norm:
        return (v - mon_min) / (mon_max - mon_min)
    else:
        return v


def ron_score(mol, std_score=False, norm=False):
    smi = Chem.MolToSmiles(mol)
    if smi is None or not valid_lambda_special(Chem.MolFromSmiles(smi)):
        v = 0
    elif smi in ron_buff:
        v = ron_buff[smi]
    else:
        try:
            df = props[props['SMILES'] == smi]
            v = df['RON'].iloc[0]
        except IndexError:
            v = get_props(smi)['RON']
            ron_buff[smi] = v
    if std_score:
        return (v - ron_mean) / ron_std
    elif norm:
        return (v - ron_min) / (ron_max - ron_min)
    else:
        return v


# def save_dcn_dict():
#     with open('/home/stefanie/Downloads/graph_grammar-master/tasks/template_working_dir_DCN/INPUT/data/dcn.pkl', 'wb') as f:
#         pickle.dump(dcn_buff, f, pickle.HIGHEST_PROTOCOL)
#
#
# def load_dcn_dict():
#     with open ('/home/stefanie/Downloads/graph_grammar-master/tasks/template_working_dir_DCN/INPUT/data/dcn.pkl', 'rb') as f:
#         return pickle.load(f)
