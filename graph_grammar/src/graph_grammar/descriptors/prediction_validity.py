import os
import pickle
import gzip
from rdkit import DataStructs
from rdkit import Chem
from rdkit.Chem import QED
from rdkit.Chem import Crippen

import math
import numpy as np
import pandas as pd

from graph_neural_network_for_fuel_ignition_quality.trained_model.infer_DCN_MON_RON_single_mol import get_lats
from gnn_validity.ad_methods.support_vector_machine.ocsvm import SVM_AD


class PredictionValidity(object):

    @staticmethod
    def validity_score(smiles, method, **kwargs):
        if method == 'svm':
            return PredictionValidity.svm_classify(smiles, kwargs)
        elif method == 'tanimoto':
            raise NotImplementedError("Tanimoto similarity not yet imlpemented.")
        elif method == 'pca':
            raise NotImplementedError("PCA not yet imlpemented.")
        else:
            raise RuntimeError('Method unknown: {}'.format(method))

    @staticmethod
    def svm_classify(smiles, kwargs={}):
    # svm either returns 1 (valid) or -1 (invalid)

        # get latent vectors of smiles
        smiles_latent = []
        for smi in smiles:
            if smi is None or not PredictionValidity.valid_lambda_special(Chem.MolFromSmiles(smi)):
                smiles_latent.append(None)
            else:
                smiles_latent.append(get_lats(smi))

        # initialize SVM
        svm_ad = SVM_AD()

        val_score = []
        # evaluate each SVM for a GNN within the ensemble (in GNN_FIQ_Schweidtmann -> 40 GNNs -> 40 SVMs evaluations)
        num_models = 40 # number of models in GNN ensemble, TODO: make it variable
        for model_id in range(1,num_models+1):
            kernel, gamma, nu, tol = 'rbf', 'scale', 0.05, 1e-3 # select hyperparameters
            model_name = "hp_test_{}_{}_{}_{}_GNN{}".format(kernel, gamma, nu, tol, model_id)
            svm_ad.load_model("../../gnn_validity/pretrained_models/support_vector_machine/GNN_FIQ_Schweidtmann", model_name) # TODO: change path dep and make it a variable
            val_score_i = []
            for smi_lat in smiles_latent:
                if smi_lat is None:
                    val_score_i.append(-1)
                else:
                    tmp_latent_vec = smi_lat[model_id-1][0][1]
                    tmp_val_score = svm_ad.predict([tmp_latent_vec])
                    val_score_i.append(tmp_val_score)
            val_score.append(val_score_i)
        
        # majority vote across SVMs for ensembled GNNs (in total: 40 votes of either 1 or -1)
        val_score = np.array(val_score).sum(axis=0) # sum votes (note that sum cannot be odd number)
        val_score = val_score.clip(max=1) # if sum >= 1 -> valid -> 1
        val_score[np.where(val_score<1)] = -1 # if sum <= 0 -> invalid -> -1
        return val_score.reshape(-1)


    @staticmethod
    def valid_lambda(x):
        return x is not None and Chem.MolToSmiles(x) != ''

    @staticmethod
    def valid_lambda_special(x):
        s = Chem.MolToSmiles(x) if x is not None else ''
        return x is not None and '*' not in s and '.' not in s and s != ''
