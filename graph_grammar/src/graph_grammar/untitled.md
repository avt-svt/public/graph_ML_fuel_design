Sachen die ich ausgeführt habe

ACHTUNG: auf Hilbert ist das conda-environment "molgan" für die Graph grammar und umgekehrt "grammar" für Molgan...

pip3.7 install geneticalgorithm

in tasks:


python3.7 main.py
    wirft Fehlermeldungen aus was alles fehlt und beschwert sich dann , dass working-dir nicht gesetzt ist.

OMP_NUM_THREADS=2 python3.7 main.py MultipleBayesianOptimizationWithPred --working-dir template_working_dir_RON_MON --use-gpu --workers 4
    hat erstmal nicht funktioniert

Fehlermeldungen werden nicht in der Konsole ausgegeben, sondern in working_dir/ENGLOG/engine.log
-> dort gefunden, dass die Daten nicht geladen werden konnten und python dabei abgestürzt ist. Die Fehlermeldung landet dabei nicht auf der Konsole, sondern ausschliesslich im logfile.
-> hinzufügen des Symlinks auf den data Ordner in jedem INPUT ordner.
-> all_smiles.txt existiert erstmal nicht
    -> wo kommt die denn her?
    -> habe die qm9/raw/HCO_smiles.txt genommen, umbenannt und nach oben verschoben (dahin wo gesucht wird)
    
ohne weitere veränderungen passiert hiermit etwas:    
python3.7 main.py MultipleBayesianOptimizationWithPred --working-dir template_working_dir_MON --workers=1 --use-gpu

jetzt stürzt es mit einem Segfault in der PCA ab. (und dank segfault funktioniert auch debugging nicht so richtig)
